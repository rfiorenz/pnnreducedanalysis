MAKEFLAGS += --check-symlink-times

BUILD_DIR := build
BIN_DIR := bin
ANALYZER_SRC := Analyzers/src
EXECUTABLE_DIR := Executables

PNNNA62EVENT := PnnNA62Event
PNNNA62EVENT_HH := Tools/include/$(PNNNA62EVENT).hh
LINKDEF_H := Tools/include/LinkDef.h

ROOTCONFIG := root-config
ROOTFLAGS := $(shell $(ROOTCONFIG) --cflags)
ROOTLIBS := $(shell $(ROOTCONFIG) --libs) -lTMVA

INCLUDEFLAGS := $(addprefix -I,$(shell find . -type d -name 'include'))
COMMONFLAGS := $(ROOTFLAGS) $(if ${DEBUG},-g,-O) -fPIC $(INCLUDEFLAGS)
CXXFLAGS := $(COMMONFLAGS) -MMD -MP
LDFLAGS += $(ROOTLIBS) -lstdc++fs

PNNNA62EVENT_OBJ := $(BUILD_DIR)/$(PNNNA62EVENT)Dict.o
OBJS := $(PNNNA62EVENT_OBJ)
OBJS += $(patsubst %,$(BUILD_DIR)/%.o,$(shell find Tools -name '*.cc'))
OBJS += $(BUILD_DIR)/main.cc.o
OBJS += $(BUILD_DIR)/$(ANALYZER_SRC)/VAnalysis.cc.o

PRECOMPILE = echo Building object at $@ && mkdir -p $(@D)

ANALYZEROBJ = $(addprefix $(BUILD_DIR)/$(ANALYZER_SRC)/,$(shell [ $* \= Makefile ] && exit; grep '\#include' $(EXECUTABLE_DIR)/$*.cc | sed 's/"//g;s/'"\#include"' //;s/.hh/.cc.o/'))

EXECUTABLES := $(patsubst $(EXECUTABLE_DIR)/%.cc,$(BIN_DIR)/%,$(wildcard $(EXECUTABLE_DIR)/*.cc))

all: $(EXECUTABLES)

.PHONY: clean
clean:
	rm -rf $(BUILD_DIR)
	rm -rf $(BIN_DIR)

.SECONDEXPANSION:
$(EXECUTABLES): $(BIN_DIR)/%: $(OBJS) $$(ANALYZEROBJ) $(BUILD_DIR)/$(EXECUTABLE_DIR)/%.cc.o
	@$(PRECOMPILE)
	@$(CXX) $(OBJS) $(ANALYZEROBJ) $(BUILD_DIR)/$(EXECUTABLE_DIR)/$*.cc.o -o $@ $(LDFLAGS)

$(BUILD_DIR)/%.cc.o: %.cc
	@$(PRECOMPILE)
	@$(CXX) $(CXXFLAGS) -c $< -o $@

$(PNNNA62EVENT_OBJ): $(BUILD_DIR)/$(PNNNA62EVENT)Dict.cxx
	@$(PRECOMPILE)
	@$(CXX) $(COMMONFLAGS) -I. -c $< -o $@
	@mkdir -p $(BIN_DIR) && cp $(BUILD_DIR)/*.pcm $(BIN_DIR)

$(BUILD_DIR)/$(PNNNA62EVENT)Dict.cxx: $(PNNNA62EVENT_HH) $(LINKDEF_H)
	@echo Generating ROOT Dictionary $@
	@mkdir -p $(@D)
	@rootcling -f $@ $(INCLUDEFLAGS) $^

-include $(patsubst %,$(BUILD_DIR)/%.d,$(shell find . -name '*.cc'))