#!/bin/bash

pathappend() {
    if [ -z "${!1}" ]; then # Make sure the variable exists and is exported
        eval "${1}="""
    fi
    for ARG in "${@:2}"
    do
        if [[ ":${ARG}:" != *":${!1}:"* ]]; then
            eval "${1}=$ARG:${!1}"
        fi
    done
    export ${1}
}

if [ ! -z "$DEBUG" ]; then
    BUILDMODE=dbg
else
    BUILDMODE=opt
fi

source /cvmfs/sft.cern.ch/lcg/contrib/clang/14.0.6/x86_64-centos9/setup.sh # For clang-format

source /cvmfs/sft.cern.ch/lcg/contrib/gcc/11.3.0/x86_64-centos9-gcc11-opt/setup.sh
source /cvmfs/sft.cern.ch/lcg/releases/LCG_102b/gdb/11.2/x86_64-centos9-gcc11-opt/gdb-env.sh
source /cvmfs/sft.cern.ch/lcg/releases/LCG_102b/ROOT/6.26.08/x86_64-centos9-gcc11-"${BUILDMODE}"/bin/thisroot.sh
LCGDIR=/cvmfs/sft.cern.ch/lcg/views/LCG_102b/x86_64-centos9-gcc11-"${BUILDMODE}"
pathappend LD_LIBRARY_PATH ${LCGDIR}/lib ${LCGDIR}/lib64 

