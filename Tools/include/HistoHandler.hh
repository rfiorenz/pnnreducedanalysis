#pragma once

#include <TString.h>
#include <TObject.h>
#include <TH1.h>
#include <TH2.h>
#include <TDirectory.h>
#include <TEfficiency.h>
#include <vector>
#include <unordered_map>

#include "globals.hh"

class HistoHandler {
public:
  HistoHandler(TDirectory *parent = nullptr, const TString path = "");
  ~HistoHandler();
  std::shared_ptr<HistoHandler> MakeChild(const TString path = "");

  static bool AutoWrite;

  void BookHisto(TObject *object, TString path = "", TString name = "");
  void UnbookHisto(TString name);

  template <typename T>
  T *Get(TString name) const
  {
    auto it = fHistos.find(name);
    if (it != fHistos.end())
      return dynamic_cast<T *>(it->second);

    // not found, search for it in the children
    for (auto &child : fChildren)
      if (T *result = child->Get<T>(name))
        return result;

    return NULL;
  }

  template <typename T = TH1, typename... Args>
  void FillHisto(TString name, Args... args)
  {
    T *histo = Get<T>(name);
    if (histo)
      histo->Fill(args...);
  }

  void Write(TString name = "", TDirectory *outputDirectory = nullptr);

  static TH2F *ResizeHisto(TH2 *histo, TString newName, double xmin = NAN, double xmax = NAN, double ymin = NAN, double ymax = NAN); // copy the contents into a differently limited histo (NAN = auto)
  void         ResizeHisto(TString name, double xmin = NAN, double xmax = NAN, double ymin = NAN, double ymax = NAN);                // resize a booked histogram

  static TH1 *ApplyEfficiency(const TH1 *histo, const TEfficiency *efficiency, const TString newname, bool binomialError = true);    // errors = propagation of variances of Poisson or of binomial
  static TH1 *DivideByEfficiency(const TH1 *histo, const TEfficiency *efficiency, const TString newname, bool binomialError = true); // same as above

protected:
  TDirectory   *fParentDirectory;
  const TString fPath;

  std::vector<std::pair<TString, TString>> fHistoOrder;
  std::unordered_map<TString, TObject *>   fHistos;

  std::vector<std::shared_ptr<HistoHandler>> fChildren;

  bool fWritten = false; // if the user calls Write(), they are responsible to write: don't autowrite
};