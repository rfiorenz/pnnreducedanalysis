#pragma once

#include "HistoHandler.hh"
#include <TGraph.h>

namespace Pnn {

class KpiAssociationStd {
public:
  KpiAssociationStd(HistoHandler &histo, bool isRun2);

  bool                 QualityCut(double CDA, double TGTK, double TKTAG, double TRICH);
  std::pair<int, bool> FindMatch(const std::vector<PileupTrack> &event);

  const std::vector<double> NGTK;

protected:
  std::shared_ptr<HistoHandler> fHisto;

  std::vector<double> fPDFKaonCDA[2];
  std::vector<double> fPDFPileCDA[2];
  std::vector<double> fPDFKaonDT[2];
  std::vector<double> fPDFPileDT[2];
  Double_t            fIntPDFKaonCDA[2];
  Double_t            fIntPDFKaonDT[2];
  Double_t            fIntPDFPileCDA[2];
  Double_t            fIntPDFPileDT[2];

  Double_t PDFKaonCDA(Double_t cda);
  Double_t PDFKaonDT(Double_t dt, bool RICH);
  Double_t PDFPileDT();
  Double_t PDFPileCDA(Double_t cda);
  void     DiscriminantNormalization(bool detector);
  double   Discriminant(double cda_val, double dt_val, bool detector);
};

} // namespace Pnn