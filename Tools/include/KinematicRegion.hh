#pragma once

#include <TString.h>
#include <map>

namespace Pnn {

enum KinematicRegion {
  kPNN1       = 1,
  kPNN2       = 2,
  kControl1   = 10,
  kControl2   = 20,
  kControlMu  = 30,
  kControlMu2 = 31,
  kControlMu3 = 32,
  kControl3pi = 40,
  kControl3d  = 50,
  kK2pi       = 100,
  kKmu2       = 200,
  kK3pi       = 300,
  kUpstream   = 400,
  kOther      = 0,
};

const std::vector<KinematicRegion> KinematicRegions = {
    kPNN1,
    kPNN2,
    kControl1,
    kControl2,
    kControlMu,
    kControlMu2,
    kControlMu3,
    kControl3pi,
    kControl3d,
    kK2pi,
    kKmu2,
    kK3pi,
    kUpstream,
    kOther,
};

const std::map<KinematicRegion, TString> KinematicRegionToString = {
    {kPNN1, "#pi#nu#nu1"},
    {kPNN2, "#pi#nu#nu2"},
    {kControl1, "Control1"},
    {kControl2, "Control2"},
    {kControlMu, "Control"},
    {kControlMu2, "ControlMu2"},
    {kControlMu3, "ControlMu3"},
    {kControl3pi, "Control3pi"},
    {kControl3d, "Control3d"},
    {kK2pi, "K_{2#pi}"},
    {kKmu2, "K_{#mu2}"},
    {kK3pi, "K_{3#pi}"},
    {kUpstream, "Upstream"},
    {kOther, "Other"},
};

} // namespace Pnn