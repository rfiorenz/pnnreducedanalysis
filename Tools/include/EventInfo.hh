#pragma once

#include "PnnNA62Event.hh"
#include "ReducedTree.hh"
#include <filesystem>
#include <bitset>
#include <optional>

class EventInfo {
public:
  void Update(const char *filePath, const Pnn::ReducedTree treeType, const int iEvent, PnnNA62Event *event);

  const bool IsBadBurst(std::bitset<64> mask = -1) const;
  const bool IsRealBadBurst() const;

  const std::filesystem::path GetFilePath() const;
  const std::string           GetFileName() const;
  const TString               GetRecoVersion() const;
  const TString               GetFilterVersion() const;
  const TString               GetReductionVersion() const;
  const Pnn::ReducedTree      GetTree() const;

  const bool            IsMC() const;
  const short           GetRunID() const;
  const long            GetBurstID() const;
  const int             GetEventNumber() const;
  const int             GetEventIndex() const;
  const std::bitset<64> GetBadBurstMask() const;

protected:
  PnnNA62Event *fEvent = NULL;

  mutable std::optional<std::filesystem::path> fFilePath;
  mutable std::optional<std::string>           fFileName;

  mutable std::optional<TString> fRecoVersion;
  mutable std::optional<TString> fFilterVersion;
  mutable std::optional<TString> fReductionVersion;

  mutable std::optional<Pnn::ReducedTree> fTree;

  mutable std::optional<bool>  fIsMC;
  mutable std::optional<short> fRunID;
  mutable std::optional<long>  fBurstID;
  mutable std::optional<int>   fEventNumber;
  mutable std::optional<int>   fEventIndex;

  mutable std::optional<std::bitset<64>> fBadBurstMask;
  mutable std::optional<bool>            fRealBadBurst;

  const TString   SearchTagVersion(TString tag) const;
  const long      CalculateBurstID() const;
  const bool      CheckRealBadBurst() const;
  const ULong64_t FetchBadBurstMask() const;

  short fRawRunNumber   = -1;
  short fRawBurstNumber = -1;

  // in MC, fEvent->GetBurstNumber() is only up to 1000
  static constexpr int MaxMCBurstNumber = 1000;
};