#pragma once

namespace Pnn {
struct CommandLineOptions {
  int               ExitCode  = 0;
  int               MaxNFiles = INT32_MAX, StartFile = 0;
  TString           OutputFilename = "outFile.root";
  std::stringstream Liststream;
};

CommandLineOptions ParseCommandLineOptions(int argc, char **argv);
} // namespace Pnn