#pragma once

#include <set>
#include <TDirectory.h>
#include <TTree.h>

#include "ReducedTree.hh"
#include "EventInfo.hh"
#include "HistoHandler.hh"

class ProcessMonitor {
public:
  ProcessMonitor(Pnn::ReducedTree tree, TDirectory *outDir);

  void StartOfRun(const EventInfo &eventInfo);
  void StartOfBurst(const EventInfo &eventInfo);
  void Process(const EventInfo &eventInfo);
  void EndOfBurst(const EventInfo &eventInfo);
  void EndOfRun(const EventInfo &eventInfo);

  void PrintSummary() const;

  void Write();

private:
  TTree *fTree;

  Pnn::ReducedTree fTreeType;

  HistoHandler fTreeWrapper;
  HistoHandler fHistoWrapper;

  bool      fMC;
  long      fRunID;
  long      fBurstID;
  long      fNEvents;
  ULong64_t fBadBurstMask;
  bool      fRealBadBurst;

  bool fWarning = false;

  std::set<long>                  fRunsAnalyzed;
  std::set<std::pair<long, long>> fBurstsAnalyzed;
};
