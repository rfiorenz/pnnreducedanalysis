#pragma once

#include <chrono>

class ProgressMonitor {
public:
  ProgressMonitor(const long nEntries, const int minEventsInterval = 1000, const int secondsInterval = 1, const float printInterval = 30, const float alpha = 0.1);
  void Print();
  void End();

private:
  std::chrono::steady_clock::time_point startTime;
  std::chrono::steady_clock::time_point lastTime;

  const long  nEntries;
  const int   minEventsInterval = 1000; // min n of events between estimations of speed
  const int   secondsInterval   = 1;    // seconds between estimations of speed
  const float printInterval     = 30;   // time between prints of progress
  const float alpha             = 0.1;  // smoothing factor for EMA

  long  iEntry    = 0;
  long  lastEvent = 0;
  int   nPrints   = 1;
  float speed     = 0; // events per second, calculated via EMA
};
