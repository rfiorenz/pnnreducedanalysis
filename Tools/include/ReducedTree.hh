#pragma once

#include <TString.h>
#include <map>

namespace Pnn {

enum ReducedTree {
  TPNN,
  TK2PI,
  TKMU2,
  TNORMK2PI,
  TPNN_MIN
};

const std::map<ReducedTree, TString> TreeToString = {
    {TPNN, "TPNN"},
    {TK2PI, "TK2PI"},
    {TKMU2, "TKMU2"},
    {TNORMK2PI, "TNORMK2PI"},
    {TPNN_MIN, "TPNN_MIN"},
};

} // namespace Pnn