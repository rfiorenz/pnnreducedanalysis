#pragma once

#define MKCH 0.493677
#define MPICH 0.13957039
#define MMU 0.1056583755
#define ME 0.000510998950
#define MPICH2 (MPICH * MPICH)
#define MMU2 (MMU * MMU)

#define TIMESTAMP_TO_SECONDS 24.951059536 / 1e9

#include <TVector3.h>
#include <TString.h>

namespace Pnn {
const TVector3   STRAW1Position = TVector3(101.2, 0, 183508.);
const std::array PeriodStarts   = {
    6270, 6730,   // 2016
    7610, 8290,   // 2017
    8510, 9470,   // 2018
    10780, 11170, // 2021
    11740, 12580, // 2022
    12990, 13710, // 2023
};

struct PileupTrack {
  double CDA, TGTK, TKTAG, TRICH;
};
} // namespace Pnn

namespace std {
template <>
struct hash<TString> {
  size_t operator()(const TString &v) const
  {
    return v.Hash();
  }
};

template <>
struct hash<std::pair<short, long>> {
  size_t operator()(const std::pair<short, long> &v) const
  {
    return ((ulong)v.first << 47) | v.second; // overlaps if v.second > 2^47 ~ 1e14, hope we don't have such large burstIDs...
  }
};

} // namespace std

void    ListFiles(std::string path, std::function<void(const std::string &)> cb);                              // executes cb over all files in path, recursively
void    ROOTErrorIgnoreLevel(Int_t errorIgnoreLevel, std::function<void(void)> cb);                            // sets the ROOTErrorIgnoreLevel temporarily
void    TraverseList(std::stringstream &liststream, int start, int maxlines, std::function<void(TString)> cb); // execute cb over each line of the list
TString PrependXRDProtocol(TString filepath);                                                                  // add XRootDProtocol to path
void    deep_copy_TObject(TObject *in, TObject *out, int bufsize = 32000);