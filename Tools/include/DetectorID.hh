#pragma once

#include <TString.h>
#include <map>

namespace Pnn {

enum DetectorID {
  // see NA62Global.hh -> DetectorID
  kDummy        = 0,
  kCedar        = 1,
  kGigaTracker  = 2,
  kCHANTI       = 3,
  kLAV          = 4,
  kSpectrometer = 5,
  kCHOD         = 6,
  kRICH         = 7,
  kIRC          = 8,
  kLKr          = 9,
  kMUV1         = 10,
  kMUV2         = 11,
  kMUV3         = 12,
  kSAC          = 13,
  kNewCHOD      = 14,
  kHAC          = 15,
  kL0TP         = 16,
  kL1TP         = 17,
  kL2EB         = 18,
  kDIM          = 19,
  kLKrL0        = 20, // originally called kL0Calo, renamed because of name clash with NA62::Trigger::kL0Calo
  // 21: reserved
  // 22: reserved
  // 23: reserved
  kANTI0          = 24,
  kVetoCounter    = 25,
  kMUV0           = 26,
  kSAV            = 27,
  kVetoCounterTEL = 28,
  // 29: reserved
  // 30: reserved
  kMUV0v2       = 20, // temporary: for backward compatibility with v2 bad bursts
  kSAVv2        = 21, // temporary: for backward compatibility with v2 bad bursts
  kProcessingv2 = 30, // temporary: for backward compatibility with v2 bad bursts
  kProcessing   = 31,

  // L0 systems: see NA62Global.hh -> L0DetectorID
  kL0CHOD    = 32 + 0,
  kL0RICH    = 32 + 1,
  kL0LAV     = 32 + 2,
  kL0MUV3    = 32 + 3,
  kL0NewCHOD = 32 + 4,
  kL0TALK    = 32 + 5,
  kL0ANTI0   = 32 + 5,
  kL0Calo    = 32 + 6
};

const std::map<DetectorID, TString> DetectorIDToString = {
    {kCedar, "Cedar"},
    {kGigaTracker, "GigaTracker"},
    {kCHANTI, "CHANTI"},
    {kLAV, "LAV"},
    {kSpectrometer, "Spectrometer"},
    {kCHOD, "CHOD"},
    {kRICH, "RICH"},
    {kIRC, "IRC"},
    {kLKr, "LKr"},
    {kMUV1, "MUV1"},
    {kMUV2, "MUV2"},
    {kMUV3, "MUV3"},
    {kSAC, "SAC"},
    {kNewCHOD, "NewCHOD"},
    {kHAC, "HAC"},
    {kL0TP, "L0TP"},
    {kL1TP, "L1TP"},
    {kL2EB, "L2EB"},
    {kDIM, "DIM"},
    {kLKrL0, "LKrL0"},
    {kANTI0, "ANTI0"},
    {kVetoCounter, "VetoCounter"},
    {kMUV0, "MUV0"},
    {kSAV, "SAV"},
    {kVetoCounterTEL, "VetoCounterTEL"},
    {kMUV0v2, "MUV0v2"},
    {kSAVv2, "SAVv2"},
    {kProcessingv2, "Processingv2"},
    {kProcessing, "Processing"},
    {kL0CHOD, "L0CHOD"},
    {kL0RICH, "L0RICH"},
    {kL0LAV, "L0LAV"},
    {kL0MUV3, "L0MUV3"},
    {kL0NewCHOD, "L0NewCHOD"},
    {kL0TALK, "L0TALK"},
    {kL0ANTI0, "L0ANTI0"},
    {kL0Calo, "L0Calo"},
};

} // namespace Pnn