#pragma once

#include <TH1.h>
#include <TGraph.h>
#include <vector>
#include <TMVA/Reader.h>
#include <TLine.h>

#include "HistoHandler.hh"
#include "globals.hh"
#include "Veto.hh"
#include "KinematicRegion.hh"
#include "PnnNA62Event.hh"

#define PNNSELECT(PNN, X, ...)     \
  if (!PNN.X(__VA_ARGS__)) return; \
  PNN.MonitorCut(#X);

#define PNNSELECTBOOL(PNN, X, ...)       \
  if (!PNN.X(__VA_ARGS__)) return false; \
  PNN.MonitorCut(#X);

class PnnSelection {
public:
  PnnSelection(PnnNA62Event *event, HistoHandler *histo = nullptr);
  PnnSelection(PnnNA62Event *event, HistoHandler &histo);

  template <typename T = TH1>
  T *GetHistogramTemplate(TString name) const
  {
    return dynamic_cast<T *>(fHistogramTemplates.at(name).first);
  }

  static bool DisableAutoHisto;
  bool        DisableHistoFilling = false;

  void AddMonitoringHisto(const TH1 *h, std::function<void(TH1 *)> fillingCallback);

  Pnn::KinematicRegion GetRegion() const;
  double               GetTrackMomentum() const { return fTrackMomentum; };
  double               GetRSTRAW1() const { return fRSTRAW1; };

  void MonitorCut(TString cut);

  bool TriggerMask(int bit); // bit == -1 for CTRL
  bool Vertex(bool UCS = false);
  bool CHANTI(); // only for TPNN
  bool BeamKinematics();
  bool BeamBackground();
  bool Timing();
  bool CDA();
  bool KPiAssociation();
  bool KPiAssociationBayes();
  bool BoxCut();
  bool MUV3SpatialAssociation();
  bool BroadMultivertex(bool checkMUV12 = false);
  bool RICHPresence();
  bool MUV3Veto();
  bool PionPIDCalo();
  bool PionPIDRICH();
  bool VetoCounterVeto();
  bool PhotonVeto();
  bool MultiplicityVeto();
  bool CobraVeto();
  bool ANTI0Veto();
  bool GTKExtraHits();

  double GetMmissKmu2g() const;
  bool   Kmu2gVeto();

  enum PnnVetoMode {
    kSingle,
    kSequence,
    kExclusive,
  };

  bool                      PnnVeto(Pnn::Veto veto); // true if veto!
  std::map<Pnn::Veto, bool> PnnVeto(std::vector<Pnn::Veto> vetoes, PnnVetoMode mode);

  int                  UpstreamValidationSample();
  static constexpr int NUpstreamValidationSamples = 11;

  int MaxGTKInTimeTracks = INT_MAX;

protected:
  static const int fFirstNewColRunID;

  static const std::unordered_map<TString, std::pair<TH1 *, TString>> fHistogramTemplates;

  std::map<const TH1 *, std::function<void(TH1 *)>> fMonitoringHistos;

  PnnNA62Event *fEvent;
  HistoHandler *fHisto;

  static TMVA::Reader *MakeTMVAReader(TString filename);
  static TMVA::Reader *fTMVAReader;
  static float         fTMVAVariables[10];

  template <typename... Args>
  void FillHisto(TString name, Args... args)
  {
    if (fHisto == NULL || DisableHistoFilling)
      return;

    if (DisableAutoHisto) {
      fHisto->FillHisto(name, args...);
      return;
    }

    TH1 *histo = fHisto->Get<TH1>(name);
    if (!histo) {
      std::pair<TH1 *, TString> booking = fHistogramTemplates.at(name);
      fHisto->BookHisto(booking.first->Clone(), booking.second);
      histo = fHisto->Get<TH1>(name);
    }
    histo->Fill(args...);
  }

  double fTrackMomentum;
  double fRSTRAW1;
  double fZVtx;
};