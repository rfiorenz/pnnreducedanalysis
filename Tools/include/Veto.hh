#pragma once

#include <TString.h>
#include <map>

namespace Pnn {

enum Veto {
  // photon vetoes
  kIRC,
  kSAC,
  kSAV,
  kLKr,
  kLAV,
  kPhotonVeto,
  // multiplicity vetoes
  kHAC,
  kMUV0,
  kSlabsInTime,
  kCHOD,
  kNewCHOD,
  kCHODNewCHOD,
  kLKrMerged,
  kSegment,
  kNegative,
  kMultiplicityVeto,
  // all
  kAllVetoes,
};

const std::vector<Veto> PnnVetoes = {
    // photon vetoes
    kIRC,
    kSAC,
    kSAV,
    kLKr,
    kLAV,
    kPhotonVeto,
    // multiplicity vetoes
    kHAC,
    kMUV0,
    kSlabsInTime,
    kCHOD,
    kNewCHOD,
    kCHODNewCHOD,
    kLKrMerged,
    kSegment,
    kNegative,
    kMultiplicityVeto,
    // all
    kAllVetoes,
};

const std::vector<Veto> PhotonVetoes = {
    kIRC,
    kSAC,
    kSAV,
    kLKr,
    kLAV,
    kPhotonVeto,
};

const std::vector<Veto> MultiplicityVetoes = {
    kHAC,
    kMUV0,
    kSlabsInTime,
    kCHOD,
    kNewCHOD,
    kCHODNewCHOD,
    kLKrMerged,
    kSegment,
    kNegative,
    kMultiplicityVeto,
};

const std::map<Veto, TString> VetoToString = {
    {kIRC, "IRC"},
    {kSAC, "SAC"},
    {kSAV, "SAV"},
    {kLKr, "LKr"},
    {kLAV, "LAV"},
    {kPhotonVeto, "PhotonVeto"},
    {kHAC, "HAC"},
    {kMUV0, "MUV0"},
    {kSlabsInTime, "SlabsInTime"},
    {kCHOD, "CHOD"},
    {kNewCHOD, "NewCHOD"},
    {kCHODNewCHOD, "CHODNewCHOD"},
    {kLKrMerged, "LKrMerged"},
    {kSegment, "Segment"},
    {kNegative, "Negative"},
    {kMultiplicityVeto, "MultiplicityVeto"},
    {kAllVetoes, "Total"},
};

} // namespace Pnn