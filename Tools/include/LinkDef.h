#if __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclasses;
#pragma link C++ class PnnNA62Event+;
#pragma link C++ class PnnNA62DownstreamParticle+;
#pragma link C++ class PnnNA62UpstreamParticle+;
#pragma link C++ class PnnNA62Trigger+;

#endif
