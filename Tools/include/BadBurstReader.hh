#include <TString.h>
#include <TTree.h>
#include <bitset>
#include <unordered_map>

class BadBurstReader {
public:
  // this set of three functions can be called at any time, but it is inefficient to call them multiple times per run
  static const std::bitset<64> GetBadBurstMaskFromCDB(int runID, int burstID);                // find the CDB bad burst file and parse it to search for the burstID.
  static const bool            IsBadBurst(int runID, int burstID, std::bitset<64> mask = -1); // check for bad burst, using GetBadBurstMaskFromCDB(int, int). mask: set bits to 1 if you want to check the corresponding systems (defaults to all)
  static const bool            CheckRealBadBurst(int runID, int burstID);                     // check for bad burst ignoring hardcoded subsystems, using IsBadBurst(int, int, std::bitset)

  // alternatively these functions can be used: ideally GetBadBurstsFromCDB(int) is called once per run, at the start of run, and its output is used as input any for the other two
  static const std::unordered_map<int, std::bitset<64>> GetBadBurstsFromCDB(int runID);                                                                                // find the CDB bad burst file, parse it and return all the bad bursts for this run
  static const bool                                     IsBadBurst(const std::unordered_map<int, std::bitset<64>> &badBursts, int burstID, std::bitset<64> mask = -1); // search for this burstID in the map (previously returned by GetBadBurstsFromCDB()). mask: set bits to 1 if you want to check the corresponding systems (defaults to all)
  static const bool                                     CheckRealBadBurst(const std::unordered_map<int, std::bitset<64>> &badBursts, int runID, int burstID);          // check for bad bursts ignoring hardcoded subsystems, using IsBadBurst(std::unordered_map, int, std::bitset)
};