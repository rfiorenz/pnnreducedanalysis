#pragma once

#include "HistoHandler.hh"
#include <TGraph.h>

namespace Pnn {

class KpiAssociationBayes {
public:
  KpiAssociationBayes(HistoHandler &histo, bool isRun2);

  bool                 QualityCut(double CDA, double TGTK, double TKTAG, double TRICH);
  std::pair<int, bool> FindMatch(const std::vector<PileupTrack> &event);

  const std::vector<double> NGTK;

protected:
  std::shared_ptr<HistoHandler> fHisto;

  std::vector<double> ffN;
  bool                fIsRun2;

  double fCutDeltaTsum = 4.2;
  double fCutCDA       = 48;

  TF1 *fCDA;
  TF1 *fDTsum;
  TF1 *fCDA_p;
  TF1 *fDTsum_p;

  double fRhoNorm;

  double Likelihood(const PileupTrack &track, double prior, double normalization);
};

} // namespace Pnn