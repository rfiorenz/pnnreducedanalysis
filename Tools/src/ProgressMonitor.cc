#include "ProgressMonitor.hh"

#include <chrono>
#include <TString.h>
#include <iostream>

using namespace std::chrono;

ProgressMonitor::ProgressMonitor(const long nEntries, const int minEventsInterval, const int secondsInterval, const float printInterval, const float alpha)
    : startTime(steady_clock::now()), lastTime(startTime), nEntries(nEntries),
      minEventsInterval(minEventsInterval), secondsInterval(secondsInterval), printInterval(printInterval), alpha(alpha)
{
  time_t  time_c  = time(NULL);
  TString timeStr = ctime(&time_c);
  std::cout << "[" << timeStr(0, timeStr.Index("\n")) << "] Begin processing " << nEntries << " entries" << std::endl;
}

void ProgressMonitor::Print()
{
  iEntry++;

  if (iEntry % minEventsInterval != 0)
    return;

  auto  now        = steady_clock::now();
  float timePassed = duration_cast<milliseconds>(now - lastTime).count() * 1e-3;
  if (timePassed < secondsInterval)
    return;

  speed     = (1 - alpha) * speed + alpha * (iEntry - lastEvent) / timePassed; // EMA formula
  lastTime  = now;
  lastEvent = iEntry;

  auto elapsedSeconds = duration_cast<seconds>(now - startTime).count();
  if (elapsedSeconds / printInterval < nPrints) // print progress only every printInterval
    return;

  time_t  time_c  = time(NULL);
  TString timeStr = ctime(&time_c);
  std::cout << "[" << timeStr(0, timeStr.Index("\n")) << "] "                     // time now
            << "Processing entry " << iEntry << "/" << nEntries << " "            // done / total
            << "(" << (int)(iEntry * 100. / nEntries) << "%) "                    // % done
            << "Elapsed: " << elapsedSeconds << "s "                              // elapsed time
            << "ETA: " << (int)((nEntries - iEntry) / speed) << "s" << std::endl; // ETA
  nPrints++;
}

void ProgressMonitor::End()
{
  time_t  time_c  = time(NULL);
  TString timeStr = ctime(&time_c);

  auto now            = steady_clock::now();
  auto elapsedSeconds = duration_cast<seconds>(now - startTime).count();
  std::cout << "[" << timeStr(0, timeStr.Index("\n")) << "] End processing. Total time: " << elapsedSeconds << "s" << std::endl;
}
