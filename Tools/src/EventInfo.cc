#include "EventInfo.hh"
#include <bitset>
#include <iostream>
#include <regex>
#include <TMethodCall.h>

#include "DetectorID.hh"

void EventInfo::Update(const char *filePath, const Pnn::ReducedTree treeType, const int iEvent, PnnNA62Event *event)
{
  // check what changed. if something changed, just reset the fields that depend on that.
  bool fileChanged  = !fFilePath || *fFilePath != filePath;
  bool burstChanged = !fEvent || fRawBurstNumber != event->GetBurstNumber();
  bool runChanged   = !fEvent || fRawRunNumber != event->GetRunNumber();

  fEvent = event;
  if (burstChanged) fRawBurstNumber = event->GetBurstNumber();
  if (runChanged) fRawRunNumber = event->GetRunNumber();

  if (fileChanged) fFilePath = filePath;
  if (fileChanged) fFileName.reset();

  if (fileChanged) fRecoVersion.reset();
  if (fileChanged) fFilterVersion.reset();
  if (fileChanged) fReductionVersion.reset();

  fTree = treeType; // meh, update it anyway

  if (runChanged) fIsMC.reset();
  if (runChanged) fRunID.reset();
  if (burstChanged || fileChanged) fBurstID.reset();
  fEventNumber.reset(); // always changes!
  fEventIndex = iEvent; // this is the only chance to get it

  if (burstChanged || runChanged || fileChanged) fBadBurstMask.reset();
  if (burstChanged || runChanged || fileChanged) fRealBadBurst.reset();
}

const TString EventInfo::SearchTagVersion(TString tag) const
{
  static const std::regex rgx("_" + tag + "\\.(\\w[\\d\\.]+)");
  std::string             filename = GetFileName();
  std::smatch             match;
  if (std::regex_search(filename, match, rgx))
    return TString(match[1]);
  else
    return "";
}

const std::filesystem::path EventInfo::GetFilePath() const
{
  if (!fFilePath) // should really be defined when accessing it
    throw std::runtime_error("Accessing EventInfo::fFilePath when not defined yet");

  return *fFilePath;
}

const std::string EventInfo::GetFileName() const
{
  if (!fFileName) fFileName = GetFilePath().filename();
  return *fFileName;
}

const TString EventInfo::GetRecoVersion() const
{
  if (!fRecoVersion) fRecoVersion = SearchTagVersion("p");
  return *fRecoVersion;
}

const TString EventInfo::GetFilterVersion() const
{
  if (!fFilterVersion) fFilterVersion = SearchTagVersion("f");
  return *fFilterVersion;
}
const TString EventInfo::GetReductionVersion() const
{
  if (!fReductionVersion) fReductionVersion = SearchTagVersion("pnn");
  return *fReductionVersion;
}

const Pnn::ReducedTree EventInfo::GetTree() const
{
  if (!fTree) // should really be defined when accessing it
    throw std::runtime_error("Accessing EventInfo::fTree when not defined yet");

  return *fTree;
}

const bool EventInfo::IsMC() const
{
  if (!fIsMC) fIsMC = fEvent->GetRunNumber() < 0;
  return *fIsMC;
}

const short EventInfo::GetRunID() const
{
  if (!fRunID) fRunID = abs(fEvent->GetRunNumber());
  return *fRunID;
}

const long EventInfo::CalculateBurstID() const
{
  long burstID = fEvent->GetBurstNumber();

  return burstID; // hotfix: until we use proper filenames for MC, all the rest is pointless...

  // in MC, fEvent->GetBurstNumber() is only up to 1000: get the full burstID (= seed) from the filename
  if (!IsMC() || burstID >= MaxMCBurstNumber)
    return burstID;

  std::smatch match;
  std::string fname = GetFileName();
  TString     runID = TString::Format("%06d", fRunID);
  if (std::regex_search(fname, match, std::regex(runID + "-(\\d+).root"))) { // is there just one single seed in the filename?
    long burstInFile = TString(match[1]).Atoll();
    return (burstInFile % MaxMCBurstNumber == burstID) ? burstInFile : burstID; // if not consistent with the one we got from the event, don't touch it
  }

  if (std::regex_search(fname, match, std::regex(runID + "-(\\d+)-(\\d+).root"))) {
    long minBurstInFile = TString(match[1]).Atoll();
    long maxBurstInFile = TString(match[2]).Atoll();
    if (maxBurstInFile < minBurstInFile)
      return burstID; // surrender, don't touch it

    // we know the range where the burstID is. the next formulae calculate the first and the last number in the range that end in burstID
    long minPossibleBurst = MaxMCBurstNumber * (minBurstInFile / MaxMCBurstNumber + (minBurstInFile % MaxMCBurstNumber > burstID)) + burstID;
    long maxPossibleBurst = MaxMCBurstNumber * (maxBurstInFile / MaxMCBurstNumber + (maxBurstInFile % MaxMCBurstNumber > burstID) - 1) + burstID;

    // if these two values coincide, that's it! otherwise, just don't touch it
    return (minPossibleBurst == maxPossibleBurst) ? minPossibleBurst : burstID;
  }

  return burstID; // surrender, don't touch it
}

const long EventInfo::GetBurstID() const
{
  if (!fBurstID) fBurstID = CalculateBurstID();
  return *fBurstID;
}

const int EventInfo::GetEventNumber() const
{
  if (!fEventNumber) fEventNumber = fEvent->GetEventNumber();
  return *fEventNumber;
}

const int EventInfo::GetEventIndex() const
{
  if (!fEventIndex) // should really be defined when accessing it
    throw std::runtime_error("Accessing EventInfo::fEventIndex when not defined");

  return *fEventIndex;
}

const std::bitset<64> EventInfo::GetBadBurstMask() const
{
  if (!fBadBurstMask) fBadBurstMask = FetchBadBurstMask();
  return *fBadBurstMask;
}

const ULong64_t EventInfo::FetchBadBurstMask() const
{
  return fEvent->GetBadBurstMask();

  // // super ugly code to be able to compile against old versions
  // if (fEvent->Class_Version() < 2)
  //   return 0;
  // TMethodCall *mc = new TMethodCall(fEvent->Class(), "GetBadBurstMask", "");
  // char        *ret;
  // mc->Execute(fEvent, "", &ret);
  // if (fEvent->Class_Version() == 2)
  //   return *(uint *)ret;
  // return *(ULong64_t *)ret;
}

const bool EventInfo::IsBadBurst(std::bitset<64> mask) const
{
  return (GetBadBurstMask() & mask).any();
}

const bool EventInfo::CheckRealBadBurst() const
{
  std::bitset<64> ignoreMask;

  if (GetRunID() < 10000) { // Run1
    ignoreMask.set(Pnn::kVetoCounterTEL);
  } else { // Run2
    ignoreMask.set(Pnn::kL0CHOD);
    ignoreMask.set(Pnn::kL0LAV);
  }

  static const std::vector ignoredSystems = {
      Pnn::kVetoCounter,
      Pnn::kANTI0,
      Pnn::kL0ANTI0,
      Pnn::kMUV0,
      Pnn::kSAV,
  };

  for (auto &ignoredSystem : ignoredSystems)
    ignoreMask.set(ignoredSystem);

  return IsBadBurst(~ignoreMask);
}

const bool EventInfo::IsRealBadBurst() const
{
  if (!fRealBadBurst) fRealBadBurst = CheckRealBadBurst();
  return *fRealBadBurst;
}
