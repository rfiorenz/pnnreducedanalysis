#include "KpiAssociationBayes.hh"
#include "PnnNA62Event.hh"
#include "PnnSelection.hh"
#include <TF1.h>

#define DTMAX 4.2
#define DTBIN 0.1
#define TIMEBINNING round(2 * DTMAX / DTBIN), -DTMAX, DTMAX

#define MAXCDA 4
#define CDABINNING 25, 0, MAXCDA

using namespace Pnn;

double evalNormalized(TF1 *func, double x)
{
  // work around ROOT behaviour: after TF1::SetNormalized, TF1::Eval is not normalized, but TF1::EvalPar is.
  double arr[] = {x};
  return func->GetXmin() <= x && x <= func->GetXmax() ? func->EvalPar(arr) : 0.;
};

const std::vector<double> NGTK_Run2 = {
    // distribution extracted from 2022ABCDE-t0.143.1
    0,
    0.0745159,
    0.149471,
    0.180337,
    0.162441,
    0.124153,
    0.087607,
    0.0599379,
    0.0411782,
    0.0291402,
    0.0212237,
    0.0156127,
    0.0114707,
    0.00847414,
    0.0063082,
    0.00478746,
    0.00374091,
    0.00308143,
    0.00260411,
    0.00217327,
    0.00176295,
    0.00142273,
    0.00116931,
    0.000958367,
    0.000800433,
    0.000679743,
    0.00494911,
};

const std::vector<double> NGTK_Run1 = {
    0,
    0.174439,
    0.250229,
    0.218383,
    0.146466,
    0.0857714,
    0.0478715,
    0.0271265,
    0.016465,
    0.0108158,
    0.00736276,
    0.00493249,
    0.00327541,
    0.00220258,
    0.00152095,
    0.00105716,
    0.000768558,
    0.000563989,
    0.000421704,
    0.000327579,
    0.00132707,
};

KpiAssociationBayes::KpiAssociationBayes(HistoHandler &histo, bool isRun2)
    : fHisto(histo.MakeChild("KpiAssociationBayes")), NGTK(isRun2 ? NGTK_Run2 : NGTK_Run1), fIsRun2(isRun2)
{
  fHisto->BookHisto(new TH1F("logrho_alltracks", "", 720, 0, 18));
  fHisto->BookHisto(new TH1F("logrho_1trackEvents", "", 720, 0, 18));
  fHisto->BookHisto(new TH2F("logRvslogrho1", ";log#rho_{1};logR", 720, 0, 18, 720, 0, 18));
  fHisto->BookHisto(new TH2F("MatchCDA", "", CDABINNING, TIMEBINNING));
  fHisto->BookHisto(new TH2F("MatchlogrhovsDTsum", "", 720, 0, 18, TIMEBINNING));

  auto ExpPolFormula =
      [](int n) { // generate a TFormula for exp(polN), which is not supported in ROOT...
        TString pol = "[0]";
        for (int i = 1; i <= n; i++) {
          pol += Form("+[%d]*x[0]", i);
          for (int j = 1; j < i; j++)
            pol += "*x[0]";
        }
        return Form("exp(%s)", pol.Data());
      };

  fCDA   = new TF1("fCDA", ExpPolFormula(8), 0., fCutCDA);
  fDTsum = new TF1("fDTsum", "[0]*exp(-0.5*(x/[1])*(x/[1])) + [2]*exp(-0.5*(x/[3])*(x/[3]))",
                   -fCutDeltaTsum, fCutDeltaTsum);
  fCDA_p =
      new TF1("fCDA_p", "[0]*exp(-0.5*((x-[1])/[2])*((x-[1])/[2])) + [3]*exp(-0.5*(x/[4])*(x/[4]))",
              0., fCutCDA);
  fDTsum_p = new TF1("fDTsum_p", "1", -fCutDeltaTsum, fCutDeltaTsum); // constant

  std::vector<double> kCDAPars, kDTPars, puCDAPars;
  if (!isRun2) { // Run1
    kCDAPars  = {-0.75533, -0.223585, -0.151597, 0.0249934, -0.00183792,
                 7.27183e-05, -1.60124e-06, 1.85034e-08, -8.75735e-11};
    kDTPars   = {0.0680406, 0.323426, 2.19939, 0.171417};
    puCDAPars = {0.00606457, 20.1037, -10.2912, 0.0609595, 11.0932};
    ffN       = {0, 0.91163, 0.473901, 0.31975, 0.240841, 0.192821, 0.160505,
                 0.137295, 0.119865, 0.106559, 0.0958382, 0.0868998, 0.0792526, 0.0730387,
                 0.0675292, 0.0626595, 0.0582096, 0.0545038, 0.0513155, 0.0485085, 0.0461126,
                 0.0434676, 0.0412239, 0.0394455, 0.0374019, 0.0350724};
  } else { // Run2
    kCDAPars  = {-0.88885263, 7.70612e-02, -3.07616e-01, 5.86224e-02, -5.57705e-03,
                 3.02980e-04, -9.52623e-06, 1.60906e-07, -1.12657e-09};
    kDTPars   = {1.74580e-1, 2.42553e-1, 2.55474, 1.39522e-1};
    puCDAPars = {4.82871e-03, 2.10587e+01, -9.24290e+00, 6.27752e-02, 1.12923e+01};
    ffN       = {0, 0.91531, 0.470875, 0.316924, 0.23848, 0.190434, 0.157975,
                 0.134489, 0.116851, 0.102869, 0.0921135, 0.0830119, 0.075865, 0.0702627,
                 0.0646351, 0.0599477, 0.0561655, 0.05293, 0.0500939, 0.0474973, 0.0444903,
                 0.0422693, 0.0404494, 0.0384391, 0.036631, 0.0347892};
  }

  auto setPDFParameters = [](TF1 *f, const std::vector<double> &pars) {
    if (size_t(f->GetNpar()) != pars.size())
      throw std::logic_error(
          Form("%d parameters were expected for function %s, but %zu were supplied!", f->GetNpar(),
               f->GetName(), pars.size()));
    f->SetParameters(pars.data());
    f->SetNormalized(true);
  };

  setPDFParameters(fCDA, kCDAPars);
  setPDFParameters(fDTsum, kDTPars);
  setPDFParameters(fCDA_p, puCDAPars);
  setPDFParameters(fDTsum_p, {});

  auto getMaximumRatio = [&](TF1 *num, TF1 *den) {
    // calculate max(num(x) / den(x)) where num(x) and den(x) are normalized functions.
    return TF1(
               "tmp",
               [&](double *x, double *) {
                 return evalNormalized(num, x[0]) / evalNormalized(den, x[0]);
               },
               std::max(num->GetXmin(), den->GetXmin()), std::min(num->GetXmax(), den->GetXmax()), 0)
        .GetMaximum();
  };

  double maxfN = *std::max_element(ffN.begin(), ffN.end());
  fRhoNorm     = getMaximumRatio(fCDA, fCDA_p) * getMaximumRatio(fDTsum, fDTsum_p) * maxfN / (1 - maxfN);
}

bool KpiAssociationBayes::QualityCut(double CDA, double TGTK, double TKTAG, double TRICH)
{
  double deltaT1 = TRICH - TGTK;
  double deltaT2 = TKTAG - TGTK;
  double sum     = (deltaT1 + deltaT2) / sqrt(2.);
  return fabs(sum) <= fCutDeltaTsum && CDA < fCutCDA;
};

double KpiAssociationBayes::Likelihood(const PileupTrack &track, double prior, double normalization)
{
  double cda     = track.CDA;
  double deltaT1 = track.TRICH - track.TGTK;
  double deltaT2 = track.TKTAG - track.TGTK;
  double sum     = (deltaT1 + deltaT2) / sqrt(2.);
  double dif     = (deltaT1 - deltaT2) / sqrt(2.);

  double Dkaon   = evalNormalized(fCDA, cda) * evalNormalized(fDTsum, sum) * prior;
  double Dpileup = evalNormalized(fCDA_p, cda) * evalNormalized(fDTsum_p, sum) * (1. - prior);
  return log(normalization) + log(Dpileup / Dkaon); // log likelihood ratio
}

std::pair<int, bool> KpiAssociationBayes::FindMatch(const std::vector<PileupTrack> &event)
{
  int    nGTK  = event.size();
  double prior = size_t(nGTK) < ffN.size() ? ffN.at(nGTK) : ffN.back();

  std::vector<std::pair<int, double>> likelihoods;
  likelihoods.reserve(nGTK);
  for (int i = 0; i < nGTK; i++) {
    likelihoods.push_back({i, Likelihood(event[i], prior, fRhoNorm)});
  }

  assert(event.size() == likelihoods.size());

  std::nth_element(likelihoods.begin(), likelihoods.begin() + 1, likelihoods.end(),
                   [&](std::pair<int, double> l1, std::pair<int, double> l2) {
                     return l1.second < l2.second;
                   }); // get the best two tracks

  double logrho1 = likelihoods[0].second;                                                      // best likelihood
  double logrho2 = nGTK > 1 ? likelihoods[1].second : std::numeric_limits<double>::infinity(); // second best likelihood
  double logR    = nGTK > 1 ? logrho2 - logrho1 : -1;

  // ask PnnSelection whether the event passed
  auto na62Event        = new PnnNA62Event();
  auto upstreamParticle = new PnnNA62UpstreamParticle();
  upstreamParticle->SetNGTKTracks(nGTK);
  na62Event->SetUpstreamParticle(upstreamParticle);
  na62Event->SetIsKTAGBeamDiscriminant(logrho1);
  na62Event->SetIsSecondBestBeamDiscriminant(logrho2);
  na62Event->SetRunNumber(fIsRun2 ? 12000 : 8000);
  PnnSelection pnn(na62Event);
  bool         passed = pnn.KPiAssociationBayes();
  delete na62Event; // upstreamParticle is already deleted inside of here

  auto &track = event[likelihoods[0].first]; // matched track

  if (track.CDA < MAXCDA) { // good event, can fill plots
    for (auto &l : likelihoods)
      fHisto->FillHisto("logrho_alltracks", l.second);
    if (nGTK == 1)
      fHisto->FillHisto("logrho_1trackEvents", logrho1);
    else
      fHisto->FillHisto("logRvslogrho1", logrho1, logR);

    double dtsum = ((track.TGTK - track.TKTAG) + (track.TGTK - track.TRICH)) / sqrt(2.);
    fHisto->FillHisto("MatchCDA", track.CDA, dtsum);
    fHisto->FillHisto("MatchlogrhovsDTsum", likelihoods[0].second, dtsum);
  }

  return {likelihoods[0].first, passed};
}
