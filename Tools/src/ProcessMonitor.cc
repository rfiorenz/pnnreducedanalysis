#include "ProcessMonitor.hh"

#include <iostream>
#include <bitset>

#include "DetectorID.hh"

ProcessMonitor::ProcessMonitor(Pnn::ReducedTree treeType, TDirectory *outDir)
    : fTreeType(treeType), fTreeWrapper(outDir, "BurstsProcessed"), fHistoWrapper(outDir, "BadBursts")
{
  fTree = new TTree(Pnn::TreeToString.at(treeType), "");
  fTree->Branch("MC", &fMC);
  fTree->Branch("RunID", &fRunID);
  fTree->Branch("BurstID", &fBurstID);
  fTree->Branch("NEvents", &fNEvents);
  fTree->Branch("BadBurstMask", &fBadBurstMask);
  fTree->Branch("RealBadBurst", &fRealBadBurst);

  fTreeWrapper.BookHisto(fTree);
  for (TString mode : {"inclusive", "exclusive"}) {
    auto h = new TH1F(Pnn::TreeToString.at(treeType) + "_BadBursts_" + mode, Pnn::TreeToString.at(treeType) + " bad bursts, " + mode, 64, 0, 64);
    for (auto &kv : Pnn::DetectorIDToString)
      h->GetXaxis()->SetBinLabel(kv.first + 1, kv.second);
    fHistoWrapper.BookHisto(h, "../BadBursts", "BB" + mode);
  }
}

void ProcessMonitor::StartOfRun(const EventInfo &eventInfo)
{
  fRunID = eventInfo.GetRunID();

  if (!fRunsAnalyzed.insert(fRunID).second) {
    fWarning = true;
    std::cout << "WARNING: Restarting run " << fRunID << "!" << std::endl;
  }
}

void ProcessMonitor::StartOfBurst(const EventInfo &eventInfo)
{
  fBurstID = eventInfo.GetBurstID();
  fMC      = eventInfo.IsMC();
  fNEvents = 0;

  if (!eventInfo.IsMC() && // hotfix: disable this for MC. if we're not using proper filenames this is pointless...
      !fBurstsAnalyzed.insert({fRunID, fBurstID}).second) {
    fWarning = true;
    std::cout << "WARNING: Restarting burst " << fRunID << "-" << fBurstID << "!" << std::endl;
  }
}

void ProcessMonitor::Process(const EventInfo &eventInfo)
{
  fNEvents++;
}

void ProcessMonitor::EndOfBurst(const EventInfo &eventInfo)
{
  std::bitset bbmask = eventInfo.GetBadBurstMask();
  fBadBurstMask      = bbmask.to_ullong();
  fRealBadBurst      = eventInfo.IsRealBadBurst();
  fTree->Fill();

  bool isExclusive = bbmask.count() == 1;
  for (int i = 0; i < bbmask.size(); i++)
    if (bbmask[i]) {
      fHistoWrapper.FillHisto("BBinclusive", i);
      if (isExclusive) {
        fHistoWrapper.FillHisto("BBexclusive", i);
        break; // don't waste any other time
      }
    }
}

void ProcessMonitor::EndOfRun(const EventInfo &eventInfo)
{
}

void ProcessMonitor::PrintSummary() const
{
  std::cout << "Found " << fRunsAnalyzed.size() << " runs, " << fBurstsAnalyzed.size() << " bursts" << std::endl;
  if (fWarning)
    std::cout << "WARNING: Consistency check failed!" << std::endl;
}

void ProcessMonitor::Write()
{
  fTreeWrapper.Write();
  fHistoWrapper.Write();
}
