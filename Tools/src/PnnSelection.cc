#include "PnnSelection.hh"
#include <iostream>
#include <bitset>
#include <set>
#include <TH2F.h>
#include <TLorentzVector.h>
#include <TLine.h>

bool PnnSelection::DisableAutoHisto = false;

const int PnnSelection::fFirstNewColRunID = 8800; // 8500-8800: 2018-ABC (Old collimator)
                                                  // 8800-9500: 2018-DEFGH (New collimator)

float PnnSelection::fTMVAVariables[10];

TMVA::Reader *PnnSelection::MakeTMVAReader(TString filename)
{
  const char *inputVars[] = {"Xtrim5", "Ytrim5", "Xstraw1", "Ystraw1", "Rstraw1", "Xvtx", "Yvtx", "Zvtx", "ThetaX", "ThetaY"};
  auto        reader      = new TMVA::Reader("Silent");
  for (int i = 0; i < 10; i++)
    reader->AddVariable(TString(inputVars[i]), &fTMVAVariables[i]);
  reader->BookMVA("myBDT", filename);
  return reader;
}

TMVA::Reader *PnnSelection::fTMVAReader = MakeTMVAReader("/afs/cern.ch/work/f/fbriziol/public/BDTweights_june.xml");

const std::map<const TH1 *, std::function<void(TH1 *, PnnSelection *, PnnNA62Event *)>> DefaultMonitoringHistos = {
    {new TH2F("mmiss_p", ";p (GeV/c);m^{2}_{miss} (GeV^{2}/c^{4})", 100, 0, 100, 900, -0.3, 0.15), [](TH1 *h, PnnSelection *pnn, PnnNA62Event *event) { h->Fill(pnn->GetTrackMomentum(), event->GetMMiss()); }},
    {new TH2F("mmissmu_p", ";p (GeV/c);m^{2}_{miss} (GeV^{2}/c^{4})", 100, 0, 100, 900, -0.3, 0.15), [](TH1 *h, PnnSelection *pnn, PnnNA62Event *event) { h->Fill(pnn->GetTrackMomentum(), event->GetMmuMiss()); }},
    {new TH2F("p_zvtx", ";Z_{vtx} (mm);p (GeV/c)", 250, 50000, 200000, 200, 0, 100), [](TH1 *h, PnnSelection *pnn, PnnNA62Event *event) { h->Fill(event->GetVertex().Z(), pnn->GetTrackMomentum()); }},
    {new TH2F("rSTRAW1_zvtx", ";Z_{vtx} (mm);R_{STRAW1} (mm)", 250, 50000, 200000, 120, 0, 1200), [](TH1 *h, PnnSelection *pnn, PnnNA62Event *event) { h->Fill(event->GetVertex().Z(), pnn->GetRSTRAW1()); }},
    {new TH2F("intensity_timestamp", ";Timestamp (s);#lambda_{GTK} (MHz)", 200, 0, 6.5, 120, 0, 5400), [](TH1 *h, PnnSelection *pnn, PnnNA62Event *event) { h->Fill(event->GetTimestamp() * TIMESTAMP_TO_SECONDS, event->GetLambdaFW()); }},
};

const std::unordered_map<TString, std::pair<TH1 *, TString>> PnnSelection::fHistogramTemplates = [] {
  std::unordered_map<TString, std::vector<TH1 *>> histos = {
      {"Timing",
       {
           new TH1F("dtGTKCHOD", "T_{GTK} - T_{CHOD};#DeltaT (ns)", 150, -3, 3),
           new TH1F("dtKTAGRICH", "T_{KTAG} - T_{RICH};#DeltaT (ns)", 150, -3, 3),
           new TH1F("dtGTKKTAG", "T_{GTK} - T_{KTAG};#DeltaT (ns)", 150, -3, 3),
           new TH1F("dtGTKRICH", "T_{GTK} - T_{RICH};#DeltaT (ns)", 150, -3, 3),
       }},
      {"BeamBackground",
       {
           [] {
             TH1F *h = new TH1F("BeamBackground", "", 6, 0, 6);
             h->GetXaxis()->SetBinLabel(1, "Input");
             h->GetXaxis()->SetBinLabel(2, "BeamKinematics");
             h->GetXaxis()->SetBinLabel(3, "Interaction");
             h->GetXaxis()->SetBinLabel(4, "HighToT");
             h->GetXaxis()->SetBinLabel(5, "GTKInTimeTracks");
             h->GetXaxis()->SetBinLabel(6, "GTKExtraHits");
             return h;
           }(),
           new TH1F("GTKInTimeTracks", "Number of in-time GTK Candidates", 26, -0.5, 25.5),
       }},
      {"CDA",
       {
           new TH1F("CDA", "CDA;mm", 400, 0, 40),
       }},
      {"KPiAssociation",
       {
           new TH2F("beamDiscriminant", "Beam discriminant;KTAG discriminant;RICH discriminant", 200, 0, 1, 200, 0, 1),
       }},
      {"BoxCut",
       {
           new TH2F("TRIM5", "Track position extrapolated at TRIM5;X (mm); Y(mm)", 100, -1000, 1000, 100, -1000, 1000),
           new TH2F("trackTheta", "Track slope;#theta_{X};#theta_{Y}", 400, -0.02, 0.02, 400, -0.02, 0.02),
           new TH1F("MVAOutput", "MVA output", 100, 0, 1),
           new TH2F("TRIM5_after", "Track position extrapolated at TRIM5;X (mm); Y(mm)", 100, -1000, 1000, 100, -1000, 1000),
           new TH2F("trackTheta_after", "Track slope;#theta_{X};#theta_{Y}", 400, -0.02, 0.02, 400, -0.02, 0.02),
       }},
      {"KPiAssociationBayes",
       {
           new TH1F("logrho_1trackEvents", "", 720, 0, 18),
           new TH2F("logRvslogrho1", ";log#rho_{1};logR", 720, 0, 18, 720, 0, 18),
       }},
      {"VetoCounterVeto",
       {
           new TH1F("dtVCKTAG", ";#T_{VC} - T_{KTAG} (ns)", 200, -4, 4),
       }},
      {"PionPIDCalo", {
                          [] {
                            TH1F *h = new TH1F("PionPIDCaloSelection", "", 5, 0, 5);
                            h->GetXaxis()->SetBinLabel(1, "Input");
                            h->GetXaxis()->SetBinLabel(2, "MUV veto");
                            h->GetXaxis()->SetBinLabel(3, "E/p veto");
                            h->GetXaxis()->SetBinLabel(4, "EM cluster veto");
                            h->GetXaxis()->SetBinLabel(5, "Pion prob");
                            return h;
                          }(),
                      }},
      {"PionPIDRICH", {
                          [] {
                            TH1F *h = new TH1F("PionPIDRICHSelection", "", 6, 0, 6);
                            h->GetXaxis()->SetBinLabel(1, "Input");
                            h->GetXaxis()->SetBinLabel(2, "Prob0");
                            h->GetXaxis()->SetBinLabel(3, "Prob1");
                            h->GetXaxis()->SetBinLabel(4, "Prob2");
                            h->GetXaxis()->SetBinLabel(5, "Prob4");
                            h->GetXaxis()->SetBinLabel(6, "RICHMass");
                            return h;
                          }(),
                      }}};

  std::unordered_map<TString, std::pair<TH1 *, TString>> result;
  for (const auto &x : histos) {
    TString dirname = x.first;
    for (const auto &histo : x.second) {
      if (!result.insert({histo->GetName(), {histo, dirname}}).second)
        std::cout << "[PnnSelection] WARNING: The histogram list contains multiple histos with name " << histo->GetName() << std::endl;
    }
  }
  return result;
}();

PnnSelection::PnnSelection(PnnNA62Event *event, HistoHandler *histo)
    : fEvent(event),
      fHisto(histo), fTrackMomentum(fEvent->GetDownstreamParticle()->GetP().Mag()), fRSTRAW1((TVector2(fEvent->GetDownstreamParticle()->GetPosStraw1X(), fEvent->GetDownstreamParticle()->GetPosStraw1Y()) - Pnn::STRAW1Position.XYvector()).Mod()), fZVtx(fEvent->GetVertex().Z())
{
  for (auto &[hTemplate, cb] : DefaultMonitoringHistos)
    AddMonitoringHisto(hTemplate, std::bind(cb, std::placeholders::_1, this, fEvent));
}

PnnSelection::PnnSelection(PnnNA62Event *event, HistoHandler &histo)
    : PnnSelection(event, &histo) {};

void PnnSelection::AddMonitoringHisto(const TH1 *h, std::function<void(TH1 *)> fillingCallback)
{
  fMonitoringHistos.emplace(h, fillingCallback);
};

void PnnSelection::MonitorCut(TString cut)
{
  if (fHisto == NULL)
    return;

  TH1 *selectionHisto = fHisto->Get<TH1>("selection");
  if (!selectionHisto) {
    fHisto->BookHisto(new TH1F("selection", "", 1, 0, 1));
    selectionHisto = fHisto->Get<TH1>("selection");
    selectionHisto->SetOption("hist text0");
  }
  selectionHisto->Fill(cut, 1);
  selectionHisto->LabelsDeflate();

  for (auto &[histoTemplate, fillingCallback] : fMonitoringHistos) {
    TString histoName = cut + histoTemplate->GetName();
    TH1    *histo     = fHisto->Get<TH1>(histoName);
    if (!histo) {
      fHisto->BookHisto(histoTemplate->Clone(), cut, histoName);
      histo = fHisto->Get<TH1>(histoName);
    }

    fillingCallback(histo);
  }
}

Pnn::KinematicRegion PnnSelection::GetRegion() const
{
  double momentum     = fTrackMomentum;
  double mmiss        = fEvent->GetMMiss();
  double mmissRICH    = fEvent->GetMMissRich();
  double mmissNominal = fEvent->GetMMissNominalK();
  double bound        = (MPICH2 - MMU2) * (1. - 75. / momentum);

  // WARNING: these if's may not be commutative...

  if ((momentum <= 20 &&
       ((mmiss > 0 && mmiss <= 0.01) && (mmissNominal > -0.005 && mmissNominal <= 0.0135) && (mmissRICH > 0. && mmissRICH <= 0.01))) ||
      (20 < momentum && momentum <= 25) &&
          ((mmiss > 0 && mmiss <= 0.01) && (mmissNominal > -0.005 && mmissNominal <= 0.0135) && (mmissRICH > 0. && mmissRICH <= 0.02)) ||
      (25 < momentum && momentum <= 35) &&
          ((mmiss > 0 && mmiss <= 0.01) && (mmissNominal > 0 && mmissNominal <= 0.0135) && (mmissRICH > -0.005 && mmissRICH <= 0.02)))
    return Pnn::kPNN1;

  if (mmiss <= 0.068 && mmiss > 0.026 && (mmissNominal > 0.024 && mmissNominal <= 0.068) && (mmissRICH > 0.02 && mmissRICH <= 0.07))
    return Pnn::kPNN2;

  if (mmiss > 0.01 && mmiss <= 0.015)
    return Pnn::kControl1;

  if (mmiss > 0.021 && mmiss <= 0.026)
    return Pnn::kControl2;

  if ((bound + 0.0036) < mmiss && mmiss <= std::min(bound + 0.0072, 0.))
    return Pnn::kControlMu;

  if (mmiss <= 0. && mmiss > (bound + 0.0072))
    return Pnn::kControlMu2;

  if ((momentum > 35) && (mmiss > 0 && mmiss <= 0.01))
    return Pnn::kControlMu3;

  if (mmiss > 0.068 && mmiss <= 0.072)
    return Pnn::kControl3pi;

  if ((mmiss > 0 && mmiss <= 0.01) || (mmiss <= 0.068 && mmiss > 0.026))
    return Pnn::kControl3d;

  if (mmiss > 0.015 && mmiss <= 0.021)
    return Pnn::kK2pi;

  if (mmiss > bound - 0.0036 && mmiss <= bound + 0.0036)
    return Pnn::kKmu2;

  if (mmiss > 0.072)
    return Pnn::kK3pi;

  if (mmiss <= -0.05)
    return Pnn::kUpstream;

  return Pnn::kOther; // only between Kmu2Region and UpstreamRegion
}

bool PnnSelection::TriggerMask(int bit)
{
  if (fEvent->GetRunNumber() == 12165) // low intensity run
    return false;

  if (fEvent->GetRunNumber() < 0) // TODO: abstract this! (EventInfo is no use...)!
    return true;                  // MC is always ok

  if (fEvent->GetRunNumber() > 10000 && fEvent->GetRunNumber() < 11500 && fEvent->GetTimestamp() * 1e-9 * 24.951059536 < 2)
    return false;

  if (!(-1 <= bit && bit <= 2)) {
    std::cout << "[TriggerMask] WARNING: Bit " << bit << "is an unknown trigger mask!" << std::endl;
    return false;
  }
  if (fEvent->GetRunNumber() < 10000) // Run1
    switch (bit) {
    case -1: // CTRL
    case 0:  // mask0 = normalization mask = CTRL in Run1
    case 2:  // mask2 = minbias mask = CTRL in Run1
      return fEvent->GetTriggerMask() % 2 == 0;
    case 1: // PNN
      return fEvent->GetTriggerMask() != 0;
    default: // should never happen
      return false;
    }
  else // Run2
    return std::bitset<4>(fEvent->GetTriggerMask()).test(bit == -1 ? 0 : 3 - bit);
}

bool PnnSelection::Vertex(bool UCS)
{
  // RSTRAW1 cut (trapezoid sides): dependent on momentum
  static const std::array pBinEdges = {20, 25, 30, 35, 40};
  static const std::array qu        = {1794.62, 1632.69, 1473.85, 1301.92, 1143.08, 1028.46};
  static const std::array mu        = {9.61538, 8.69231, 7.84615, 6.92308, 6.07692, 5.46154};
  static const std::array ql        = {532.692, 401.923, 323.462, 271.154, 390.000, 390.000};
  static const std::array ml        = {2.69231, 1.92308, 1.46154, 1.15385, 2.00000, 2.00000};

  // cut on RSTRAW1
  int i = std::lower_bound(pBinEdges.begin(), pBinEdges.end(), fTrackMomentum) - pBinEdges.begin(); // find momentum bin
  if (!(ql[i] - ml[i] * 1e-3 * fZVtx < fRSTRAW1)) return false;                                     // lower bound
  if (GetRegion() != Pnn::kUpstream && !(fRSTRAW1 < qu[i] - mu[i] * 1e-3 * fZVtx)) return false;    // upper bound

  // Z vertex

  if (UCS) // upstream volume
    return 105000 < fZVtx && fZVtx < 110000;

  double zvtxmax;
  if (abs(fEvent->GetRunNumber()) < fFirstNewColRunID) { // TODO: abstract this! (EventInfo is no use...)
    if (fTrackMomentum < 20)                             // this was originally also applied to 2018D+
      zvtxmax = 155000;                                  // p < 20
    else if (fTrackMomentum > 35)                        // this was originally also applied to 2018D+
      zvtxmax = 160000;                                  // p > 35
    else
      zvtxmax = 165000; // 20 < p < 35
  } else {
    if (fTrackMomentum < 20)
      zvtxmax = 165000;
    else
      zvtxmax = 170000;
  }

  return 110000 < fZVtx && fZVtx < zvtxmax;
}

bool PnnSelection::CHANTI()
{
  return !fEvent->GetIsSignalInCHANTI();
}

bool PnnSelection::BeamKinematics()
{
  // cut empirically estimated

  auto pk = fEvent->GetUpstreamParticle()->GetP();

  double   TRIMKick = 0.0913; // GeV
  TVector2 kaonTheta(pk.X() / pk.Z() - TRIMKick / pk.Mag(), pk.Y() / pk.Z());
  TVector2 thetaCenter(0.00002, 0.00002);

  if ((kaonTheta - thetaCenter).Mod() > 0.00035)
    return false;
  if (pk.Mag() < 72.7 || pk.Mag() > 77.2)
    return false;

  return true;
}

bool PnnSelection::BeamBackground()
{
  int nGoodGTKTracks = fEvent->GetUpstreamParticle()->GetNGTKInTimeTracks();
  FillHisto("GTKInTimeTracks", nGoodGTKTracks);

  FillHisto("BeamBackground", "Input", 1);

  if (!BeamKinematics())
    return false;
  FillHisto("BeamBackground", "BeamKinematics", 1);

  if (fEvent->GetIsInteraction())
    return false;
  FillHisto("BeamBackground", "Interaction", 1);

  if (fEvent->GetIsHighToT())
    return false;
  FillHisto("BeamBackground", "HighToT", 1);

  if (nGoodGTKTracks > MaxGTKInTimeTracks)
    return false;
  FillHisto("BeamBackground", "GTKInTimeTracks", 1);

  if (!GTKExtraHits())
    return false;
  FillHisto("BeamBackground", "GTKExtraHits", 1);

  return true;
}

bool PnnSelection::GTKExtraHits()
{
  short          GTKExtraHitsWord = fEvent->GetUpstreamParticle()->GetGTKExtraHits();
  std::bitset<4> extraHitsRICH    = GTKExtraHitsWord,      // 4 LSBs = hits in time with RICH
      extraHitsKTAG               = GTKExtraHitsWord >> 4; // 4 MSBs = hits in time with KTAG

  auto isExtraHitsVeto = [](std::bitset<4> extraHits) {
    return (extraHits[0] || extraHits[1]) && (extraHits[2] || extraHits[3]); // at least 2 hits, of which one in GTK2 or 3
  };

  return !(isExtraHitsVeto(extraHitsKTAG) || isExtraHitsVeto(extraHitsRICH));
}

bool PnnSelection::Timing()
{
  bool   pass     = true;
  double ktagTime = fEvent->GetUpstreamParticle()->GetTKTAG();
  double gtkTime  = fEvent->GetUpstreamParticle()->GetTGTK();
  double richTime = fEvent->GetDownstreamParticle()->GetTRICH();
  double chodTime = fEvent->GetDownstreamParticle()->GetTCHOD();

  for (auto &x : {
           // histogram name to fill, Delta_T, max Delta_T to pass the event
           std::make_tuple("dtGTKCHOD", gtkTime - chodTime, 1.1),
           std::make_tuple("dtKTAGRICH", ktagTime - richTime, 0.5),
           std::make_tuple("dtGTKKTAG", gtkTime - ktagTime, 0.5),
           std::make_tuple("dtGTKRICH", gtkTime - richTime, 0.5),
       }) {
    FillHisto(std::get<0>(x), std::get<1>(x));
    pass = pass && TMath::Abs(std::get<1>(x)) <= std::get<2>(x);
  }

  return pass;
}

bool PnnSelection::CDA()
{
  FillHisto("CDA", fEvent->GetCDA());

  return fEvent->GetCDA() <= 4;
}

bool PnnSelection::KPiAssociation()
{
  double ktagDiscriminant = fEvent->GetIsKTAGBeamDiscriminant(),
         richDiscriminant = fEvent->GetIsRICHBeamDiscriminant();

  FillHisto("beamDiscriminant", ktagDiscriminant, richDiscriminant);

  return ((ktagDiscriminant > 0.03 && richDiscriminant > 0.005) ||
          (richDiscriminant > 0.03 && ktagDiscriminant > 0.005));
}

bool PnnSelection::KPiAssociationBayes()
{
  double maxlogrho;
  TLine  logrhologRcut1, logrhologRcut2;
  if (abs(fEvent->GetRunNumber()) > 10000) {
    maxlogrho      = 2.825;
    logrhologRcut1 = TLine(2.45, 0.1, 6.3, 1.05);
    logrhologRcut2 = TLine(6.3, 1.05, 6.6, 160);
  } else {
    maxlogrho      = 3.625;
    logrhologRcut1 = TLine(2.45, 0.8, 5.8, 2.1);
    logrhologRcut2 = TLine(5.8, 2.1, 6.8, 160);
  }

  const auto isLeftOfLine = [](double x, double y, const TLine &l) { // returns whether (x,y) is left of vector (x1, y1) -> (x2, y2)
    return (y - l.GetY1()) * (l.GetX2() - l.GetX1()) > (x - l.GetX1()) * (l.GetY2() - l.GetY1());
  };

  int    nGoodGTKTracks = fEvent->GetUpstreamParticle()->GetNGTKTracks();
  double logrho1        = fEvent->GetIsKTAGBeamDiscriminant();

  if (nGoodGTKTracks == 1) { // special case with single track
    FillHisto("logrho_1trackEvents", logrho1);
    return logrho1 <= maxlogrho;
  } else { // more than one track
    double logrho2 = fEvent->GetIsSecondBestBeamDiscriminant();
    double logR    = logrho2 - logrho1;
    FillHisto("logRvslogrho1", logrho1, logR);
    if (!std::isfinite(logR))
      std::cerr << "WARNING: logR is not finite! N(GTK) and prior probability might be badly estimated..." << std::endl;
    return isLeftOfLine(logrho1, logR, logrhologRcut1) && isLeftOfLine(logrho1, logR, logrhologRcut2);
  }
}

bool PnnSelection::BoxCut()
{
  auto     positionAtTRIM5  = TVector2(fEvent->GetDownstreamParticle()->GetPosTrim5X(), fEvent->GetDownstreamParticle()->GetPosTrim5Y());
  auto     positionAtSTRAW1 = TVector3(fEvent->GetDownstreamParticle()->GetPosStraw1X(), fEvent->GetDownstreamParticle()->GetPosStraw1Y(), Pnn::STRAW1Position.Z());
  TVector3 vertex           = fEvent->GetVertex();
  TVector2 trackTheta       = fEvent->GetDownstreamParticle()->GetP().XYvector() / fEvent->GetDownstreamParticle()->GetP().Z();

  FillHisto("TRIM5", positionAtTRIM5.X(), positionAtTRIM5.Y());
  FillHisto("trackTheta", trackTheta.X(), trackTheta.Y());

  if (abs(fEvent->GetRunNumber()) < fFirstNewColRunID) // pre-2018NewCol // TODO: abstract this! (EventInfo is no use...)
    return TMath::Abs(positionAtTRIM5.X()) > 100 ||
           TMath::Abs(positionAtTRIM5.Y()) > 500;

  // post-2018NewCol: BDT cut
  fTMVAVariables[0] = positionAtTRIM5.X();
  fTMVAVariables[1] = positionAtTRIM5.Y();
  fTMVAVariables[2] = positionAtSTRAW1.X();
  fTMVAVariables[3] = positionAtSTRAW1.Y();
  fTMVAVariables[4] = (positionAtSTRAW1 - Pnn::STRAW1Position).XYvector().Mod();
  fTMVAVariables[5] = vertex.X();
  fTMVAVariables[6] = vertex.Y();
  fTMVAVariables[7] = vertex.Z();
  fTMVAVariables[8] = trackTheta.X();
  fTMVAVariables[9] = trackTheta.Y();

  double MVAOutput = fTMVAReader->EvaluateMVA("myBDT");
  FillHisto("MVAOutput", MVAOutput);

  bool goodevent = MVAOutput > 0.208423;
  if (!goodevent)
    return false;

  FillHisto("TRIM5_after", positionAtTRIM5.X(), positionAtTRIM5.Y());
  FillHisto("trackTheta_after", trackTheta.X(), trackTheta.Y());

  return true;
}

bool PnnSelection::MUV3SpatialAssociation()
{
  return fEvent->GetDownstreamParticle()->GetIsMuon() > 1;
}

bool PnnSelection::BroadMultivertex(bool checkMUV12)
{
  return !fEvent->GetIsBroadMultiVertex() && !(checkMUV12 && fEvent->GetDownstreamParticle()->GetIsMulti());
}

bool PnnSelection::RICHPresence()
{
  return fEvent->GetDownstreamParticle()->GetIsRICHSingleRing();
}

bool PnnSelection::MUV3Veto()
{
  auto                              muv3word   = fEvent->GetDownstreamParticle()->GetIsMUV3();
  std::bitset<sizeof(muv3word) * 8> muv3bitset = muv3word;

  // time matching with year-dependent veto window [7ns for Run1, 5ns for Run2]. after trigger definition change, unveto spatial matches with tile 147
  size_t bit = abs(fEvent->GetRunNumber()) < 12397 ? 1 : 3;

  return !(muv3bitset.test(bit));
}

bool PnnSelection::PionPIDCalo()
{

  auto downstreamTrack = fEvent->GetDownstreamParticle();

  FillHisto("PionPIDCaloSelection", "Input", 1);
  // MUV vetoes
  if (downstreamTrack->GetIsMulti()) return false;                                         // no extra energy in MUV1+2 (< 5 GeV)
  if (downstreamTrack->GetIsMip()) return false;                                           // not a MIP (MIP discriminant)
  if (!downstreamTrack->GetIsGoodMUV1() && downstreamTrack->GetIsGoodMUV2()) return false; // cut on only MUV2 energy (no MUV1 in the cluster)

  FillHisto("PionPIDCaloSelection", "MUV veto", 1);

  // E/p vetoes
  if (downstreamTrack->GetCaloEnergy() / fTrackMomentum > 1.2) return false; // total E/p
  if (downstreamTrack->GetLKrEnergy() / fTrackMomentum > 0.8) return false;  // LKr E/p (for positron rejection)

  FillHisto("PionPIDCaloSelection", "E/p veto", 1);

  // EM cluster veto
  if (fTrackMomentum > 35) {
    double rMUV1     = downstreamTrack->GetMUV1Energy() / downstreamTrack->GetCaloEnergy();
    double rMUV2     = downstreamTrack->GetMUV2Energy() / downstreamTrack->GetCaloEnergy();
    double seedRatio = downstreamTrack->GetLKrSeedEnergy() / downstreamTrack->GetLKrEnergy();
    double cellRatio = downstreamTrack->GetLKrNCells() / downstreamTrack->GetLKrEnergy();
    if (rMUV1 < 0.01 && rMUV2 < 0.01) {
      if (seedRatio < 0.05 || seedRatio > 0.35) return false;
      double minCellRatio = seedRatio > 0.2 ? 3 : 1.8;
      if (cellRatio < minCellRatio) return false;
    } else {
      if (0 < seedRatio && seedRatio < 0.8 && cellRatio < 1.4) return false;
    }
  }

  FillHisto("PionPIDCaloSelection", "EM cluster veto", 1);

  // pion probability: sigmoid cut
  bool isPion = downstreamTrack->GetCaloPionProb() >= 0.85 + (0.1 / (1. + exp(-(0.7 * (fTrackMomentum - 40.0)))));

  if (!isPion)
    return false;

  FillHisto("PionPIDCaloSelection", "Pion prob", 1);

  return true;
}

bool PnnSelection::PionPIDRICH()
{
  FillHisto("PionPIDRICHSelection", "Input", 1);

  double p = fTrackMomentum;
  double maxLikelihood, minMass;
  if (p < 25)
    maxLikelihood = 0.16, minMass = 0.121;
  else if (p > 35)
    maxLikelihood = 0.82, minMass = 0.112;
  else
    maxLikelihood = 0.09, minMass = 0.125;

  for (int i : {0, 1, 2, 4}) {
    if (fEvent->GetDownstreamParticle()->GetRICHLikeProb(i) > maxLikelihood)
      return false;
    FillHisto("PionPIDRICHSelection", Form("Prob%d", i), 1);
  }

  bool goodRICHMass = minMass <= fEvent->GetDownstreamParticle()->GetRICHMass() && fEvent->GetDownstreamParticle()->GetRICHMass() < 0.200;
  if (!goodRICHMass)
    return false;

  FillHisto("PionPIDRICHSelection", "RICHMass", 1);

  return true;
}

bool PnnSelection::PnnVeto(Pnn::Veto veto)
{
  switch (veto) {
  case Pnn::kIRC:
    return fEvent->GetIsSignalInIRC();
  case Pnn::kSAC:
    return fEvent->GetIsSignalInSAC();
  case Pnn::kSAV:
    return fEvent->GetIsSignalInSAV();
  case Pnn::kLKr:
    return fEvent->GetIsLKrInTimeHits() || (fEvent->GetPhotonVetoTimeFlag() & 0b0001'0001'0100'0100);
  case Pnn::kLAV:
    return fEvent->GetIsSignalInLAV();
  case Pnn::kPhotonVeto:
    return std::any_of(Pnn::PhotonVetoes.begin(), Pnn::PhotonVetoes.end() - 1, [&](Pnn::Veto veto) { return PnnVeto(veto); });

  case Pnn::kHAC:
    return fabs(fEvent->GetMultHAC()) == 1;
  case Pnn::kMUV0:
    return fEvent->GetMultMUV0() == 1;
  case Pnn::kSlabsInTime:
    return fEvent->GetMultAllOldCH7() > 3;
  case Pnn::kCHOD:
    return fEvent->GetMultOldCHLKr();
  case Pnn::kNewCHOD:
    return fEvent->GetMultNewCHLKr();
  case Pnn::kCHODNewCHOD:
    return fEvent->GetMultOldNewCH();
  case Pnn::kLKrMerged:
    return fEvent->GetMultLKrMerged() && fEvent->GetMultAllOldCH7() > 0;
  case Pnn::kSegment:
    return fEvent->GetIsSegment() % 2 == 1;
  case Pnn::kNegative:
    return !fEvent->GetIsOneParticle();
  case Pnn::kMultiplicityVeto:
    return std::any_of(Pnn::MultiplicityVetoes.begin(), Pnn::MultiplicityVetoes.end() - 1, [&](Pnn::Veto veto) { return PnnVeto(veto); });

  case Pnn::kAllVetoes:
    return PnnVeto(Pnn::kPhotonVeto) || PnnVeto(Pnn::kMultiplicityVeto);

  default:
    std::cout << "[PnnVeto] WARNING: Unknown veto " << veto << std::endl;
    return false;
  }
}

std::map<Pnn::Veto, bool> PnnSelection::PnnVeto(std::vector<Pnn::Veto> vetoes, PnnVetoMode mode)
{
  std::map<Pnn::Veto, bool> result;

  bool sequencePass = true;
  for (auto &v : vetoes) {
    if (mode == kSingle)
      result[v] = !PnnVeto(v);
    else if (mode == kSequence) {
      result[v]    = sequencePass && !PnnVeto(v);
      sequencePass = result[v];
    } else if (mode == kExclusive)
      result[v] = std::none_of(vetoes.begin(), vetoes.end(), [&](Pnn::Veto x) { return v != x && PnnVeto(x); });
  }

  return result;
}

bool PnnSelection::PhotonVeto()
{
  return !PnnVeto(Pnn::kPhotonVeto);
}

bool PnnSelection::MultiplicityVeto()
{
  return !PnnVeto(Pnn::kMultiplicityVeto);
}

bool PnnSelection::VetoCounterVeto()
{
  // Two parts: best match + cluster word. TODO: check interactions between the two!

  int    matchType = fEvent->GetUpstreamParticle()->GetVetoCounterBestMatchType();
  double dtMatch   = fEvent->GetUpstreamParticle()->GetVetoCounterBestMatchTime() - fEvent->GetUpstreamParticle()->GetTKTAG();
  FillHisto("dtVCKTAG", dtMatch);

  /* best match type:
   * 0: other
   * 1: pi-like
   * 2: photon-like
   * 3: mu-like
   * 4: pi0-like
   * 5: pi+shower-like
   */

  bool matchVeto = std::set{1, 2, 4, 5}.count(matchType) && fabs(dtMatch) < 2; // veto pi, photon, pi0, pi+shower

  std::bitset<7> clusterWord = fEvent->GetUpstreamParticle()->GetVetoCounterClusterWord();
  double         dtCluster   = fEvent->GetUpstreamParticle()->GetVetoCounterClusterTime() - fEvent->GetUpstreamParticle()->GetTKTAG();

  /* clusterWord bits:
   * 0: any candidate in station 1
   * 1: any candidate in station 2
   * 2: any candidate in station 3
   * 3: noise
   * 4: muon
   * 5: other clusters
   * 6: other in-time clusters
   */

  // veto if any candidate in any station, but not if noise or muon
  bool clusterVeto   = (clusterWord[0] || clusterWord[1] || clusterWord[2]) && fabs(dtCluster) < 2;
  bool clusterUnVeto = clusterWord[3] || clusterWord[4];

  return !(
      (clusterVeto && !clusterUnVeto) // intended
      || (matchVeto && !clusterVeto)  // bug?
  );
}

double PnnSelection::GetMmissKmu2g() const
{
  const double MIPCorrection      = 0.6; // 0.6 GeV = MIP deposit in LKr
  const double ZLKR               = 241093;
  auto         downstreamParticle = fEvent->GetDownstreamParticle();

  TLorentzVector kaon4momentum, muon4momentum, photon4momentum;
  kaon4momentum.SetVectM(fEvent->GetUpstreamParticle()->GetP(), MKCH);
  muon4momentum.SetVectM(downstreamParticle->GetP(), MMU);

  TVector3 posAtLKr(downstreamParticle->GetLKrPosition().X(), downstreamParticle->GetLKrPosition().Y(), ZLKR);
  photon4momentum.SetVectM((posAtLKr - fEvent->GetVertex()).Unit() * (downstreamParticle->GetLKrEnergy() - MIPCorrection), 0);

  return (kaon4momentum - muon4momentum - photon4momentum).M2();
}

bool PnnSelection::Kmu2gVeto()
{
  auto downstreamParticle   = fEvent->GetDownstreamParticle();
  auto maxNonPionLikelihood = std::max({downstreamParticle->GetRICHLikeProb(0), downstreamParticle->GetRICHLikeProb(1), downstreamParticle->GetRICHLikeProb(2), downstreamParticle->GetRICHLikeProb(4)});

  return !(fEvent->GetDownstreamParticle()->GetLKrEnergy() > 5 && std::abs(GetMmissKmu2g()) < 0.01 &&
           // fTrackMomentum > 35
           maxNonPionLikelihood > 0.05);
}

bool PnnSelection::CobraVeto()
{
  auto posAtTRIM5 = TVector2(fEvent->GetDownstreamParticle()->GetPosTrim5X(), fEvent->GetDownstreamParticle()->GetPosTrim5Y());

  return !(                                                                                       // veto if
      (400 < posAtTRIM5.Mod() && posAtTRIM5.Mod() < 500 && posAtTRIM5.X() < -300) &&              // region at TRIM5
      (75 < fRSTRAW1 && fRSTRAW1 < 200 && fEvent->GetDownstreamParticle()->GetPosStraw1X() > 150) // region at STRAW1
  );
}

bool PnnSelection::ANTI0Veto()
{
  return !(abs(fEvent->GetDownstreamParticle()->GetANTI0dtmin()) < 3);
}

static bool upstream_validation_sample_condition(PnnSelection *pnn, PnnNA62Event *fEvent, int iSample)
{
  switch (iSample) {
  case 0: // sample C
    return pnn->BoxCut() && !fEvent->GetIsInteraction() && !fEvent->GetIsHighToT() && pnn->GTKExtraHits() && pnn->VetoCounterVeto() && pnn->CHANTI();
  case 1:
  case 2:
    return pnn->BoxCut() && (fEvent->GetIsInteraction() || fEvent->GetIsHighToT());
  case 3:
  case 4:
    return pnn->BoxCut() && !fEvent->GetIsInteraction() && !fEvent->GetIsHighToT() && !pnn->GTKExtraHits() && pnn->VetoCounterVeto() && pnn->CHANTI();
  case 5:
  case 6:
    return pnn->BoxCut() && !fEvent->GetIsInteraction() && !fEvent->GetIsHighToT() && pnn->CHANTI() && !pnn->VetoCounterVeto();
  case 7:
  case 8:
    return pnn->BoxCut() && !fEvent->GetIsInteraction() && !fEvent->GetIsHighToT() && !pnn->CHANTI() && pnn->VetoCounterVeto();
  case 9:
  case 10:
    return !pnn->BoxCut() && !fEvent->GetIsInteraction() && !fEvent->GetIsHighToT() && pnn->GTKExtraHits() && pnn->VetoCounterVeto() && pnn->CHANTI();
  default:
    return false;
  };
}

static bool upstream_validation_kinematic_condition(PnnSelection *pnn, PnnNA62Event *fEvent, int iSample)
{
  if (iSample > 0 && iSample % 2 == 0)
    return pnn->GetRegion() == Pnn::kUpstream;
  else
    return (pnn->GetRegion() == Pnn::kPNN1 || pnn->GetRegion() == Pnn::kPNN2);
}

int PnnSelection::UpstreamValidationSample()
{

  int sample = -1;
  for (int iSample = 0; iSample <= NUpstreamValidationSamples; iSample++)
    if (upstream_validation_sample_condition(this, fEvent, iSample) && upstream_validation_kinematic_condition(this, fEvent, iSample)) {
      if (sample == -1)
        sample = iSample;
      else
        throw std::logic_error("Samples are not orthogonal!");
    }

  return sample;
}