#include "HistoHandler.hh"

#include <iostream>
#include <TH2F.h>
#include <TChain.h>

bool HistoHandler::AutoWrite = true;

TDirectory *mkdir_if_not_exists(TDirectory *parent, TString path)
{
  if (path == "")
    return parent;

  if (parent->GetDirectory(path) == NULL)
    parent->mkdir(path);

  return parent->GetDirectory(path);
}

HistoHandler::HistoHandler(TDirectory *parent, TString path)
    : fParentDirectory(parent), fPath(path)
{
}

std::shared_ptr<HistoHandler> HistoHandler::MakeChild(TString childPath)
{
  auto ptr = std::make_shared<HistoHandler>(fParentDirectory, childPath == "" ? fPath : fPath + "/" + childPath);
  fChildren.push_back(ptr);
  return ptr;
}

void HistoHandler::BookHisto(TObject *object, TString path, TString name)
{
  if (name == "")
    name = object->GetName();

  if (fHistos.insert({name, object}).second)
    fHistoOrder.push_back({name, path});
  else
    std::cout << "WARNING: Failed to book an object with already taken name " << name << std::endl;
}

void HistoHandler::UnbookHisto(TString name)
{
  if (fHistos.erase(name))
    fHistoOrder.erase(
        std::find_if(fHistoOrder.begin(), fHistoOrder.end(),
                     [&](std::pair<TString, TString> namepathPair) { return namepathPair.first == name; }));
}

HistoHandler::~HistoHandler()
{
  if (AutoWrite && fParentDirectory && !fWritten && fHistoOrder.size() != 0)
    Write();
}

void HistoHandler::Write(TString name, TDirectory *directory)
{
  fWritten = true;

  if (directory == nullptr) {
    if (fParentDirectory == nullptr)
      throw std::runtime_error("Calling HistoHandler::Write() with no target TDirectory");
    else
      directory = mkdir_if_not_exists(fParentDirectory, fPath);
  }

  for (auto &x : fHistoOrder) {
    if (name == "" || x.first == name) {
      auto        histo           = fHistos.at(x.first);
      TDirectory *outputDirectory = mkdir_if_not_exists(directory, x.second);
      if (auto chain = dynamic_cast<TChain *>(histo)) {
        // make a single tree with all entries, via TChain::Merge().
        // in fact, this method thinks the chain might be so large that it can't stay in one 100GB file, and will create multiple files.
        // without "keep", it will close the file (as it isn't necessarily the original one).
        // in principle, if that happens, it will break everything (but no way to know that in advance...).
        // hopefully we aren't analyzing large chains...

        // need to cd into outputDirectory
        TDirectory *previousDir = gDirectory;
        outputDirectory->cd();
        chain->Merge(outputDirectory->GetFile(), 0, "fast keep");
        previousDir->cd();
      } else
        outputDirectory->WriteTObject(histo);
    }
  }
}

std::pair<double, double> FindHistoEdges(TH1 *hist, double xmin, double xmax)
{
  auto LowEdge = [](TH1 *hist, double xmin) {
    int imin = hist->FindFirstBinAbove();
    if (!std::isnan(xmin)) {
      // adjust it to the binning grid
      xmin = hist->GetXaxis()->GetXmin() + hist->GetXaxis()->GetBinWidth(1) * std::floor((xmin - hist->GetXaxis()->GetXmin()) / hist->GetXaxis()->GetBinWidth(1));
      if (imin == -1 || xmin < hist->GetXaxis()->GetBinLowEdge(imin)) // no content, or xmin lower than first bin with content
        return xmin;
    }

    // xmin is nan, or larger than first bin with content
    return imin == -1 ? hist->GetXaxis()->GetXmin() : hist->GetXaxis()->GetBinLowEdge(imin);
  };

  auto HighEdge = [](TH1 *hist, double xmax) {
    int imax = hist->FindLastBinAbove();

    if (!std::isnan(xmax)) {
      // adjust it to the binning grid
      xmax = hist->GetXaxis()->GetXmax() - hist->GetXaxis()->GetBinWidth(1) * std::floor((hist->GetXaxis()->GetXmax() - xmax) / hist->GetXaxis()->GetBinWidth(1));
      if (imax == -1 || xmax > hist->GetXaxis()->GetBinLowEdge(imax + 1)) // no content, or xmax larger than last bin with content
        return xmax;
    }

    // xmax is nan, or lower than last bin with content
    return (imax == -1) ? hist->GetXaxis()->GetXmax() : hist->GetXaxis()->GetBinLowEdge(imax + 1);
  };

  xmin = LowEdge(hist, xmin);
  xmax = HighEdge(hist, xmax);
  if (xmax <= xmin)
    xmax = hist->GetXaxis()->GetBinLowEdge(hist->GetXaxis()->FindBin(xmin) + 1);
  return std::make_pair(xmin, xmax);
}

TH2F *HistoHandler::ResizeHisto(TH2 *histo, TString newName, double xmin, double xmax, double ymin, double ymax)
{
  std::tie(xmin, xmax) = FindHistoEdges(histo->ProjectionX(), xmin, xmax);
  std::tie(ymin, ymax) = FindHistoEdges(histo->ProjectionY(), ymin, ymax);
  TH2F *resized        = new TH2F(newName, histo->GetTitle(),
                                  (xmax - xmin) / histo->GetXaxis()->GetBinWidth(1), xmin, xmax,
                                  (ymax - ymin) / histo->GetYaxis()->GetBinWidth(1), ymin, ymax);
  resized->GetXaxis()->SetTitle(histo->GetXaxis()->GetTitle());
  resized->GetYaxis()->SetTitle(histo->GetYaxis()->GetTitle());
  resized->SetEntries(histo->GetEntries());
  for (int x = 0; x <= resized->GetNbinsX() + 1; x++)
    for (int y = 0; y <= resized->GetNbinsY() + 1; y++)
      resized->SetBinContent(x, y,
                             histo->GetBinContent(
                                 histo->FindBin(
                                     resized->GetXaxis()->GetBinCenter(x),
                                     resized->GetYaxis()->GetBinCenter(y))));

  return resized;
}

void HistoHandler::ResizeHisto(TString name, double xmin, double xmax, double ymin, double ymax)
{
  TH2 *toResize = Get<TH2>(name);
  if (toResize == NULL)
    return;

  toResize->SetName(name + "_toResize"); // move it to different name, so that the resized version can have the same name

  fHistos[name] = ResizeHisto(toResize, name, xmin, xmax, ymin, ymax); // replace the histo in the container

  delete toResize; // prevent memory leak
}

TH1 *HistoHandler::ApplyEfficiency(const TH1 *histo, const TEfficiency *efficiency, const TString newname, bool binomialError)
{
  TH1 *result = static_cast<TH1 *>(histo->Clone(newname));
  result->Reset();
  result->Divide(efficiency->GetPassedHistogram(), efficiency->GetTotalHistogram(), 1, 1, binomialError ? "B" : "");
  result->Multiply(histo);
  result->SetEntries(histo->GetEntries());
  return result;
}

TH1 *HistoHandler::DivideByEfficiency(const TH1 *histo, const TEfficiency *efficiency, const TString newname, bool binomialError)
{
  TH1 *result = static_cast<TH1 *>(histo->Clone(newname));
  result->Reset();
  result->Divide(efficiency->GetPassedHistogram(), efficiency->GetTotalHistogram(), 1, 1, binomialError ? "B" : "");
  result->Divide(histo);
  result->SetEntries(histo->GetEntries());
  return result;
}