#include <TString.h>
#include <sstream>
#include <fstream>
#include <iostream>
#include <getopt.h>

#include "globals.hh"
#include "CommandLineParser.hh"

namespace Pnn {
// TODO: remove -d, if a directory is passed to -i (or as a further argument) recurse into it.
CommandLineOptions ParseCommandLineOptions(int argc, char **argv)
{
  CommandLineOptions parsed;

  const char *const short_opts  = "i:l:d:o:f:F:h";
  const option      long_opts[] = {
      {"input-file", required_argument, NULL, 'i'},
      {"list", required_argument, NULL, 'l'},
      {"directory", required_argument, NULL, 'd'},
      {"output", required_argument, NULL, 'o'},
      {"nfiles", required_argument, NULL, 'f'},
      {"start-file", required_argument, NULL, 'F'},
      {"help", no_argument, NULL, 'h'},
      {NULL, no_argument, NULL, 0},
  };

  // parse command line options
  while (true) {
    const auto opt = getopt_long(argc, argv, short_opts, long_opts, nullptr);

    if (-1 == opt)
      break;

    switch (opt) {
    case 'i':
      parsed.Liststream << optarg << std::endl;
      break;
    case 'l': {
      std::ifstream listfile(optarg);
      if (listfile) {
        parsed.Liststream << listfile.rdbuf();
        listfile.close();
      }
      break;
    }
    case 'd':
      ListFiles(optarg, [&](const std::string &path) {
        parsed.Liststream << path << std::endl;
      });
      break;
    case 'o':
      parsed.OutputFilename = optarg;
      break;
    case 'f':
      parsed.MaxNFiles = std::stoi(optarg);
      if (parsed.MaxNFiles <= 0)
        parsed.MaxNFiles = INT32_MAX;
      break;
    case 'F':
      parsed.StartFile = std::stoi(optarg); // 0-based
      break;
    case 'h': // help
    case '?': // unrecognized
    default:
      parsed.ExitCode = -1;
      return parsed;
    }
  }

  for (int index = optind; index < argc; index++)
    parsed.Liststream << argv[index] << std::endl;

  if (parsed.Liststream.rdbuf()->in_avail() == 0)
    parsed.ExitCode = 1;

  return parsed;
}

} // namespace Pnn
