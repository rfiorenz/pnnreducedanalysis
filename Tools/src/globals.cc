#include "globals.hh"
#include <dirent.h>
#include <functional>
#include <sstream>
#include <TBufferFile.h>

void ROOTErrorIgnoreLevel(Int_t errorIgnoreLevel, std::function<void(void)> cb)
{
  auto previousErrorIgnoreLevel = gErrorIgnoreLevel;
  gErrorIgnoreLevel             = errorIgnoreLevel;
  cb();
  gErrorIgnoreLevel = previousErrorIgnoreLevel;
};

void ListFiles(std::string path, std::function<void(const std::string &)> cb)
{
  // ensure there is one and only one trailing / from path
  path.erase(std::find_if(path.rbegin(), path.rend(), [](unsigned char ch) {
               return ch != '/';
             }).base(),
             path.end());
  path += "/";

  if (auto dir = opendir(path.c_str())) {
    while (auto f = readdir(dir)) {
      if (!f->d_name || f->d_name[0] == '.')
        continue;
      if (f->d_type == DT_DIR)
        ListFiles(path + f->d_name + "/", cb);

      if (f->d_type == DT_REG)
        cb(path + f->d_name);
    }
    closedir(dir);
  }
}

void TraverseList(std::stringstream &liststream, int start, int maxlines, std::function<void(TString)> cb)
{
  int         iLine = 0;
  std::string line;
  while (getline(liststream, line)) {
    iLine++;
    if (iLine - start > maxlines)
      break;
    if (iLine <= start)
      continue;
    cb(line);
  }
  liststream.clear();
  liststream.seekg(0, std::ios::beg); // rewind the file list
}

TString PrependXRDProtocol(TString filepath)
{
  if (filepath.BeginsWith("/eos/user") || filepath.BeginsWith("/eos/home"))
    filepath = "root://eosuser.cern.ch/" + filepath;
  else if (filepath.BeginsWith("/eos/experiment"))
    filepath = "root://eospublic.cern.ch/" + filepath;

  return filepath;
}

void deep_copy_TObject(TObject *in, TObject *out, int bufsize)
{ // ROOT, I hate you

  // code from https://root-forum.cern.ch/t/copy-constructor-of-unkown-tobject-delivered-class/28825/6
  TBufferFile buffer(TBuffer::kWrite, bufsize);
  buffer.MapObject(in); // register obj in map to handle self reference
  in->Streamer(buffer);
  // here some additional compliciation needed if you use TRef  in conjunction with those objects.

  // read new object from buffer
  buffer.SetReadMode();
  buffer.ResetMap();
  buffer.SetBufferOffset(0);

  buffer.MapObject(out); // register obj in map to handle self reference
  out->Streamer(buffer);
  out->ResetBit(kIsReferenced);
  out->ResetBit(kCanDelete);
}