#include "KpiAssociationStd.hh"
#include "PnnNA62Event.hh"
#include "PnnSelection.hh"

#define DTMAX 1.2
#define DTBIN 0.05
#define TIMEBINNING round(2 * DTMAX / DTBIN), -DTMAX, DTMAX

using namespace Pnn;

KpiAssociationStd::KpiAssociationStd(HistoHandler &histo, bool /*isRun2*/)
    : fHisto(histo.MakeChild("KpiAssociationStd")), NGTK({0, 1})
{
  DiscriminantNormalization(0);
  DiscriminantNormalization(1);

  fHisto->BookHisto(new TH2F("DRICH", ";DRICH;TRICH-TGTK", 100, 0, 1, TIMEBINNING));
  fHisto->BookHisto(new TH2F("DKTAG", ";DKTAG;TKTAG-TGTK", 100, 0, 1, TIMEBINNING));
  fHisto->BookHisto(new TH2F("Discr", ";DRICH;DKTAG", 100, 0, 1, 100, 0, 1));
};

bool KpiAssociationStd::QualityCut(double CDA, double TGTK, double TKTAG, double TRICH)
{
  return true; // should be done, but it is completely equivalent to not looking at some bins of PmistagDT (and time cut is already applied at selection level anyway)
};

std::pair<int, bool> KpiAssociationStd::FindMatch(const std::vector<PileupTrack> &event)
{
  if (event.size() != 1)
    throw std::logic_error("There should only be 1 track in each event!");

  const PileupTrack &track = event[0];

  double CDA   = track.CDA;
  double TGTK  = track.TGTK;
  double TKTAG = track.TKTAG;
  double TRICH = track.TRICH;

  double ktagDiscriminant = Discriminant(CDA, TGTK - TKTAG, 0);
  double richDiscriminant = Discriminant(CDA, TGTK - TRICH, 1);

  fHisto->FillHisto("DRICH", richDiscriminant, TRICH - TGTK);
  fHisto->FillHisto("DKTAG", ktagDiscriminant, TKTAG - TGTK);
  fHisto->FillHisto("Discr", richDiscriminant, ktagDiscriminant);

  // ask PnnSelection whether event passed
  auto na62Event = new PnnNA62Event();
  na62Event->SetIsKTAGBeamDiscriminant(ktagDiscriminant);
  na62Event->SetIsRICHBeamDiscriminant(richDiscriminant);
  PnnSelection pnn(na62Event);
  bool         passed = pnn.KPiAssociation();
  delete na62Event;

  return {0, passed};
}

Double_t KpiAssociationStd::PDFKaonCDA(Double_t cda)
{
  Double_t cdapar[8];
  cdapar[0] = 0.25;
  cdapar[1] = 1.27; // sigma 1 [mm]
  cdapar[2] = 0.039;
  cdapar[3] = 2.23; // sigma 2 [mm]
  cdapar[4] = 0.0033;
  cdapar[5] = 0.145;
  cdapar[6] = -0.00046;
  cdapar[7] = 1.51e-05;

  Double_t cdag1 = cdapar[0] * exp(-0.5 * pow(cda / cdapar[1], 2));
  Double_t cdag2 = cdapar[2] * exp(-0.5 * pow(cda / cdapar[3], 2));
  Double_t cdaf1 = cdapar[4] * exp(-cdapar[5] * cda) + cdapar[6] + cdapar[7] * cda;
  if (cda > 25.)
    return 0.;
  return cdag1 + cdag2 + cdaf1;
}

Double_t KpiAssociationStd::PDFKaonDT(Double_t dt, bool RICH)
{
  Double_t dtpar[6];

  if (!RICH) { // KTAG pdf
    dtpar[0] = 0.027;
    dtpar[1] = 0.130; // sigma 1 [ns]
    dtpar[2] = 0.0017;
    dtpar[3] = 0.230; // sigma 2 [ns]
  } else {            // rich pdf
    dtpar[0] = 0.026;
    dtpar[1] = 0.141; // sigma 1 [ns]
    dtpar[2] = 0.001;
    dtpar[3] = 0.279; // sigma 2 [ns]
  }

  Double_t dtg1 = dtpar[0] * exp(-0.5 * pow(dt / dtpar[1], 2));
  Double_t dtg2 = dtpar[2] * exp(-0.5 * pow(dt / dtpar[3], 2));
  return dtg1 + dtg2;
}

Double_t KpiAssociationStd::PDFPileDT()
{
  return 8.1e-03;
}

Double_t KpiAssociationStd::PDFPileCDA(Double_t cda)
{
  Double_t cdapar[4]; // "Universal" pdf

  cdapar[0] = 0.0122;
  cdapar[1] = 5.95; // sigma 1 [mm]
  cdapar[2] = 0.0235;
  cdapar[3] = 13.87; // sigma 2 [mm]

  Double_t cdag1 = cdapar[0] * exp(-0.5 * pow(cda / cdapar[1], 2));
  Double_t cdag2 = cdapar[2] * exp(-0.5 * pow(cda / cdapar[3], 2));
  return cdag1 + cdag2;
}

void KpiAssociationStd::DiscriminantNormalization(bool detector) // detector = RICH, !detector = KTAG
{

  double pass_cda = 0.01;  // mm
  double pass_dt  = 0.001; // ns

  fPDFKaonCDA[detector].clear();
  fIntPDFKaonCDA[detector] = 0;
  fPDFPileCDA[detector].clear();
  fIntPDFPileCDA[detector] = 0;
  for (int jbincda(0); jbincda < 6000; jbincda++) { // < 60 mm
    double cda   = pass_cda * jbincda + 0.5 * pass_cda;
    double pdfkc = PDFKaonCDA(cda);
    if (pdfkc < 0)
      pdfkc = 0;
    fIntPDFKaonCDA[detector] += pdfkc * pass_cda / 0.25;
    //    if (fIntPDFKaonCDA[jD]>=1) fIntPDFKaonCDA[jD] = 1;
    fPDFKaonCDA[detector].push_back(fIntPDFKaonCDA[detector]);
    double pdfpc = PDFPileCDA(cda);
    if (pdfpc < 0)
      pdfpc = 0;
    fIntPDFPileCDA[detector] += pdfpc * pass_cda / 0.25;
    //    if (fIntPDFPileCDA[jD]>=1) fIntPDFPileCDA[jD] = 1;
    fPDFPileCDA[detector].push_back(fIntPDFPileCDA[detector]);
  }
  fPDFKaonDT[detector].clear();
  fIntPDFKaonDT[detector] = 0;
  fPDFPileDT[detector].clear();
  fIntPDFPileDT[detector] = 0;
  for (int jbindt(0); jbindt < 1000; jbindt++) { // +-1 ns
    double dt = pass_dt * jbindt + 0.5 * pass_dt;
    fIntPDFKaonDT[detector] += (PDFKaonDT(dt, detector) + PDFKaonDT(-dt, detector)) * pass_dt / 0.01;
    fPDFKaonDT[detector].push_back(fIntPDFKaonDT[detector]);
    fIntPDFPileDT[detector] += (PDFPileDT() + PDFPileDT()) * pass_dt / 0.01;
    fPDFPileDT[detector].push_back(fIntPDFPileDT[detector]);
  }
}

double KpiAssociationStd::Discriminant(double cda_val, double dt_val, bool detector) // detector = RICH, !detector = KTAG
{
  static const auto EvaluateCondition = [](Double_t var, Double_t cut, Double_t integral) { return (var > cut) ? integral : -1; };

  int jD = detector;

  double discr = 0;
  if (cda_val > 59.9 || fabs(dt_val) > 0.95)
    return discr; // range of validity of the normalization

  double                        pass_cda = 0.01;  // mm
  double                        pass_dt  = 0.001; // 0.05; // ns
  double                        pkcda    = 0.;
  std::vector<double>::iterator ikcda    = fPDFKaonCDA[jD].begin();
  while (ikcda != fPDFKaonCDA[jD].end()) {
    double cda = (double)(distance(fPDFKaonCDA[jD].begin(), ikcda) - 1) * pass_cda;
    if ((pkcda = EvaluateCondition(cda, cda_val, *ikcda)) >= 0)
      break;
    ++ikcda;
  }
  pkcda /= fIntPDFKaonCDA[jD];
  double                        pkdt = 0.;
  std::vector<double>::iterator ikdt = fPDFKaonDT[jD].begin();
  while (ikdt != fPDFKaonDT[jD].end()) {
    double dt = (double)(distance(fPDFKaonDT[jD].begin(), ikdt) - 1) * pass_dt;
    if ((pkdt = EvaluateCondition(fabs(dt), fabs(dt_val), *ikdt)) >= 0)
      break;
    ++ikdt;
  }
  pkdt /= fIntPDFKaonDT[jD];

  // Simple discriminant
  double discr_kaon = (1 - pkcda) * (1 - pkdt); // larger (closer to 1) is better.
  discr             = discr_kaon;

  return discr;
}