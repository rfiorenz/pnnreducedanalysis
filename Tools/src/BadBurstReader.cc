#include "BadBurstReader.hh"

#include "DetectorID.hh"

#include <regex>
#include <fstream>
#include <iostream>

const TString GetProcVersion(int runID)
{
  if (8805 <= runID && runID < 8968) return "v3.1.7";
  if (8968 <= runID && runID < 9047) return "v3.1.4";
  if (9047 <= runID && runID < 9147) return "v3.1.7";
  if (9147 <= runID && runID < 9306) return "v3.1.7";
  if (9306 <= runID && runID < 10787) return "v3.2.3";
  if (10787 <= runID && runID < 10915) return "v3.1.6";
  if (10915 <= runID && runID <= 11161) return "v3.1.5";
  if (11858 <= runID && runID < 12013) return "v3.4.3";
  if (12013 <= runID && runID < 12165) return "v3.3.1";
  if (12165 <= runID && runID < 12340) return "v3.7.1";
  if (12340 <= runID && runID < 12464) return "v3.4.1";
  if (12464 <= runID && runID < 13000) return "v3.4.2";

  throw std::logic_error(Form("Unknown run ID: %d", runID));
}

const std::bitset<64> GetIgnoreMask(int runID)
{
  std::bitset<64> ignoreMask;

  if (runID < 10000) // require VetoCounterTEL in Run2, ignore it in Run1
    ignoreMask.set(Pnn::kVetoCounterTEL);

  auto procVersion = GetProcVersion(runID);
  if (procVersion == "v3.1.5")
    ignoreMask.set(Pnn::kMUV1);
  if (procVersion.BeginsWith("v3.1"))
    ignoreMask.set(Pnn::kVetoCounterTEL);

  ignoreMask.set(Pnn::kVetoCounter);
  ignoreMask.set(Pnn::kANTI0);
  ignoreMask.set(Pnn::kL0ANTI0);
  ignoreMask.set(Pnn::kL0CHOD);
  ignoreMask.set(Pnn::kL0LAV);

  return ignoreMask;
}

const TString GetCDBBadBurstFile(int runID)
{
  const TString procVersion = GetProcVersion(runID); // use lookup for processing version, to update manually

  // alternatively, the following code gets the procversion automagically from the filename of the file being read, but it needs to be passed the tree...
  // std::string filename = fTree->GetCurrentFile()->GetName();
  // static const std::regex rgx("_p\\.(\\w[\\d\\.]+)");
  // std::smatch             match;
  // if (std::regex_search(filename, match, rgx))
  //   return TString(match[1]);
  // else
  //   return "";

  return Form("/cvmfs/na62.cern.ch/offline/CDB/Data/%s/%06d/BadBursts.dat", procVersion.Data(), runID);
}

const std::bitset<64> BadBurstReader::GetBadBurstMaskFromCDB(int runID, int burstID)
{
  const TString badBurstPath = GetCDBBadBurstFile(runID);
  std::ifstream badBurstFile(badBurstPath);
  std::string   line;
  while (std::getline(badBurstFile, line)) {         // read file line by line
    if (line.empty() || line.at(0) == '#') continue; // skip empty lines and comments
    std::istringstream iss(line);
    int                runIDread, burstIDread;
    ULong64_t          badBurstMask;

    // parse line
    if (!(iss >> runIDread >> burstIDread >> std::hex >> badBurstMask)) { // parse and check format is okay
      std::cerr << "WARNING: Error parsing " << badBurstPath << ": unexpected format. Skipping line..." << std::endl;
      continue;
    }

    // double check for runID
    if (runIDread != runID) {
      std::cerr << "WARNING: Run " << runIDread << " found in " << badBurstPath << " (expected " << runID << "). Skipping line..." << std::endl;
      continue;
    }

    if (burstIDread == burstID)
      return badBurstMask;
  }

  return 0;
}

const std::unordered_map<int, std::bitset<64>> BadBurstReader::GetBadBurstsFromCDB(int runID)
{
  std::unordered_map<int, std::bitset<64>> badBursts;

  const TString badBurstPath = GetCDBBadBurstFile(runID);
  std::ifstream badBurstFile(badBurstPath);
  std::string   line;
  while (std::getline(badBurstFile, line)) {         // read file line by line
    if (line.empty() || line.at(0) == '#') continue; // skip empty lines and comments
    std::istringstream iss(line);
    int                runIDread, burstID;
    ULong64_t          badBurstMask;

    // parse line
    if (!(iss >> runIDread >> burstID >> std::hex >> badBurstMask)) { // parse and check format is okay
      std::cerr << "WARNING: Error parsing " << badBurstPath << ": unexpected format. Skipping line..." << std::endl;
      continue;
    }

    // double check for runID
    if (runIDread != runID) {
      std::cerr << "WARNING: Run " << runIDread << " found in " << badBurstPath << " (expected " << runID << "). Skipping line..." << std::endl;
      continue;
    }

    badBursts[burstID] = badBurstMask;
  }

  return badBursts;
}

const bool BadBurstReader::IsBadBurst(int runID, int burstID, std::bitset<64> mask)
{
  return (GetBadBurstMaskFromCDB(runID, burstID) & mask).any();
}

const bool BadBurstReader::IsBadBurst(const std::unordered_map<int, std::bitset<64>> &badBursts, int burstID, std::bitset<64> mask)
{
  if (badBursts.count(burstID)) // burstID is known
    return (badBursts.at(burstID) & mask).any();
  else // not found among bad bursts
    return false;
}

const bool BadBurstReader::CheckRealBadBurst(int runID, int burstID)
{
  return IsBadBurst(runID, burstID, ~GetIgnoreMask(runID));
}

const bool BadBurstReader::CheckRealBadBurst(const std::unordered_map<int, std::bitset<64>> &badBursts, int runID, int burstID)
{
  return IsBadBurst(badBursts, burstID, ~GetIgnoreMask(runID));
}