#include <iostream>
#include <fstream>
#include <TFile.h>
#include <TChain.h>
#include <TH2F.h>
#include <string>
#include <iomanip>
#include <vector>
#include <numeric>
#include <algorithm>
#include <functional>

#include "globals.hh"
#include "CommandLineParser.hh"
#include "ProgressMonitor.hh"
#include "ProcessMonitor.hh"
#include "VAnalysis.hh"
#include "HistoHandler.hh"
#include "PnnNA62Event.hh"
#include "EventInfo.hh"
#include "BadBurstReader.hh"

extern std::vector<VAnalysis *> executable;

int main(int argc, char **argv)
{
  Pnn::CommandLineOptions opts = Pnn::ParseCommandLineOptions(argc, argv);

  if (opts.ExitCode == 1)
    std::cout << "ERROR: no input files!" << std::endl;

  if (opts.ExitCode > 0)
    return opts.ExitCode;
  else if (opts.ExitCode < 0) {
    // print help and exit
    std::cout << "-i, --input-file <file>  Path to an input ROOT file" << std::endl
              << "-l, --list <list>        Path to a text file containing a list of paths to" << std::endl
              << "                         input ROOT files. One file per line " << std::endl
              << "-d, --directory <path>   Path containing input ROOT files (recursive)" << std::endl
              << "-o, --output <file>      Path to output ROOT file. Will be overwritten if already exists" << std::endl
              << "-f, --nfiles <num>       Maximum number of files to process (-1 = no limit)" << std::endl
              << "-F, --start-file <num>   Index of the first file to process (0-based)" << std::endl
              << std::endl
              << "Any further argument will be interpreted as an input ROOT file." << std::endl;
    return 0;
  }

  // create output file
  TFile *outfile = new TFile(opts.OutputFilename, "RECREATE");
  if (!outfile || outfile->IsZombie()) {
    std::cout << "ERROR: cannot write on " << opts.OutputFilename << "!" << std::endl;
    return 3;
  }
  TH1::AddDirectory(false);

  // check which tree will be analyzed by which analyzer
  std::unordered_map<Pnn::ReducedTree, std::vector<VAnalysis *>> treeAnalyzerMap;
  for (auto &analysis : executable)
    for (auto &tree : analysis->GetAllTrees())
      treeAnalyzerMap[tree].push_back(analysis);

  // print analysis summary
  std::cout << "#####    Analysis summary    #####" << std::endl;
  std::vector<VAnalysis *> histosExecutable;
  std::copy_if(executable.begin(), executable.end(), std::back_inserter(histosExecutable), [](VAnalysis *an) { return an->ProcessesHistos(); });
  if (!histosExecutable.empty())
    std::cout << std::setw(15) << "InputHistos"
              << " : " << std::accumulate(std::next(histosExecutable.begin()), histosExecutable.end(), histosExecutable[0]->GetName(), [&](TString total, VAnalysis *an) {
                   return std::move(total) + ", " + an->GetName();
                 })
              << std::endl;
  for (auto &treeAnalyzerPair : treeAnalyzerMap)
    std::cout << std::setw(15) << Pnn::TreeToString.at(treeAnalyzerPair.first) << " : "
              << std::accumulate(std::next(treeAnalyzerPair.second.begin()), treeAnalyzerPair.second.end(),
                                 treeAnalyzerPair.second[0]->GetSummary(treeAnalyzerPair.first),
                                 [&](TString total, VAnalysis *an) {
                                   TString summary = an->GetSummary(treeAnalyzerPair.first);
                                   if (summary != "" && total != "")
                                     summary = ", " + summary;
                                   return std::move(total) + summary;
                                 })
              << std::endl;
  std::cout << std::endl;

  // start of analysis
  for (auto &analysis : executable) {
    analysis->InitHistoHandlers(outfile);
    analysis->DoStartOfJob();
  }

  // process histos
  if (!histosExecutable.empty()) {
    std::cout << "#####  Start processing histos  #####" << std::endl;

    for (auto &analysis : histosExecutable)
      analysis->DoStartOfHistos();

    // calculate number of files, to get a total for ProgressMonitor
    int nFiles = 0;
    TraverseList(opts.Liststream, opts.StartFile, opts.MaxNFiles, [&](TString) { nFiles++; });

    ProgressMonitor fileMonitor(nFiles, 1);
    ROOTErrorIgnoreLevel(kError, [&] { // silence warnings
      TraverseList(opts.Liststream, opts.StartFile, opts.MaxNFiles, [&](TString filepath) {
        fileMonitor.Print();
        auto f = new TFile(PrependXRDProtocol(filepath));
        if (!f || f->IsZombie())
          return; // ROOT is already complaining, so no message needed
        for (auto &analysis : histosExecutable)
          analysis->DoProcessFile(f);
        f->Close();
      });
    });
    for (auto &analysis : histosExecutable)
      analysis->DoEndOfHistos();
    fileMonitor.End();

    std::cout << "#####  End processing histos    #####" << std::endl
              << std::endl;
  }

  // loop over trees
  for (auto &treeAnalyzerPair : treeAnalyzerMap) {
    Pnn::ReducedTree treeType = treeAnalyzerPair.first;
    TString          treeName = Pnn::TreeToString.at(treeType);
    std::cout << "#####  Start processing " << treeName << "  #####" << std::endl;

    // create the TChain (read files)
    TChain *tree   = new TChain(treeName);
    int     nFiles = 0;
    ROOTErrorIgnoreLevel(kError, [&] { // silence warnings
      TraverseList(opts.Liststream, opts.StartFile, opts.MaxNFiles, [&](TString filepath) {
        if (tree->AddFile(PrependXRDProtocol(filepath), 0)) {
          nFiles++;
          // if (nFiles % 20 == 0)
          std::cout << std::flush << nFiles << " files added, " << tree->GetEntriesFast() << " events found\r";
        }
      });
    });
    std::cout << nFiles << " files added, " << tree->GetEntriesFast() << " events found " << std::endl;

    // initialize tree reading
    PnnNA62Event *event = NULL;
    tree->SetBranchAddress("NA62Event", &event);

    const long nEntries      = tree->GetEntries();
    int        latestRun     = -1;
    int        latestBurst   = -1;
    long       nBurstChanges = 0;
    long       nRunChanges   = 0;
    bool       skipBurst     = true;

    std::optional<EventInfo>                 eventInfo;
    std::unordered_map<int, std::bitset<64>> badBurstMasks;

    // initialize monitors
    ProgressMonitor progress(nEntries);
    ProcessMonitor  processMonitor(treeType, outfile);

    // start actual processing of tree
    std::vector<VAnalysis *> &treeAnalyzers = treeAnalyzerPair.second;
    for (auto &analysis : treeAnalyzers)
      analysis->DoStartOfTree(treeType);

    // start event loop
    for (long iEvent = 0; iEvent < nEntries; iEvent++) {
      tree->GetEntry(iEvent);
      progress.Print();

      int thisRun   = event->GetRunNumber();
      int thisBurst = event->GetBurstNumber();

      bool runChanged   = thisRun != latestRun;
      bool burstChanged = runChanged || thisBurst != latestBurst;

      if (eventInfo) { // not the very first time we're here
        if (runChanged) {
          processMonitor.EndOfRun(*eventInfo);
          for (auto &analysis : treeAnalyzers)
            analysis->DoEndOfRun(*eventInfo);
        }

        if (burstChanged) {
          processMonitor.EndOfBurst(*eventInfo);
          if (!skipBurst)
            for (auto &analysis : treeAnalyzers)
              analysis->DoEndOfBurst(*eventInfo);
        }
      }

      if (!eventInfo) eventInfo = EventInfo();
      eventInfo->Update(tree->GetCurrentFile()->GetName(), treeType, iEvent, event);
      if (burstChanged)
        skipBurst = !eventInfo->IsMC() && eventInfo->IsRealBadBurst();

      latestRun   = thisRun;
      latestBurst = thisBurst;

      if (runChanged) {
        processMonitor.StartOfRun(*eventInfo);
        for (auto &analysis : treeAnalyzers)
          analysis->DoStartOfRun(*eventInfo);
      }

      if (burstChanged) {
        processMonitor.StartOfBurst(*eventInfo);
        if (!skipBurst)
          for (auto &analysis : treeAnalyzers)
            analysis->DoStartOfBurst(*eventInfo);
      }

      processMonitor.Process(*eventInfo);

      if (!skipBurst)
        for (auto &analysis : treeAnalyzers)
          analysis->DoProcess(*eventInfo, event);

    } // end event loop

    // last end of run
    if (eventInfo) {
      for (auto &analysis : treeAnalyzers) {
        processMonitor.EndOfBurst(*eventInfo);
        if (!skipBurst)
          analysis->DoEndOfBurst(*eventInfo);

        processMonitor.EndOfRun(*eventInfo);
        analysis->DoEndOfRun(*eventInfo);
      }
    }

    // end of tree
    progress.End();

    for (auto &analysis : treeAnalyzers)
      analysis->DoEndOfTree(treeType);

    processMonitor.Write();
    processMonitor.PrintSummary();

    std::cout << "#####  End processing " << treeName << "    #####" << std::endl
              << std::endl;

    delete tree; // free memory
  }

  // end of job
  for (auto &analysis : executable) {
    analysis->DoEndOfJob();
    delete analysis; // free memory + triggers histogram autowrite if enabled
  }

  outfile->Close();
  std::cout << "Output written to file: " << opts.OutputFilename << std::endl;
}
