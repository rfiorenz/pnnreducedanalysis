#pragma once

#include "VAnalysis.hh"
#include <TTree.h>

struct SampleEstimation {
  double NExpected = NAN, NExpectedError = NAN;
  int    NObserved = -1;
  int    NSampleC  = -1;
  double fScale = NAN, fScaleError = NAN;
  double K = NAN, KError = NAN;
};

class UpstreamEstimation : public VAnalysis {
public:
  UpstreamEstimation()
      : VAnalysis("UpstreamEstimation", std::vector{
                                            InputHisto("K2piNormAnalysis/Pmistag", "Pmatch", true),
                                            InputHisto("UpstreamAnalysis/UpstreamSample", true),
                                            InputHisto("UpstreamAnalysis/Validation", true),
                                        }){};

  void EndOfJob();

  const auto &GetEstimations() const { return fSamples; };

  static constexpr std::array MomentumBins = {15., 20., 25., 30., 35., 40., 45.};

protected:
  std::set<int> fSampleIDs;

  std::map<int, std::array<SampleEstimation, MomentumBins.size()>> fSamples; // indicized by validation sample and momentum bin

  std::map<int, std::array<std::shared_ptr<HistoHandler>, MomentumBins.size()>> fSampleHisto;

  const std::set<int> FindSamples(TTree *upstreamTree);
  void                DoNObserved(TTree *validationTree, int sample, int momentumBin);
  void                DoNExpected(TTree *upstreamTree, int sample, int momentumBin);

  const std::pair<double, double> Fscale(TTree *upstreamTree, int sample, int momentumBin);
  TH2D                           *SampleCHisto(TTree *upstreamTree, int sample, int momentumBin);
  TEfficiency                    *Pmistag();
};