#pragma once

#include "VAnalysis.hh"

class TestAnalysis : public VAnalysis {
public:
  TestAnalysis(std::set<Pnn::ReducedTree> trees = {Pnn::TNORMK2PI, Pnn::TKMU2}, std::vector<InputHisto> histos = {InputHisto("PnnTrackAssociation/CTRL_trig", true)})
      : VAnalysis("TestAnalysis", trees, histos){};

  TestAnalysis(std::unique_ptr<TestAnalysis> child)
      : fSubAnalysis(std::move(child)), VAnalysis("TestSubAnalysis", child.get()){}; // take ownership of child analysis, pass a copy to base class

protected:
  void StartOfJob();
  void Process(const EventInfo &eventInfo, PnnNA62Event *event);
  void EndOfJob();

  void ProcessHistos(HistoHandler &histos);

  std::shared_ptr<HistoHandler> fKmu2Histo;

  std::unique_ptr<TestAnalysis> fSubAnalysis;
};