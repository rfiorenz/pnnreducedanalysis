#pragma once

#include <vector>
#include <set>
#include <memory>
#include <TString.h>
#include <TFile.h>

#include "HistoHandler.hh"
#include "PnnNA62Event.hh"
#include "ReducedTree.hh"
#include "EventInfo.hh"

#define BUILD(...)     \
  using namespace Pnn; \
  std::vector<VAnalysis *> executable = {__VA_ARGS__};

class VAnalysis {
public:
  struct InputHisto {
    const TString                HistoPath;  // path in the file
    const TString                Name;       // name that will be used in the temp container
    const std::optional<TString> MergedPath; // if not empty, merge the histo and save it to the histohandler under this path

    InputHisto(TString histoPath, TString name = "", std::optional<TString> mergedPath = {})
        : HistoPath(histoPath), Name(name != "" ? name : GetNameFromPath(histoPath)), MergedPath(mergedPath){};

    InputHisto(TString histoPath, TString name, TString mergedPath)
        : InputHisto(histoPath, name, std::optional<TString>{mergedPath}){};

    InputHisto(TString histoPath, TString name, const char *mergedPath)
        : InputHisto(histoPath, name, std::optional<TString>{mergedPath}){};

    InputHisto(TString histoPath, TString name, bool saveMerged)
        : InputHisto(histoPath, name, saveMerged ? "" : std::optional<TString>{}){};

    InputHisto(TString histoPath, bool saveMerged)
        : InputHisto(histoPath, "", saveMerged ? "" : std::optional<TString>{}){};

  private:
    static TString GetNameFromPath(TString path) { return path(path.Last('/') + 1, path.Length()); };
  };

  VAnalysis(TString name, Pnn::ReducedTree tree, std::vector<InputHisto> inputHistos = {});
  VAnalysis(TString name, std::set<Pnn::ReducedTree> trees, std::vector<InputHisto> inputHistos = {});
  VAnalysis(TString name, VAnalysis *subanalysis);
  VAnalysis(TString name, std::vector<VAnalysis *> subanalysis);
  VAnalysis(TString name, std::vector<InputHisto> inputHistos);
  VAnalysis(TString name, std::vector<InputHisto> inputHistos, VAnalysis *subanalysis);
  VAnalysis(TString name, std::vector<InputHisto> inputHistos, std::vector<VAnalysis *> subanalysis);
  VAnalysis(TString name, std::set<Pnn::ReducedTree> trees, std::vector<VAnalysis *> subanalysis);
  VAnalysis(TString name, std::set<Pnn::ReducedTree> trees, std::vector<InputHisto> inputHistos, std::vector<VAnalysis *> subanalysis);
  virtual ~VAnalysis(){};

  const TString                    GetName() const { return fName; }
  const std::set<Pnn::ReducedTree> GetTrees() const { return fTrees; };
  const std::set<Pnn::ReducedTree> GetAllTrees() const;
  const bool                       ProcessesHistos() const;

  TString GetSummary(Pnn::ReducedTree tree) const;

  template <typename T = TH1>
  T *GetHisto(TString name, TString newName) const // gives a copy of a histo
  {
    auto obj = fHisto->Get<T>(name);
    if (obj == NULL)
      return NULL;
    return static_cast<T *>(obj->Clone(newName));
  };

  void InitHistoHandlers(TDirectory *directory);

  void DoStartOfJob();
  void DoStartOfTree(Pnn::ReducedTree tree);
  void DoStartOfRun(const EventInfo &eventInfo);
  void DoStartOfBurst(const EventInfo &eventInfo);
  void DoProcess(const EventInfo &eventInfo, PnnNA62Event *event);
  void DoEndOfBurst(const EventInfo &eventInfo);
  void DoEndOfRun(const EventInfo &eventInfo);
  void DoEndOfTree(Pnn::ReducedTree tree);
  void DoEndOfJob();

  void DoStartOfHistos();
  void DoProcessFile(TFile *file);
  void DoEndOfHistos();

protected:
  virtual void StartOfJob(){};
  virtual void StartOfTree(Pnn::ReducedTree tree) { StartOfTree(); };
  virtual void StartOfRun(const EventInfo &eventInfo) { StartOfRun(); };
  virtual void StartOfBurst(const EventInfo &eventInfo) { StartOfBurst(); };
  virtual void Process(const EventInfo &eventInfo, PnnNA62Event *event) { Process(event); };
  virtual void EndOfBurst(const EventInfo &eventInfo) { EndOfBurst(); };
  virtual void EndOfRun(const EventInfo &eventInfo) { EndOfRun(); };
  virtual void EndOfTree(Pnn::ReducedTree tree) { EndOfTree(); };
  virtual void EndOfJob(){};

  virtual void StartOfTree(){};
  virtual void StartOfRun(){};
  virtual void StartOfBurst(){};
  virtual void Process(PnnNA62Event *event) { Process(); };
  virtual void EndOfBurst(){};
  virtual void EndOfRun(){};
  virtual void EndOfTree(){};

  virtual void Process(){};

  virtual void StartOfHistos(){};
  virtual void ProcessHistos(HistoHandler &histos){};
  virtual void EndOfHistos(){};

  std::unique_ptr<HistoHandler> fHisto; // pointer, because it must be NULL when creating the analysis // FIXME: this is not true anymore!!

private:
  void ProcessFile(TFile *file);

  const TString                    fName;
  const std::set<Pnn::ReducedTree> fTrees;
  const std::vector<InputHisto>    fInputHistos;
  std::vector<VAnalysis *>         fSubAnalysis;

  mutable std::optional<bool> fProcessesHistos; // cached value
};