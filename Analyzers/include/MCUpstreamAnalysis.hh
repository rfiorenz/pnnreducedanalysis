#pragma once

#include "VAnalysis.hh"

#include <fstream>
#include <TTree.h>

class MCUpstreamAnalysis : public VAnalysis {
public:
  MCUpstreamAnalysis()
      : VAnalysis("MCUpstreamAnalysis", Pnn::TNORMK2PI){};

protected:
  void StartOfTree();
  void Process(PnnNA62Event *event);

  std::shared_ptr<HistoHandler> fSelectionHisto;

  TTree        *fOutputTree;
  PnnNA62Event *fTreeEvent;
  // PnnNA62Trigger            *fTreeTrigger;
  // PnnNA62UpstreamParticle   *fTreeUP;
  // PnnNA62DownstreamParticle *fTreeDP;
};