#pragma once

#include "VAnalysis.hh"
#include "UpstreamEstimation.hh"
#include <TTree.h>

class FinalNumbers : public VAnalysis {
public:
  FinalNumbers(UpstreamEstimation *upstreamEstimation = new UpstreamEstimation())
      : VAnalysis("FinalNumbers", MakeListOfInputHistos(), upstreamEstimation), fUpstreamEstimation(upstreamEstimation){};

  void StartOfHistos();                     // hotfix for MC processing
  void ProcessHistos(HistoHandler &histos); // hotfix for MC processing
  void EndOfHistos();
  void EndOfJob();

protected:
  std::unique_ptr<UpstreamEstimation> fUpstreamEstimation;

  void Kmu2Bkg(TH2F *pnnSelected);
  void K2piBkg(TH2F *pnnSelected);
  void Pi0Rejection();
  void UpstreamBackground();
  void CorrectedRandomVeto();
  void NK();

  std::pair<double, double> SES(TH1 *acceptance, double br, double brError, TString nExpectedHistoname);

  void Print();

  TH1 *EfficiencyToHisto(TEfficiency *efficiency, TString newName, std::array<double, 6> defaultContents = {-1}, std::array<double, 6> defaultErrors = {-1});

  double fRandomVetoCorrected, fRandomVetoCorrectedError;

private:
  static const std::vector<InputHisto> MakeListOfInputHistos();
};