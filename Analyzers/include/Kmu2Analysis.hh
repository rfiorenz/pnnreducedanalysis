#pragma once

#include "VAnalysis.hh"

class Kmu2Analysis : public VAnalysis {
public:
  Kmu2Analysis(TString name = "Kmu2Analysis")
      : VAnalysis(name, Pnn::TKMU2){};
  void StartOfJob();
  void StartOfRun();
  void Process(PnnNA62Event *event);
  void EndOfRun(const EventInfo &eventInfo);
  void EndOfJob();

protected:
  std::shared_ptr<HistoHandler> fKmu2Histo, fRVHisto, fTailsHisto;

  int fNKmu2;

private:
  void TailsAnalysis(PnnNA62Event *event);
  void RandomVetoAnalysis(PnnNA62Event *event);
};