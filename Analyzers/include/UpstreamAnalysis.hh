#pragma once

#include "VAnalysis.hh"
#include "PnnSelection.hh"

#include <TTree.h>

class UpstreamAnalysis : public VAnalysis {
public:
  UpstreamAnalysis()
      : VAnalysis("UpstreamAnalysis", Pnn::TPNN){};
  void StartOfJob();
  void Process(const EventInfo &eventInfo, PnnNA62Event *event);
  void EndOfJob();

protected:
  std::shared_ptr<HistoHandler>                                                       fSelectionHisto;
  std::array<std::shared_ptr<HistoHandler>, PnnSelection::NUpstreamValidationSamples> fSampleHisto;

  bool IsUpstreamSample(PnnNA62Event *event, int sample);

  // tree output
  TTree *fUpstreamTree, *fValidationTree;
  int    fRunID, fBurstID, fEventID, fNGTKTracks;
  double fCDA, fTGTK, fTKTAG, fTRICH, fMomentum;
  bool   fIsUpstreamSample, fKpiAssociation;
  int    fSample;
};