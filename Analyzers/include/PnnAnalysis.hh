#pragma once

#include "VAnalysis.hh"
#include <TTree.h>

class PnnAnalysis : public VAnalysis {
public:
  enum Option {
    kEvaluateAcceptance,
    kSaveEvents,
    kUnblind,
  };

  PnnAnalysis(TString name = "PnnAnalysis", std::set<Option> options = {kSaveEvents})
      : VAnalysis(name, Pnn::TPNN, options.count(kEvaluateAcceptance) ? std::vector{InputHisto("PnnTrackAssociation/Prod_zvtx", "MCZvtx", true)} : std::vector<InputHisto>{}), fOptions(options) {};
  void StartOfJob();
  void Process(const EventInfo &eventInfo, PnnNA62Event *event);
  void EndOfBurst();
  void EndOfJob();

  bool IsEventForPi0RejectionAnalysis() { return fEventIsForPi0RejectionAnalysis; };

protected:
  std::set<Option> fOptions;

  std::shared_ptr<HistoHandler> fSelectionHisto;

  bool fEventIsForPi0RejectionAnalysis;

  TTree                     *fSelectedEvents;
  PnnNA62Event              *fTreeEvent;
  PnnNA62DownstreamParticle *fDownstreamParticle;
  PnnNA62UpstreamParticle   *fUpstreamParticle;
  PnnNA62Trigger            *fTrigger;
  int                        fRegion;
  double                     fMomentum;
};