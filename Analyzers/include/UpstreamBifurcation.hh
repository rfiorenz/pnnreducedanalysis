#pragma once

#include "VAnalysis.hh"

#include <TTree.h>

class UpstreamBifurcation : public VAnalysis {
public:
  UpstreamBifurcation()
      : VAnalysis("UpstreamBifurcation", Pnn::TPNN){};

protected:
  void StartOfJob();
  void Process(const EventInfo &eventInfo, PnnNA62Event *event);

  TTree *fTree;

  int    fRunID, fBurstID, fEventID, fPnnRegion, fSample;
  double fZVtx, fCDA, fTGTK, fTKTAG, fTRICH, fTCHOD;
  bool   fKpiAssociation, fIn, fUCS;

  std::shared_ptr<HistoHandler> fSelectionHisto;
};