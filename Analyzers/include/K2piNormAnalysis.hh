#pragma once

#include "VAnalysis.hh"

class K2piNormAnalysis : public VAnalysis {
public:
  K2piNormAnalysis(bool mc = false, TString name = "K2piNormAnalysis")
      : VAnalysis(name, Pnn::TNORMK2PI, mc ? std::vector{InputHisto("PnnTrackAssociation/Prod_zvtx", "MCZvtx", true)} : std::vector<InputHisto>{}){};
  void StartOfJob();
  void StartOfRun(const EventInfo &eventInfo);
  void StartOfBurst();
  void Process(const EventInfo &eventInfo, PnnNA62Event *event);
  void EndOfBurst();
  void EndOfJob();

  bool IsEventSelected() { return fEventSelected; };
  int  GetNormTrigDownscaling() { return fNormTrigDownscaling; };

protected:
  std::shared_ptr<HistoHandler> fSelectionHisto;

  int fNormTrigBit;
  int fIntensitySumPerBurst;
  int fNormTrigDownscaling;
  int fNpipiPerBurst;

  bool fEventSelected;
};