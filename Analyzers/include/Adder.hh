#pragma once

#include "VAnalysis.hh"

class Adder : public VAnalysis {
public:
  Adder(std::vector<InputHisto> inputHistos)
      : VAnalysis("Adder", inputHistos) {};

  static InputHisto Merge(TString histoPath, TString newPath = ""); // merge the input histo and put it in the Adder directory, unless a new path is specified
};