#pragma once

#include "VAnalysis.hh"

class K2piAnalysis : public VAnalysis {
public:
  K2piAnalysis(TString name = "K2piAnalysis")
      : VAnalysis(name, Pnn::TK2PI){};

  void StartOfJob();
  void Process(PnnNA62Event *event);
  void EndOfJob();

protected:
  std::shared_ptr<HistoHandler> fSelectionHisto;
};