#pragma once

#include "VAnalysis.hh"
#include "PnnAnalysis.hh"
#include "K2piNormAnalysis.hh"
#include "Kmu2Analysis.hh"

class Pi0Rejection : public VAnalysis {

public:
  Pi0Rejection(PnnAnalysis *pnnAnalysis = new PnnAnalysis(), K2piNormAnalysis *normAnalysis = new K2piNormAnalysis());

  void StartOfJob();
  void Process(const EventInfo &eventInfo, PnnNA62Event *event);
  void EndOfJob();

protected:
  std::unique_ptr<PnnAnalysis>      fPnnAnalysis;
  std::unique_ptr<K2piNormAnalysis> fK2piNormAnalysis;

  void Pi0RejectionAnalysis(Pnn::ReducedTree tree, PnnNA62Event *event, int weight = 1);
};