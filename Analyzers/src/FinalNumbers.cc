#include "FinalNumbers.hh"
#include "PnnSelection.hh"
#include <TH2F.h>
#include <TEfficiency.h>
#include <TGraphErrors.h>
#include <TMultiGraph.h>
#include <TChain.h>
#include <TF1.h>
#include <iomanip>
#include <cmath>

void FillConstant(TH1 *h, double constant, double error = 0, bool fillOverUnderFlow = true)
{
  h->Reset();
  for (int i = !fillOverUnderFlow; i <= h->GetNbinsX() + fillOverUnderFlow; i++) {
    h->SetBinContent(i, constant);
    h->SetBinError(i, error);
  }
}

const std::vector<VAnalysis::InputHisto> FinalNumbers::MakeListOfInputHistos()
{
  std::vector<InputHisto> inputHistos;

  static const TString kmu2Data = "Kmu2Analysis";
  static const TString k2piData = "K2piAnalysis";
  static const TString pnnData  = "PnnAnalysis";
  static const TString normData = "K2piNormAnalysis";
  static const TString kmu2MC   = "Kmu2MC";
  static const TString pnnMC    = "PnnMC";
  static const TString normMC   = "K2piNormMC";
  static const TString ke4MC    = "Ke4MC";

  // pi0 rejection
  for (auto &v : Pnn::PnnVetoes)
    inputHistos.emplace_back("Pi0Rejection/pi0rejection_" + Pnn::VetoToString.at(v), "uncorrected_" + Pnn::VetoToString.at(v), "Pi0Rejection");

  // random veto
  inputHistos.emplace_back(kmu2Data + "/RandomVeto", true);
  inputHistos.emplace_back(kmu2MC + "/RandomVeto", "RandomVetoMC", true);

  // background
  inputHistos.emplace_back(kmu2Data + "/tailsDen", "Kmu2tailsDen", true);
  inputHistos.emplace_back(kmu2Data + "/tailsNum", "Kmu2tailsNum", true);
  inputHistos.emplace_back(k2piData + "/tailsDen", "K2pitailsDen", true);
  inputHistos.emplace_back(k2piData + "/tailsNum", "K2pitailsNum", true);
  inputHistos.emplace_back(ke4MC + "/MCAcceptance", "Ke4Acceptance", true);

  // pnn selection
  inputHistos.emplace_back(pnnData + "/p_region", "PnnSelected", true);
  inputHistos.emplace_back(normData + "/downscaled_p", "NpipiD", true);
  inputHistos.emplace_back(normData + "/Npipi_run_burst", true);

  // MC acceptance
  inputHistos.emplace_back(normMC + "/MCAcceptance", "K2piAcceptance", true);
  inputHistos.emplace_back(pnnMC + "/MCAcceptance", "PnnAcceptance", true);

  // upstream
  inputHistos.emplace_back("UpstreamBifurcation/Bifurcation", "UpstreamBifurcation", true);
  inputHistos.emplace_back("UpstreamAnalysis/UpstreamSample", true);
  inputHistos.emplace_back("UpstreamAnalysis/Validation", "UpstreamValidation", true);

  // processing
  for (auto &[_, treeStr] : Pnn::TreeToString) {
    inputHistos.emplace_back(Form("BurstsProcessed/%s", treeStr.Data()), treeStr + "Processed"); // FIXME: go back to ..., "", "BurstsPorcessed");
    inputHistos.emplace_back(Form("BadBursts/%s_BadBursts_inclusive", treeStr.Data()), "", "BadBursts");
    inputHistos.emplace_back(Form("BadBursts/%s_BadBursts_exclusive", treeStr.Data()), "", "BadBursts");
  }

  return inputHistos;
}

void FinalNumbers::StartOfHistos() // FIXME: reprocess everything and remove this thing
{
  // hotfix: manual merge of processing monitors
  for (auto &[_, treeStr] : Pnn::TreeToString)
    fHisto->BookHisto(new TChain(Form("BurstsProcessed/%s", treeStr.Data())), "", treeStr);
}

void FinalNumbers::ProcessHistos(HistoHandler &histos) // FIXME: reprocess everything and remove this thing
{
  // hotfix: manual merge of processing monitors
  static const std::vector<TString> MChistos = {
      "RandomVetoMC",
      "Ke4Acceptance",
      "K2piAcceptance",
      "PnnAcceptance",
  };

  bool isMC = std::any_of(MChistos.begin(), MChistos.end(), [&](const TString &histoname) { return histos.Get<TObject>(histoname) != nullptr; });

  if (!isMC)
    for (auto &[_, treeStr] : Pnn::TreeToString) {
      auto tree = histos.Get<TTree>(treeStr + "Processed");
      if (tree && tree->GetCurrentFile())
        fHisto->Get<TChain>(treeStr)->AddFile(tree->GetCurrentFile()->GetName(), 0);
    }
}

TH1 *FinalNumbers::EfficiencyToHisto(TEfficiency *eff, TString newName, std::array<double, 6> defaultContents, std::array<double, 6> defaultErrors)
{
  TH1 *h = nullptr;
  if (eff && eff->GetTotalHistogram()->GetEntries()) {
    h = eff->GetCopyPassedHisto();
    h->Divide(h, eff->GetTotalHistogram(), 1, 1, "B");
    h->SetNameTitle(newName, eff->GetTitle());
  } else if (std::none_of(defaultContents.begin(), defaultContents.end(), [](double c) { return c < 0; })) {
    h = new TH1F(newName, Form("Default %s", newName.Data()), 6, 15, 45);
    // would like to SetContent(defaultContents), but first index is the underflow...
    std::vector<double> buffer(defaultContents.size() + 2);
    std::copy(defaultContents.begin(), defaultContents.end(), buffer.begin() + 1);
    h->SetContent(buffer.data());
    std::copy(defaultErrors.begin(), defaultErrors.end(), buffer.begin() + 1);
    h->SetError(buffer.data());
  }
  return h;
}

void FinalNumbers::EndOfHistos()
{
  auto BookEfficiencyHisto = [&](TString path, TString newName, std::array<double, 6> defaultContents, std::array<double, 6> defaultErrors) {
    fHisto->BookHisto(EfficiencyToHisto(fHisto->Get<TEfficiency>(path), newName, defaultContents, defaultErrors));
  };

  BookEfficiencyHisto("RandomVeto", "RandomVetoHisto",
                      {0.594903, 0.606778, 0.609678, 0.605782, 0.599024, 0.589217},
                      {0.00018293600, 0.00011115300, 8.9479600e-05, 8.3102600e-05, 8.3612750e-05, 8.2848350e-05});

  BookEfficiencyHisto("RandomVetoMC", "RandomVetoMCHisto",
                      {0.914129, 0.944528, 0.954396, 0.952753, 0.940913, 0.929434},
                      {0.0024826350, 0.0012454850, 0.00092243950, 0.00086726700, 0.00096946350, 0.0010259050});

  BookEfficiencyHisto("TriggerEfficiency", "TriggerEfficiencyHisto",
                      {0.921222, 0.893612, 0.868705, 0.848593, 0.831602, 0.814206},
                      {0.006402, 0.006216, 0.006048, 0.005914, 0.005803, 0.005694});

  BookEfficiencyHisto("K2piAcceptance", "K2piAcceptanceHisto",
                      {0.0139487, 0.024012, 0.029698, 0.0281029, 0.0267566, 0.0219858},
                      {5.2663650e-05, 6.8712750e-05, 7.6182600e-05, 7.4171900e-05, 7.2425950e-05, 6.5822150e-05}); // overestimated, but meh

  BookEfficiencyHisto("PnnAcceptance", "PnnAcceptanceHisto",
                      {0.0113937, 0.0178486, 0.0194828, 0.0164668, 0.010752, 0.00742461},
                      {3.8056300e-05, 4.7460200e-05, 4.9541250e-05, 4.5620650e-05, 3.6983100e-05, 3.0794650e-05}); // overestimated, but meh
}

void FinalNumbers::EndOfJob()
{
  auto pnnselected = fHisto->Get<TH2F>("PnnSelected");

  CorrectedRandomVeto();
  Kmu2Bkg(pnnselected);
  K2piBkg(pnnselected);
  Pi0Rejection();
  UpstreamBackground();
  NK();

  Print();

  fHisto->UnbookHisto("UpstreamSample");
  fHisto->UnbookHisto("UpstreamValidation");
}

void FinalNumbers::Kmu2Bkg(TH2F *pnnselected)
{
  if (!pnnselected)
    return;

  int   kmu2bin = pnnselected->GetYaxis()->FindFixBin(Pnn::KinematicRegionToString.at(Pnn::kKmu2));
  TH1D *kmu2bkg = pnnselected->ProjectionX("NKmu2Bkg", kmu2bin, kmu2bin);

  auto kmu2tailsNum = fHisto->Get<TH1>("Kmu2tailsNum");
  if (!kmu2tailsNum)
    return;
  kmu2bkg->Multiply(kmu2tailsNum);

  auto kmu2tailsDen = fHisto->Get<TH1>("Kmu2tailsDen");
  if (!kmu2tailsDen)
    return;
  kmu2bkg->Divide(kmu2tailsDen);

  fHisto->BookHisto(kmu2bkg);
}

void FinalNumbers::K2piBkg(TH2F *pnnselected)
{
  if (!pnnselected)
    return;

  int   k2pibin = pnnselected->GetYaxis()->FindFixBin(Pnn::KinematicRegionToString.at(Pnn::kK2pi));
  TH1D *k2pibkg = pnnselected->ProjectionX("NK2piBkg", k2pibin, k2pibin);

  auto k2pitailsNum = fHisto->Get<TH1>("K2pitailsNum");
  if (!k2pitailsNum)
    return;
  k2pibkg->Multiply(k2pitailsNum);

  auto k2pitailsDen = fHisto->Get<TH1>("K2pitailsDen");
  if (!k2pitailsDen)
    return;
  k2pibkg->Divide(k2pitailsDen);

  fHisto->BookHisto(k2pibkg);
}

void FinalNumbers::Pi0Rejection()
{
  // already calculated, but not yet corrected for RV and trigger efficiency
  for (auto &v : Pnn::PnnVetoes) {
    auto uncorrected = fHisto->Get<TEfficiency>("uncorrected_" + Pnn::VetoToString.at(v));
    if (!uncorrected)
      continue;

    TH1 *numerator = uncorrected->GetCopyPassedHisto();
    // sometimes these histos have weird binning... the wonders of ROOT
    numerator->SetBins(6, 15, 45);
    for (int i = 1; i <= numerator->GetNbinsX(); i++) {
      int originalbin = uncorrected->GetPassedHistogram()->FindFixBin(numerator->GetBinCenter(i));
      numerator->SetBinContent(i, uncorrected->GetPassedHistogram()->GetBinContent(originalbin));
      numerator->SetBinError(i, uncorrected->GetPassedHistogram()->GetBinError(originalbin));
    }

    TH1 *denominator = uncorrected->GetCopyTotalHisto();
    denominator->SetBins(6, 15, 45);
    for (int i = 1; i <= denominator->GetNbinsX(); i++) {
      int originalbin = uncorrected->GetTotalHistogram()->FindFixBin(denominator->GetBinCenter(i));
      denominator->SetBinContent(i, uncorrected->GetTotalHistogram()->GetBinContent(originalbin));
      denominator->SetBinError(i, uncorrected->GetTotalHistogram()->GetBinError(originalbin));
    }

    auto htemp = static_cast<TH1F *>(numerator->Clone("htemp"));
    FillConstant(htemp, fRandomVetoCorrected, fRandomVetoCorrectedError);
    numerator->Divide(htemp);
    numerator->Divide(fHisto->Get<TH1>("TriggerEfficiencyHisto"));

    TEfficiency *corrected;
    ROOTErrorIgnoreLevel(kWarning, [&] { // silence "Info: weighted events"
      corrected = new TEfficiency(*numerator, *denominator);
    });
    corrected->SetNameTitle("pi0rejection_" + Pnn::VetoToString.at(v), "#pi^{0} rejection after " + Pnn::VetoToString.at(v));
    fHisto->BookHisto(corrected, "Pi0Rejection");
  }
}

void FinalNumbers::UpstreamBackground()
{
  static const double errorX  = 0.3;
  auto                samples = fUpstreamEstimation->GetEstimations();

  auto fillGraphs = [](const SampleEstimation &sample, double x, double dx, TGraphErrors *gExp, TGraphErrors *gObs) {
    if (sample.NExpected >= 0) { // do not draw point if expectation failed
      gExp->SetPoint(gExp->GetN(), x, sample.NExpected);
      gExp->SetPointError(gExp->GetN() - 1, dx, sample.NExpectedError);
    }

    if (sample.NObserved > 0) { // do not draw point if zero observed
      gObs->SetPoint(gObs->GetN(), x, sample.NObserved);
      gObs->SetPointError(gObs->GetN() - 1, 0, sqrt(sample.NObserved));
    }
  };

  auto makeMultiGraph = [](TString name, TString title, TGraphErrors *gExp, TGraphErrors *gObs) {
    TMultiGraph *mg = new TMultiGraph(name, title);
    gExp->SetFillColor(kRed - 10);
    gObs->SetMarkerStyle(20);
    mg->Add(gExp, "2");
    mg->Add(gObs, "P");
    return mg;
  };

  // total expected / observed
  TGraphErrors             *totalExpectedGraph = new TGraphErrors();
  TGraphErrors             *totalObservedGraph = new TGraphErrors();
  int                       iSample            = 0;
  std::map<double, TString> xLabels;
  for (auto &[sampleID, estimations] : samples) {
    iSample++;
    SampleEstimation totalEstimations = estimations.at(0);
    if (sampleID == 0) totalEstimations.NObserved = 0; // blinding for signal
    xLabels[iSample] = sampleID == 0 ? "Signal" : Form("%d", sampleID);
    fillGraphs(totalEstimations, iSample, errorX, totalExpectedGraph, totalObservedGraph);
  }

  // make multigraph and book it
  TMultiGraph *mg = makeMultiGraph("UpstreamBackground", "Upstream Background;Sample", totalExpectedGraph, totalObservedGraph);
  ROOTErrorIgnoreLevel(kWarning, [&] { mg->Draw("A"); }); // need to draw in order to be able to change x axis labels...
  for (auto &[x, label] : xLabels)
    mg->GetXaxis()->SetBinLabel(mg->GetXaxis()->FindBin(x), label);
  fHisto->BookHisto(mg);

  // graphs per sample
  for (auto &[sampleID, estimations] : samples) {
    TGraphErrors *expectedGraph = new TGraphErrors();
    TGraphErrors *observedGraph = new TGraphErrors();
    for (int iBin = 1; iBin < estimations.size(); iBin++) {
      SampleEstimation estimation = estimations.at(iBin);
      if (sampleID == 0) estimation.NObserved = 0; // blinding for signal
      double pmin = UpstreamEstimation::MomentumBins.at(iBin - 1);
      double pmax = UpstreamEstimation::MomentumBins.at(iBin);
      fillGraphs(estimation, (pmax + pmin) / 2, (pmax - pmin) / 2, expectedGraph, observedGraph);
    }
    TString sampleName = sampleID == 0 ? "Signal" : Form("Sample%d", sampleID);
    fHisto->BookHisto(makeMultiGraph(sampleName, ";p [GeV/c]", expectedGraph, observedGraph), "UpstreamBackground_pBins");
  }
}

void FinalNumbers::CorrectedRandomVeto()
{
  auto rvData = fHisto->Get<TH1>("RandomVetoHisto");
  auto rvMC   = fHisto->Get<TH1>("RandomVetoMCHisto");

  auto rvCorrected = (TH1 *)rvData->Clone("RandomVetoCorrected");
  rvCorrected->Divide(rvMC);

  ROOTErrorIgnoreLevel(kWarning, [&] { rvCorrected->Fit("pol0", "Q", "goff"); }); // ROOT just does not shut up
  fRandomVetoCorrected = rvCorrected->GetFunction("pol0")->GetParameter(0);
  std::vector<double> deviations;
  for (int i = 1; i <= rvCorrected->GetNbinsX(); i++)
    deviations.push_back(abs(rvCorrected->GetBinContent(i) - fRandomVetoCorrected));
  fRandomVetoCorrectedError = *std::max_element(deviations.begin(), deviations.end());
  fHisto->BookHisto(rvCorrected);
}

void FinalNumbers::NK()
{
  // K2pi BR = 0.2067 +- 0.0008
  // N_K = (NpipiD * 400) / (K2piBR * K2piAcceptance)

  if (!fHisto->Get<TH1>("NpipiD"))
    return;

  auto nk = static_cast<TH1F *>(fHisto->Get<TH1>("NpipiD")->Clone("NK"));
  nk->Scale(400);
  auto htemp = static_cast<TH1F *>(nk->Clone("htemp"));
  FillConstant(htemp, 0.2067, 0.0008);
  nk->Divide(htemp);
  nk->Divide(fHisto->Get<TH1>("K2piAcceptanceHisto"));
  nk->Scale(1. / fHisto->Get<TH1>("K2piAcceptanceHisto")->GetNbinsX());
  fHisto->BookHisto(nk);

  // N_effectiveK = N_K * triggerEfficiency * randomVeto
  auto nkeffective = static_cast<TH1F *>(nk->Clone("NKEffective"));
  nkeffective->Multiply(fHisto->Get<TH1>("TriggerEfficiencyHisto"));
  FillConstant(htemp, fRandomVetoCorrected, fRandomVetoCorrectedError);
  nkeffective->Multiply(htemp);
  fHisto->BookHisto(nkeffective);

  delete htemp;
}

std::pair<double, double> FinalNumbers::SES(TH1 *acceptance, double br, double brError, TString nExpectedHistoname)
{
  // SES = 1 / (N_K_effective * PnnAcceptance)
  // N_pnn = SMBR / SES = SMBR * PnnAcceptance * N_K_effective

  auto nkeffective = fHisto->Get<TH1F>("NKEffective");
  if (!nkeffective || !acceptance)
    return {NAN, NAN};

  auto nexpected = static_cast<TH1F *>(nkeffective->Clone(nExpectedHistoname));
  nexpected->Multiply(acceptance);
  nexpected->Scale(acceptance->GetNbinsX());
  double inverseSES, inverseSESerror;
  inverseSES = nexpected->IntegralAndError(1, nexpected->GetNbinsX(), inverseSESerror);

  auto htemp = static_cast<TH1F *>(nexpected->Clone("htemp"));
  FillConstant(htemp, br, brError);
  nexpected->Multiply(htemp);

  fHisto->BookHisto(nexpected);
  delete htemp;
  return {1 / inverseSES, inverseSESerror / pow(inverseSES, 2)};
}

template <typename... Args>
const char *FormattedFormat(const char *fmt, const char *numberfmt, Args... args)
{
  return Form(fmt, Form(numberfmt, args)...);
}

std::pair<double, double> PrintTotal(TEfficiency *eff, const char *fmt = "%g", bool acceptance = false)
{
  if (eff == nullptr) {
    std::cout << "N/A" << std::endl;
    return {NAN, NAN};
  }

  double num = eff->GetPassedHistogram()->Integral();
  double den = eff->GetTotalHistogram()->Integral();
  if (acceptance)
    den /= eff->GetTotalHistogram()->GetNbinsX();
  double central = num / den;
  double high    = TEfficiency::ClopperPearson(den, num, TMath::Erf(1 / sqrt(2.)), 1) - central;
  double low     = central - TEfficiency::ClopperPearson(den, num, TMath::Erf(1 / sqrt(2.)), 0);

  std::cout << FormattedFormat("%s +%s -%s", fmt, central, high, low) << std::endl;
  return {central, std::max(high, low)};
}

std::pair<double, double> PrintTotal(TH1 *histo, const char *fmt = "%g")
{
  if (histo == nullptr) {
    std::cout << "N/A" << std::endl;
    return {NAN, NAN};
  }
  double total, error;
  total = histo->IntegralAndError(1, histo->GetNbinsX(), error);
  std::cout << FormattedFormat("%s +- %s", fmt, total, error) << std::endl;
  return {total, error};
}

std::pair<double, double> PrintTotal(TH2 *histo, const char *fmt = "%g")
{
  if (histo == nullptr) {
    std::cout << "N/A" << std::endl;
    return {NAN, NAN};
  }
  double total, error;
  total = histo->IntegralAndError(1, histo->GetNbinsX(), 1, histo->GetNbinsY(), error);
  std::cout << FormattedFormat("%s +- %s", fmt, total, error) << std::endl;
  return {total, error};
}

void FinalNumbers::Print()
{
  static const int width = 25;

  std::cout << std::endl
            << std::endl;

  auto printLabel = [&](TString label) { std::cout << std::setw(width) << label << ": "; };

  auto printNumber = [&](TString label, double number, double error, const char *fmt = "%g") {
    printLabel(label);
    std::cout << (std::isnan(number) ? "N/A" : (error != 0 ? Form(TString(fmt) + " +- " + fmt, number, error) : Form(fmt, number))) << std::endl;
  };
  auto printEfficiency = [&](TString label, TEfficiency *eff, const char *fmt = "%g") {
    printLabel(label);
    return PrintTotal(eff, fmt);
  };
  auto printTotal = [&](TString label, auto histo, const char *fmt = "%g") {
    printLabel(label);
    return PrintTotal(histo, fmt);
  };
  auto printAcceptance = [&](TString label, TEfficiency *acceptance, const char *fmt = "%g") {
    printLabel(label);
    return PrintTotal(acceptance, fmt, true);
  };

  long long nGoodK2piNormBursts = 0;
  for (auto reducedTree : {Pnn::TPNN, Pnn::TNORMK2PI}) {
    TString   treeName    = Pnn::TreeToString.at(reducedTree);
    TTree    *tree        = fHisto->Get<TTree>(treeName);
    long long nBursts     = tree->Draw("!RealBadBurst", "", "goff");
    long long nGoodBursts = 0;
    for (int i = 0; i < nBursts; i++)
      nGoodBursts += tree->GetV1()[i];
    std::cout << std::setw(10) << treeName << " bursts processed: " << nBursts << "\tof which good: " << nGoodBursts << std::endl;

    if (reducedTree == Pnn::TNORMK2PI)
      nGoodK2piNormBursts = nGoodBursts;
  }

  {
    auto Npipi_run_burst = fHisto->Get<TH1>("Npipi_run_burst");
    auto burstsWithK2pi  = static_cast<TH1 *>(Npipi_run_burst->Clone("BurstsWithK2pi"));
    burstsWithK2pi->Divide(Npipi_run_burst);
    printNumber("N(bursts, Npipi > 0)", burstsWithK2pi->Integral(), 0);
  }
  printTotal("Npipi x D/400", fHisto->Get<TH1>("NpipiD"));
  printTotal("Random Veto", fHisto->Get<TEfficiency>("RandomVeto"));
  printTotal("Random Veto noovMC", fHisto->Get<TEfficiency>("RandomVetoMC"));
  printNumber("Random Veto corrected", fRandomVetoCorrected, fRandomVetoCorrectedError);
  printTotal("Trigger efficiency", fHisto->Get<TEfficiency>("TriggerEfficiency"));
  printAcceptance("MCAcceptance K2pi", fHisto->Get<TEfficiency>("K2piAcceptance"));
  printAcceptance("MCAcceptance Pnn", fHisto->Get<TEfficiency>("PnnAcceptance"));
  printTotal("N(K)", fHisto->Get<TH1>("NK"));
  printTotal("N(K,effective)", fHisto->Get<TH1>("NKEffective"));
  auto [SESvalue, SESerror] = SES(fHisto->Get<TH1>("PnnAcceptanceHisto"), 8.4e-11, 1.0e-11, "Npnn");
  printNumber("SES", SESvalue, SESerror);
  auto [nPnn, nPnnError] = printTotal("N(pnn)", fHisto->Get<TH1>("Npnn"));
  printNumber("N(pnn)/good bursts", nPnn / nGoodK2piNormBursts, nPnnError / nGoodK2piNormBursts); // error on n good bursts is negligible
  // TODO: B/S's and total
  printTotal("N(Kmu2)", fHisto->Get<TH1>("NKmu2Bkg"));
  printTotal("N(K2pi)", fHisto->Get<TH1>("NK2piBkg"));
  auto ke4acceptanceHisto = EfficiencyToHisto(fHisto->Get<TEfficiency>("Ke4Acceptance"), "Ke4AcceptanceHisto");
  SES(ke4acceptanceHisto, 4.247e-5, 0.024e-05, "NKe4Bkg");
  if (ke4acceptanceHisto) delete ke4acceptanceHisto;
  printTotal("N(Ke4)", fHisto->Get<TH1>("NKe4Bkg"));
  SampleEstimation upstreamEstimation{};
  if (fUpstreamEstimation->GetEstimations().count(0))
    upstreamEstimation = fUpstreamEstimation->GetEstimations().at(0)[0];
  printNumber("N(sampleC)", upstreamEstimation.NSampleC >= 0 ? double(upstreamEstimation.NSampleC) : NAN, 0);
  printNumber("fCDA", upstreamEstimation.fScale, upstreamEstimation.fScaleError);
  printNumber("N(ups)", upstreamEstimation.NExpected, upstreamEstimation.NExpectedError);

  double nUpstreamBifurcation = NAN, nUpstreamBifurcationError = NAN;
  double nPnnOut = NAN, nUcsIn = NAN, nUcsOut = NAN;
  if (auto hBifurcation = fHisto->Get<TH2>("UpstreamBifurcation")) {
    nPnnOut                   = hBifurcation->GetBinContent(2, 1);
    nUcsIn                    = hBifurcation->GetBinContent(1, 2);
    nUcsOut                   = hBifurcation->GetBinContent(1, 1);
    nUpstreamBifurcation      = nPnnOut * nUcsIn / nUcsOut;
    nUpstreamBifurcationError = nUpstreamBifurcation * sqrt(1 / nPnnOut + 1 / nUcsIn + 1 / nUcsOut);
  }
  printNumber("N(Pnn,out)", nPnnOut, 0);
  printNumber("N(UCS,in)", nUcsIn, 0);
  printNumber("N(UCS,out)", nUcsOut, 0);
  printNumber("N(upsBifurcation)", nUpstreamBifurcation, nUpstreamBifurcationError);

  std::cout << std::endl;
}
