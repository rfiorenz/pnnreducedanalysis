#include "Adder.hh"

VAnalysis::InputHisto Adder::Merge(TString histoPath, TString newPath)
{
  if (newPath == "")
    return VAnalysis::InputHisto(histoPath, true);

  TString newName = newPath(newPath.Last('/') + 1, newPath.Length());
  TString dirName = newPath(0, newPath.Last('/'));
  dirName         = dirName.Strip(TString::kTrailing, '/');

  return VAnalysis::InputHisto(histoPath, newName, dirName);
};