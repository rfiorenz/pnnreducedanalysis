#include "UpstreamBifurcation.hh"
#include "PnnSelection.hh"

void UpstreamBifurcation::StartOfJob()
{
  fSelectionHisto = fHisto->MakeChild();

  auto h = new TH2F("Bifurcation", ";Zvtx+BDT;Time+Matching", 2, 0, 2, 2, 0, 2);
  h->GetXaxis()->SetBinLabel(1, "UCS");
  h->GetXaxis()->SetBinLabel(2, "PNN");
  h->GetYaxis()->SetBinLabel(1, "OutOfTime");
  h->GetYaxis()->SetBinLabel(2, "InTime");
  h->SetOption("text0");
  fHisto->BookHisto(h);
  for (int i = 0; i < PnnSelection::NUpstreamValidationSamples; i++)
    fHisto->BookHisto(static_cast<TH2F *>(h->Clone(Form("BifurcationValidation%d", i))));

  fTree = new TTree("BifurcationSamples", "");

  fTree->Branch("RunID", &fRunID);
  fTree->Branch("BurstID", &fBurstID);
  fTree->Branch("EventID", &fEventID);
  fTree->Branch("PnnRegion", &fPnnRegion);
  fTree->Branch("ZVtx", &fZVtx);
  fTree->Branch("CDA", &fCDA);
  fTree->Branch("TGTK", &fTGTK);
  fTree->Branch("TRICH", &fTRICH);
  fTree->Branch("TKTAG", &fTKTAG);
  fTree->Branch("TCHOD", &fTCHOD);
  fTree->Branch("KpiAssociation", &fKpiAssociation);
  fTree->Branch("In", &fIn);
  fTree->Branch("UCS", &fUCS);
  fTree->Branch("Sample", &fSample);
  fHisto->BookHisto(fTree);
}

void UpstreamBifurcation::Process(const EventInfo &eventInfo, PnnNA62Event *event)
{
  PnnSelection pnn(event, *fSelectionHisto);

  pnn.DisableHistoFilling = true;
  // (redundant) blinding
  if (!eventInfo.IsMC() && eventInfo.GetRunID() > 10000 && pnn.Vertex() && pnn.Timing() && pnn.UpstreamValidationSample() == 0)
    return;
  pnn.DisableHistoFilling = false;

  pnn.MonitorCut("Total");
  PNNSELECT(pnn, TriggerMask, 1)
  PNNSELECT(pnn, CDA)
  PNNSELECT(pnn, PionPIDCalo)
  PNNSELECT(pnn, PionPIDRICH)
  PNNSELECT(pnn, BroadMultivertex)
  PNNSELECT(pnn, PhotonVeto)
  PNNSELECT(pnn, MultiplicityVeto)
  PNNSELECT(pnn, CobraVeto)
  PNNSELECT(pnn, ANTI0Veto)

  double momentum = pnn.GetTrackMomentum();
  if (!(15 < momentum && momentum < 45))
    return;
  pnn.MonitorCut("Momentum cut");
  auto fPnnRegion = pnn.GetRegion();
  if (!(fPnnRegion == Pnn::kPNN1 || fPnnRegion == Pnn::kPNN2 || fPnnRegion == Pnn::kUpstream)) return;
  pnn.MonitorCut("Signal Region");

  fRunID          = eventInfo.GetRunID();
  fBurstID        = eventInfo.GetBurstID();
  fEventID        = eventInfo.GetEventNumber();
  fZVtx           = event->GetVertex().Z();
  fCDA            = event->GetCDA();
  fTCHOD          = event->GetDownstreamParticle()->GetTCHOD();
  fTKTAG          = event->GetUpstreamParticle()->GetTKTAG();
  fTGTK           = event->GetUpstreamParticle()->GetTGTK();
  fTRICH          = event->GetDownstreamParticle()->GetTRICH();
  fKpiAssociation = pnn.KPiAssociationBayes();
  fSample         = pnn.UpstreamValidationSample();
  if (fSample < 0)
    return;

  std::array<double, 3> dt = {fTGTK - fTKTAG,
                              fTGTK - fTRICH,
                              fTKTAG - fTRICH};

  // bifurcate Zvtx
  // PNN: Vertex && BoxCut; UCS: UCSVertex
  if (pnn.Vertex()) {
    pnn.MonitorCut("Vertex");
    PNNSELECT(pnn, BoxCut)
    fUCS = false;
  } else if (pnn.Vertex(true)) {
    pnn.MonitorCut("UCSVertex");
    fUCS = true;
  } else
    return;

  // bifurcate timing
  // in: Timing && KpiAssociation; out: !GTKRICHKTAG-intime
  if (pnn.Timing()) {
    pnn.MonitorCut("Timing");
    if (!fKpiAssociation)
      return;
    pnn.MonitorCut("KpiAssociationBayes");
    fIn = true;
  } else if (std::any_of(dt.begin(), dt.end(), [](double dtime) { return abs(dtime) > 0.5; })) {
    fIn = false;
  } else
    return;

  if (fSample == 0 && !fUCS && fIn) // redundant blinding for signal box
    return;

  fTree->Fill();

  fHisto->FillHisto(fSample == 0 ? "Bifurcation" : Form("BifurcationValidation%d", fSample), !fUCS, fIn);
}
