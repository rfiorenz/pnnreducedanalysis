#include "MCUpstreamAnalysis.hh"
#include <iostream>
#include <bitset>
#include "globals.hh"
#include "PnnSelection.hh"

void MCUpstreamAnalysis::StartOfTree()
{
  fSelectionHisto = fHisto->MakeChild();

  for (auto mode : {"singleVeto", "sequenceVeto", "exclusiveVeto"})
    fHisto->BookHisto(new TH1F(mode, ";Veto;Events passing veto", Pnn::PnnVetoes.size(), 0, Pnn::PnnVetoes.size()), "PnnVeto");

  fHisto->BookHisto(new TH2F("TruePiTheta", ";#theta_{X};#theta_{Y}", 800, -0.01, 0.01, 3500, -0.01, 0.025));
  fHisto->BookHisto(new TH2F("TrueZvtx_Xvtx", ";Zvtx [m];Xvtx [m]", 800, 65000, 105000, 800, -40, 40));
  fHisto->BookHisto(new TH2F("TrueZvtx_Yvtx", ";Zvtx [m];Yvtx [m]", 800, 65000, 105000, 1500, -100, 50));
  fHisto->BookHisto(new TH2F("TrueZvtx_ThetaX", ";Zvtx [m];#theta_{X}", 800, 65000, 105000, 800, -0.01, 0.01));
  fHisto->BookHisto(new TH2F("TrueZvtx_ThetaY", ";Zvtx [m];#theta_{Y}", 800, 65000, 105000, 3500, -0.01, 0.025));
  fHisto->BookHisto(new TH2F("TrueZvtx_Ppi", ";Zvtx [m];P [GeV/c]", 800, 65000, 105000, 300, 0, 75));
  fHisto->BookHisto(new TH2F("TruePpi_ThetaX", ";P [GeV/c];#theta_{X}", 300, 0, 75, 800, -0.01, 0.01));
  fHisto->BookHisto(new TH2F("TruePpi_ThetaY", ";P [GeV/c];#theta_{Y}", 300, 0, 75, 3500, -0.01, 0.025));
  fHisto->BookHisto(new TH2F("TrueZvtx_RecoMmiss", ";Zvtx [m];m^{2}_{miss} (GeV^{2}/c^{4})", 800, 65000, 105000, 900, -0.3, 0.15));
  fHisto->BookHisto(new TH2F("TrueZvtx_RecoPpi", ";Zvtx [m];P [GeV/c]", 800, 65000, 105000, 300, 0, 75));
  fHisto->BookHisto(new TH2F("TrueZvtx_rSTRAW1", ";Zvtx [m];R_{STRAW1} [mm]", 800, 65000, 105000, 120, 0, 1200));
  fHisto->BookHisto(new TH1F("hCDA", "CDA;mm", 400, 0, 4));

  fOutputTree = new TTree("SelectedSample", "");
  fTreeEvent  = new PnnNA62Event();
  fOutputTree->Branch("NA62Event", fTreeEvent);
  fHisto->BookHisto(fOutputTree);
}

void MCUpstreamAnalysis::Process(PnnNA62Event *event)
{
  PnnSelection selection(event, *fSelectionHisto);
  selection.MonitorCut("Events");

  // no vertex cut

  // beam background: remove IsInteraction (apart from small effect on crowded events in GTK, everything else is geometry against upstream)
  if (event->GetIsHighToT()) return;
  selection.MonitorCut("HighToT");
  if (event->GetUpstreamParticle()->GetNGTKTracks() > 5) return;
  selection.MonitorCut("MaxGTKTracks");

  // GTK Extra Hits
  short          GTKExtraHitsWord = event->GetUpstreamParticle()->GetGTKExtraHits();
  std::bitset<4> extraHitsRICH    = GTKExtraHitsWord,      // 4 LSBs = hits in time with RICH
      extraHitsKTAG               = GTKExtraHitsWord >> 4; // 4 MSBs = hits in time with KTAG

  auto isExtraHitsVeto = [](std::bitset<4> extraHits) {
    return (extraHits[0] || extraHits[1]) && (extraHits[2] || extraHits[3]); // at least 2 hits, of which one in GTK2 or 3
  };

  if (isExtraHitsVeto(extraHitsKTAG) || isExtraHitsVeto(extraHitsRICH))
    return;
  selection.MonitorCut("GTKExtraHits");

  // Rest of selection
  PNNSELECT(selection, CHANTI)
  PNNSELECT(selection, Timing)
  PNNSELECT(selection, CDA)
  PNNSELECT(selection, PionPIDCalo)
  PNNSELECT(selection, PionPIDRICH)
  PNNSELECT(selection, BroadMultivertex)
  PNNSELECT(selection, KPiAssociationBayes)
  PNNSELECT(selection, CobraVeto)
  PNNSELECT(selection, ANTI0Veto)

  bool veto = false;

  for (const auto &mode : {
           std::make_pair(PnnSelection::kSingle, "singleVeto"),
           std::make_pair(PnnSelection::kSequence, "sequenceVeto"),
           std::make_pair(PnnSelection::kExclusive, "exclusiveVeto"),
       })
    for (auto &v : selection.PnnVeto(Pnn::PnnVetoes, mode.first))
      if (v.second)
        fSelectionHisto->FillHisto(mode.second, Pnn::VetoToString.at(v.first), 1);

  // No BoxCut

  PNNSELECT(selection, PhotonVeto)
  PNNSELECT(selection, MultiplicityVeto)

  selection.MonitorCut("End");

  fHisto->FillHisto("TruePiTheta", event->GetMCppiX() / event->GetMCppiZ(), event->GetMCppiY() / event->GetMCppiZ());
  fHisto->FillHisto("TrueZvtx_Ppi", event->GetMCKdecZ(), event->GetMCppiZ());
  fHisto->FillHisto("TrueZvtx_Xvtx", event->GetMCKdecZ(), event->GetMCKdecX());
  fHisto->FillHisto("TrueZvtx_Yvtx", event->GetMCKdecZ(), event->GetMCKdecY());
  fHisto->FillHisto("TrueZvtx_ThetaX", event->GetMCKdecZ(), event->GetMCppiX() / event->GetMCppiZ());
  fHisto->FillHisto("TrueZvtx_ThetaY", event->GetMCKdecZ(), event->GetMCppiY() / event->GetMCppiZ());
  fHisto->FillHisto("TruePpi_ThetaX", selection.GetTrackMomentum(), event->GetMCppiX() / event->GetMCppiZ());
  fHisto->FillHisto("TruePpi_ThetaY", selection.GetTrackMomentum(), event->GetMCppiY() / event->GetMCppiZ());
  fHisto->FillHisto("TrueZvtx_RecoMmiss", event->GetMCKdecZ(), event->GetMMiss());
  fHisto->FillHisto("TrueZvtx_RecoPpi", event->GetMCKdecZ(), selection.GetTrackMomentum());
  fHisto->FillHisto("hCDA", event->GetCDA());
  fHisto->FillHisto("TrueZvtx_rSTRAW1", event->GetMCKdecZ(), selection.GetRSTRAW1());

  deep_copy_TObject(event, fTreeEvent);
  fOutputTree->Fill();
}
