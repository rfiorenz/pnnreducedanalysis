#include "PnnAnalysis.hh"
#include "KinematicRegion.hh"
#include <iostream>
#include <TEfficiency.h>
#include "globals.hh"
#include "PnnSelection.hh"

void PnnAnalysis::StartOfJob()
{
  fSelectionHisto = fHisto->MakeChild();

  auto h = new TH2F("p_region", "Selected events", 6, 15, 45, Pnn::KinematicRegions.size(), 0, Pnn::KinematicRegions.size());
  for (size_t i = 0; i < Pnn::KinematicRegions.size(); i++)
    h->GetYaxis()->SetBinLabel(i + 1, Pnn::KinematicRegionToString.at(Pnn::KinematicRegions.at(i)));
  fHisto->BookHisto(h);

  if (fOptions.count(kSaveEvents)) {
    fTreeEvent          = new PnnNA62Event();
    fUpstreamParticle   = new PnnNA62UpstreamParticle();
    fDownstreamParticle = new PnnNA62DownstreamParticle();
    fTrigger            = new PnnNA62Trigger();

    fSelectedEvents = new TTree("SelectedEvents", "");
    fSelectedEvents->Branch("Region", &fRegion);
    fSelectedEvents->Branch("Momentum", &fMomentum);
    fSelectedEvents->Branch("NA62Event", &fTreeEvent);
    fSelectedEvents->Branch("UpstreamParticle", &fUpstreamParticle);
    fSelectedEvents->Branch("DownstreamParticle", &fDownstreamParticle);
    fSelectedEvents->Branch("Trigger", &fTrigger);
    fHisto->BookHisto(fSelectedEvents);
  }
}

void PnnAnalysis::Process(const EventInfo &eventInfo, PnnNA62Event *event)
{
  fEventIsForPi0RejectionAnalysis = false;

  PnnSelection pnn(event, *fSelectionHisto);

  auto region = pnn.GetRegion();

  if (!fOptions.count(kUnblind) && !eventInfo.IsMC() && 0 < region && region < 100) // apply (redundant) blinding
    return;

  pnn.MonitorCut("Total");
  PNNSELECT(pnn, TriggerMask, 1)
  PNNSELECT(pnn, Vertex)
  PNNSELECT(pnn, CHANTI)
  PNNSELECT(pnn, BeamBackground)
  PNNSELECT(pnn, VetoCounterVeto)
  PNNSELECT(pnn, Timing)
  PNNSELECT(pnn, CDA)
  PNNSELECT(pnn, KPiAssociationBayes)
  PNNSELECT(pnn, MUV3Veto)
  PNNSELECT(pnn, PionPIDCalo)
  PNNSELECT(pnn, PionPIDRICH)
  PNNSELECT(pnn, BroadMultivertex)
  PNNSELECT(pnn, BoxCut)
  PNNSELECT(pnn, CobraVeto)
  PNNSELECT(pnn, ANTI0Veto)

  double momentum                 = pnn.GetTrackMomentum();
  fEventIsForPi0RejectionAnalysis = (15 < momentum && momentum < 45) && (region == Pnn::kK2pi || region == Pnn::kControl1 || region == Pnn::kControl2);

  PNNSELECT(pnn, PhotonVeto)
  PNNSELECT(pnn, MultiplicityVeto)

  PNNSELECT(pnn, Kmu2gVeto)

  pnn.MonitorCut("End");

  fHisto->FillHisto<TH2>("p_region", momentum, Pnn::KinematicRegionToString.at(region), 1);

  if (fOptions.count(kSaveEvents)) {
    fRegion   = pnn.GetRegion();
    fMomentum = pnn.GetTrackMomentum();
    deep_copy_TObject(event, fTreeEvent);
    deep_copy_TObject(event->GetUpstreamParticle(), fUpstreamParticle);
    deep_copy_TObject(event->GetDownstreamParticle(), fDownstreamParticle);
    deep_copy_TObject(event->GetTrigger(), fTrigger);
    fSelectedEvents->Fill();
  }
}

void PnnAnalysis::EndOfBurst()
{
  fEventIsForPi0RejectionAnalysis = false; // avoid persistence of state
}

void PnnAnalysis::EndOfJob()
{
  // MC acceptance
  TH1 *MCZvtx = fHisto->Get<TH1>("MCZvtx");
  if (MCZvtx != nullptr && MCZvtx->GetEntries() != 0) {
    auto pnnHisto   = fHisto->Get<TH2>("p_region");
    auto region1bin = pnnHisto->GetYaxis()->FindFixBin(Pnn::KinematicRegionToString.at(Pnn::kPNN1));
    auto region2bin = pnnHisto->GetYaxis()->FindFixBin(Pnn::KinematicRegionToString.at(Pnn::kPNN2));

    // numerator
    auto num = pnnHisto->ProjectionX("region1", region1bin, region1bin);
    num->Add(pnnHisto->ProjectionX("region2", region2bin, region2bin));

    // denominator
    auto den = static_cast<TH1F *>(num->Clone("acceptance_denominator"));
    den->Reset();
    for (int i = 0; i <= den->GetNbinsX() + 1; i++)
      den->SetBinContent(i, MCZvtx->Integral(MCZvtx->FindBin(105e3), MCZvtx->FindBin(170e3 - 1)));

    // ratio
    auto acceptance = new TEfficiency(*num, *den);
    acceptance->SetName("MCAcceptance");
    fHisto->BookHisto(acceptance);
  }
}