#include "UpstreamEstimation.hh"
#include <TTree.h>
#include <TF1.h>
#include <TH3.h>
#include <numeric>
#include "ProgressMonitor.hh"

const double MAXCDA  = 4;
const int    REBINDT = 10;

const char *MomentumSelection(int momentumBin)
{
  return Form("Momentum > %g && Momentum <= %g",
              momentumBin == 0 ? *UpstreamEstimation::MomentumBins.begin() : UpstreamEstimation::MomentumBins[momentumBin - 1],
              momentumBin == 0 ? UpstreamEstimation::MomentumBins.back() : UpstreamEstimation::MomentumBins[momentumBin]);
}

void UpstreamEstimation::EndOfJob()
{
  TTree *upstreamTree   = fHisto->Get<TTree>("UpstreamSample");
  TTree *validationTree = fHisto->Get<TTree>("Validation");

  if (!upstreamTree || !validationTree)
    return;

  fSampleIDs = FindSamples(upstreamTree);

  for (int validSample : fSampleIDs) {
    fSamples[validSample]     = {}; // declare a sample
    fSampleHisto[validSample] = {};
    for (int iBin = 0; iBin < MomentumBins.size(); iBin++) {
      TString samplename = validSample == 0 ? "SampleC" : Form("Sample%d", validSample);
      if (iBin != 0)
        samplename += Form("/p%d", iBin);
      fSampleHisto[validSample][iBin] = fHisto->MakeChild(samplename);
    }
  }

  auto pmistag = Pmistag();
  pmistag->SetName("Pmistag");
  fHisto->BookHisto(pmistag);

  std::cout << "Starting upstream estimation" << std::endl;
  ProgressMonitor monitor(fSampleIDs.size() * MomentumBins.size(), 1, 1, 20, 0.1);
  for (int validSample : fSampleIDs) {
    for (int iBin = 0; iBin < MomentumBins.size(); iBin++) {
      monitor.Print();
      DoNObserved(validationTree, validSample, iBin);
      DoNExpected(upstreamTree, validSample, iBin);
    }
  }
  monitor.End();
}

const std::set<int> UpstreamEstimation::FindSamples(TTree *upstreamTree)
{
  std::set<int> samples;

  long long nEntries = upstreamTree->Draw("Sample", "", "goff");
  for (int i = 0; i < nEntries; i++)
    samples.insert(upstreamTree->GetV1()[i]);

  return samples;
}

void UpstreamEstimation::DoNObserved(TTree *validationTree, int sample, int momentumBin)
{
  if (sample == 0) // blinding for signal
    return;

  ROOTErrorIgnoreLevel(kError, [&] { // silence possible "Warning in <TTreePlayer::DrawSelect>: The selected TTree subset is empty.", that's the point
    fSamples[sample][momentumBin].NObserved = validationTree->Draw("", Form("Sample == %d && %s", sample, MomentumSelection(momentumBin)), "goff");
  });
}

template <typename Container>
TH2 *WeightedProjection(TH3 *histo, const Container &weights, TString projectionOption) // WARNING: removes under- and over-flow. (not needed here!)
{
  // sanitize inputs
  projectionOption.ToLower();
  if (!std::set<TString>{"xy", "yz", "zx", "yx", "xz", "zy"}.count(projectionOption))
    throw std::runtime_error("Invalid projection option: " + projectionOption);
  TAxis *projectionAxis;
  if (!projectionOption.Contains("x"))
    projectionAxis = histo->GetXaxis();
  else if (!projectionOption.Contains("y"))
    projectionAxis = histo->GetYaxis();
  else if (!projectionOption.Contains("z"))
    projectionAxis = histo->GetZaxis();
  if (size_t(projectionAxis->GetNbins()) != weights.size())
    throw std::runtime_error(Form("Binning mismatch: Nbins = %d != %zu = Nweights ", projectionAxis->GetNbins(), weights.size()));

  TH2 *weightedProj = static_cast<TH2 *>(histo->Project3D(projectionOption));
  weightedProj->Reset();
  weightedProj->Sumw2(); // need this to account for errors in the correct way

  for (size_t i = 0; i < weights.size(); i++) {
    projectionAxis->SetRange(i + 1, i + 1);
    auto hToAdd = histo->Project3D(projectionOption);
    weightedProj->Add(hToAdd, weights.at(i));
    delete hToAdd;
  }

  return weightedProj;
}

TEfficiency *UpstreamEstimation::Pmistag()
{
  TEfficiency *Pmatch      = fHisto->Get<TEfficiency>("Pmatch");
  auto         numerator   = static_cast<TH3 *>(Pmatch->GetCopyPassedHisto());
  auto         denominator = static_cast<TH3 *>(Pmatch->GetCopyTotalHisto());

  TH1D               *cdaDistribution = denominator->ProjectionX();
  std::vector<double> weights;
  std::transform(cdaDistribution->GetArray() + 1,
                 cdaDistribution->GetArray() + cdaDistribution->GetNbinsX() + 1,
                 std::back_inserter(weights), [&](double content) { return content > 0 ? cdaDistribution->Integral() / content : 0; });

  TEfficiency *eff;
  ROOTErrorIgnoreLevel(kWarning, [&] { // silence info about weighted events (that's exactly the point)
    eff = new TEfficiency(*WeightedProjection(numerator, weights, "zy")->RebinX(REBINDT), *WeightedProjection(denominator, weights, "zy")->RebinX(REBINDT));
  });
  delete numerator;
  delete denominator;
  return eff;
}

// TODO: pass the data rather than the tree (read the tree only once)
const std::pair<double, double> UpstreamEstimation::Fscale(TTree *upstreamTree, int sample, int momentumBin)
{
  int nSampleC;
  ROOTErrorIgnoreLevel(kError, [&] { // silence possible "Warning in <TTreePlayer::DrawSelect>: The selected TTree subset is empty.", we catch that
    nSampleC = upstreamTree->Draw("CDA", Form("Sample == %d && IsUpstreamSample && CDA > %g && %s", sample, MAXCDA, MomentumSelection(momentumBin)), "goff");
  });
  if (nSampleC == 0)
    return {-1, -1};
  TH1F *cdaDistribution = new TH1F("fscale", "", 25, 0, 100);
  for (int i = 0; i < nSampleC; i++)
    cdaDistribution->Fill(upstreamTree->GetV1()[i]);

  double nFirstBin    = cdaDistribution->GetBinContent(2);
  double nSecondBin   = cdaDistribution->GetBinContent(3);
  double fscale       = (nFirstBin + nSecondBin) / (2. * nSampleC);
  double fscaleStatSq = ((nFirstBin + nSecondBin) * (nSampleC - (nFirstBin + nSecondBin)) / pow(nSampleC, 3)) / pow(2, 2); // binomial of (n1 + n2) / N, then account for extra factor 2
  double fscaleSystSq = pow((nFirstBin - nSecondBin) / (2. * nSampleC), 2);                                                // syst
  double fscaleError  = sqrt(fscaleStatSq + fscaleSystSq);

  // plot fscale
  cdaDistribution->Scale(1. / nSampleC);
  cdaDistribution->SetBinContent(1, fscale);
  cdaDistribution->SetEntries(nSampleC); // previous line increments the entries by 1
  cdaDistribution->SetBinError(1, fscaleError);
  cdaDistribution->SetBinError(0, sqrt(fscaleStatSq)); // store stat error
  fSampleHisto[sample][momentumBin]->BookHisto(cdaDistribution);

  return {fscale, fscaleError};
}

TH2D *UpstreamEstimation::SampleCHisto(TTree *upstreamTree, int sample, int momentumBin)
{
  TH2D *sampleCHisto = static_cast<TH2D *>(fHisto->Get<TEfficiency>("Pmistag")->GetCopyPassedHisto()); // take binning from Pmistag axes
  sampleCHisto->SetNameTitle("SampleCDTpNGTK", "");
  sampleCHisto->Reset();
  int nSampleC = upstreamTree->Draw("abs(TGTK-TKTAG + TGTK-TRICH)/sqrt(2.):NGTKTracks", Form("Sample == %d && IsUpstreamSample && CDA > %g && %s", sample, MAXCDA, MomentumSelection(momentumBin)), "goff");
  for (int i = 0; i < nSampleC; i++)
    sampleCHisto->Fill(upstreamTree->GetV1()[i], upstreamTree->GetV2()[i]);

  return sampleCHisto;
}

// TODO: pass the data rather than the tree (read the tree only once)
void UpstreamEstimation::DoNExpected(TTree *upstreamTree, int sample, int momentumBin)
{
  auto &[fscale, fscaleError] = Fscale(upstreamTree, sample, momentumBin);
  if (fscale < 0) // no statistics, return
    return;

  TH2D *sampleCHisto = SampleCHisto(upstreamTree, sample, momentumBin);

  // K = sum(N_sampleC * Pmistag)
  TH2 *NexpectedHisto             = static_cast<TH2 *>(HistoHandler::ApplyEfficiency(sampleCHisto, fHisto->Get<TEfficiency>("Pmistag"), "NUpstream"));
  fSamples[sample][momentumBin].K = NexpectedHisto->IntegralAndError(0, NexpectedHisto->GetNbinsX() + 1, 0, NexpectedHisto->GetNbinsY() + 1, fSamples[sample][momentumBin].KError);

  double NexpectedError;
  NexpectedHisto->Scale(fscale);
  double Nexpected = NexpectedHisto->IntegralAndError(0, NexpectedHisto->GetNbinsX() + 1, 0, NexpectedHisto->GetNbinsY() + 1, NexpectedError);
  NexpectedError   = Nexpected * sqrt(pow(NexpectedError / Nexpected, 2) + pow(fscaleError / fscale, 2)); // add in quadrature the relative error on fscale

  // output
  fSampleHisto[sample][momentumBin]->BookHisto(sampleCHisto);
  fSampleHisto[sample][momentumBin]->BookHisto(NexpectedHisto);
  fSamples[sample][momentumBin].NSampleC       = sampleCHisto->GetEntries();
  fSamples[sample][momentumBin].fScale         = fscale;
  fSamples[sample][momentumBin].fScaleError    = fscaleError;
  fSamples[sample][momentumBin].NExpected      = Nexpected;
  fSamples[sample][momentumBin].NExpectedError = NexpectedError;
}
