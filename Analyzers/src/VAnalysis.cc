#include "VAnalysis.hh"
#include "TChain.h"
#include <numeric>
#include <iostream>

VAnalysis::VAnalysis(TString name, Pnn::ReducedTree tree, std::vector<InputHisto> inputHistos)
    : VAnalysis(name, {tree}, inputHistos, {})
{
}
VAnalysis::VAnalysis(TString name, std::set<Pnn::ReducedTree> trees, std::vector<InputHisto> inputHistos)
    : VAnalysis(name, trees, inputHistos, {})
{
}
VAnalysis::VAnalysis(TString name, VAnalysis *subanalysis)
    : VAnalysis(name, {}, {}, {subanalysis})
{
}
VAnalysis::VAnalysis(TString name, std::vector<VAnalysis *> subanalysis)
    : VAnalysis(name, {}, {}, subanalysis)
{
}

VAnalysis::VAnalysis(TString name, std::vector<InputHisto> inputHistos)
    : VAnalysis(name, {}, inputHistos, {}) {}

VAnalysis::VAnalysis(TString name, std::vector<InputHisto> inputHistos, VAnalysis *subanalysis)
    : VAnalysis(name, {}, inputHistos, {subanalysis})
{
}
VAnalysis::VAnalysis(TString name, std::vector<InputHisto> inputHistos, std::vector<VAnalysis *> subanalysis)
    : VAnalysis(name, {}, inputHistos, subanalysis)
{
}
VAnalysis::VAnalysis(TString name, std::set<Pnn::ReducedTree> trees, std::vector<VAnalysis *> subanalysis)
    : VAnalysis(name, trees, {}, subanalysis)
{
}

VAnalysis::VAnalysis(TString name, std::set<Pnn::ReducedTree> trees, std::vector<InputHisto> inputHistos, std::vector<VAnalysis *> subanalysis)
    : fName(name), fTrees(trees), fInputHistos(inputHistos), fSubAnalysis(subanalysis)
{
  // protect against null pointers: remove them
  fSubAnalysis.erase(std::remove_if(fSubAnalysis.begin(), fSubAnalysis.end(), [](VAnalysis *an) { return an == NULL; }), fSubAnalysis.end());
}

void VAnalysis::InitHistoHandlers(TDirectory *directory)
{
  for (auto &a : fSubAnalysis)
    a->InitHistoHandlers(directory);

  fHisto = std::make_unique<HistoHandler>(directory, fName);
  directory->mkdir(fName);
}

const std::set<Pnn::ReducedTree> VAnalysis::GetAllTrees() const
{
  std::set<Pnn::ReducedTree> result;
  for (auto &a : fSubAnalysis) {
    auto trees = a->GetAllTrees();
    result.insert(trees.begin(), trees.end());
  }

  result.insert(fTrees.begin(), fTrees.end());

  return result;
}

TString VAnalysis::GetSummary(Pnn::ReducedTree tree) const
{
  if (GetAllTrees().count(tree) == 0)
    return "";

  TString summary = fName;

  if (fSubAnalysis.size() > 0)
    summary += TString(fTrees.count(tree) ? "+" : " ") +
               "(" +
               std::accumulate(
                   std::next(fSubAnalysis.begin()), fSubAnalysis.end(),
                   fSubAnalysis[0]->GetSummary(tree),
                   [&](TString total, VAnalysis *an) {
                     TString summary = an->GetSummary(tree);
                     if (summary != "" && total != "")
                       summary = ", " + summary;
                     return std::move(total) + summary;
                   }) +
               ")";

  return summary;
}

const bool VAnalysis::ProcessesHistos() const
{
  if (!fProcessesHistos.has_value())
    fProcessesHistos = !fInputHistos.empty() || std::any_of(fSubAnalysis.begin(), fSubAnalysis.end(), [](VAnalysis *sub) { return sub->ProcessesHistos(); });

  return fProcessesHistos.value();
}

void VAnalysis::DoStartOfJob()
{
  for (auto &a : fSubAnalysis)
    a->DoStartOfJob();

  StartOfJob();
}

void VAnalysis::DoStartOfTree(Pnn::ReducedTree tree)
{
  for (auto &a : fSubAnalysis)
    a->DoStartOfTree(tree);

  if (fTrees.count(tree))
    StartOfTree(tree);
}

void VAnalysis::DoStartOfRun(const EventInfo &eventInfo)
{
  for (auto &a : fSubAnalysis)
    a->DoStartOfRun(eventInfo);

  if (fTrees.count(eventInfo.GetTree()))
    StartOfRun(eventInfo);
}

void VAnalysis::DoStartOfBurst(const EventInfo &eventInfo)
{
  for (auto &a : fSubAnalysis)
    a->DoStartOfBurst(eventInfo);

  if (fTrees.count(eventInfo.GetTree()))
    StartOfBurst(eventInfo);
}

void VAnalysis::DoProcess(const EventInfo &eventInfo, PnnNA62Event *event)
{
  for (auto &a : fSubAnalysis)
    a->DoProcess(eventInfo, event);

  if (fTrees.count(eventInfo.GetTree()))
    Process(eventInfo, event);
}
void VAnalysis::DoEndOfBurst(const EventInfo &eventInfo)
{
  for (auto &a : fSubAnalysis)
    a->DoEndOfBurst(eventInfo);

  if (fTrees.count(eventInfo.GetTree()))
    EndOfBurst(eventInfo);
}
void VAnalysis::DoEndOfRun(const EventInfo &eventInfo)
{
  for (auto &a : fSubAnalysis)
    a->DoEndOfRun(eventInfo);

  if (fTrees.count(eventInfo.GetTree()))
    EndOfRun(eventInfo);
}
void VAnalysis::DoEndOfTree(Pnn::ReducedTree tree)
{
  for (auto &a : fSubAnalysis)
    a->DoEndOfTree(tree);

  if (fTrees.count(tree))
    EndOfTree(tree);
}
void VAnalysis::DoEndOfJob()
{
  for (auto &a : fSubAnalysis)
    a->DoEndOfJob();

  EndOfJob();
}

void VAnalysis::DoStartOfHistos()
{
  for (auto &a : fSubAnalysis)
    a->DoStartOfHistos();

  StartOfHistos();
}

void VAnalysis::DoProcessFile(TFile *file)
{
  for (auto &a : fSubAnalysis)
    a->DoProcessFile(file);

  ProcessFile(file);
}

void VAnalysis::DoEndOfHistos()
{
  for (auto &a : fSubAnalysis)
    a->DoEndOfHistos();

  EndOfHistos();
}

void VAnalysis::ProcessFile(TFile *file)
{
  HistoHandler histos;

  for (auto &inputHisto : fInputHistos) {
    TObject *obj = file->Get<TObject>(inputHisto.HistoPath);
    if (!obj) {
      // std::cerr << "WARNING: " << fName << " requested non existing InputHisto at " << file->GetName() << ":" << inputHisto.HistoPath << std::endl;
      continue;
    }

    histos.BookHisto(obj, "", inputHisto.Name);

    if (!inputHisto.MergedPath)
      continue;

    TString mergedPath = inputHisto.MergedPath.value();

    if (!obj->IsA()->GetMethodWithPrototype("Merge", "TCollection*")) { // check if we can merge it
      std::cerr << "WARNING: " << fName << " asked to merge " << inputHisto.HistoPath << " into " << mergedPath << " but " << obj->IsA()->GetName() << " has no Merge(TCollection*) method!" << std::endl;
      continue;
    }

    // special case: TTree. Make TChain
    if (obj->IsA() == TTree::Class()) {
      TChain *chain = fHisto->Get<TChain>(inputHisto.Name);
      if (!chain) {
        chain = new TChain(inputHisto.HistoPath);
        fHisto->BookHisto(chain, mergedPath, inputHisto.Name);
      }
      chain->AddFile(PrependXRDProtocol(file->GetName()), 0);
      continue;
    }

    // all other cases: use Merge(TCollection*)
    TObject *mergedObj = fHisto->Get<TObject>(inputHisto.Name);
    if (!mergedObj) { // not found, make it and book it
      mergedObj = obj->Clone(inputHisto.Name);
      fHisto->BookHisto(mergedObj, mergedPath, inputHisto.Name);
    } else {
      // ugly code to merge whatever this object is
      TList l;
      l.Add(obj);
      Int_t error = 0;
      mergedObj->Execute("Merge", Form("((TCollection*)0x%zx)", (size_t)&l), &error);
      if (error)
        std::cerr << "ERROR: " << fName << " encountered error " << error << " while merging " << inputHisto.Name << " in file " << file->GetName() << std::endl;
    }
  }

  ProcessHistos(histos);
};