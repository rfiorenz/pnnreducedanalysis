#include "Kmu2Analysis.hh"

#include <iostream>
#include <TH2F.h>
#include <TEfficiency.h>

#include "KinematicRegion.hh"
#include "PnnSelection.hh"

static const int                                    NIntensityBins    = 26;
static const std::array<double, NIntensityBins + 1> IntensityBinEdges = {0, 40, 60, 100, 150, 200, 250, 300, 350, 400, 450, 500, 550, 600, 650, 700, 750, 800, 850, 900, 950, 1000, 1050, 1100, 1200, 1400, 2000};

static const std::vector<Pnn::KinematicRegion> RegionBins = Pnn::KinematicRegions;

static const std::map<Pnn::KinematicRegion, TString> RegionString = Pnn::KinematicRegionToString;

void StyleRVEfficiency(TEfficiency *efficiency, Pnn::Veto v);

void Kmu2Analysis::StartOfJob()
{
  fKmu2Histo  = fHisto->MakeChild();
  fRVHisto    = fHisto->MakeChild("RandomVetoSelection");
  fTailsHisto = fHisto->MakeChild("TailsSelection");

  fHisto->BookHisto(new TH1F("NKmu2_vs_Run", ";Run;N(K_{#mu2})", 4000, 8514.5, 12514.5));

  fHisto->BookHisto(new TH1F("intensity", ";Intensity (MHz)", 500, 0, 5000));

  for (auto mode : {"singleVeto", "sequenceVeto", "exclusiveVeto"}) {
    auto h = new TH2F(mode, ";Intensity (MHz);Veto;Events passing veto", NIntensityBins, IntensityBinEdges.data(), Pnn::PnnVetoes.size(), 0, Pnn::PnnVetoes.size());
    fHisto->BookHisto(h);
  }

  fHisto->BookHisto(new TEfficiency("RandomVeto", ";p [GeV/c]", 6, 15, 45));

  auto h = new TH2F("p_region", "Region after tails selection; p (GeV/c)", 6, 15, 45, RegionBins.size(), 0, RegionBins.size());
  for (size_t iBin = 0; iBin < RegionBins.size(); iBin++)
    h->GetYaxis()->SetBinLabel(iBin + 1, RegionString.at(RegionBins[iBin]));
  fHisto->BookHisto(h);

  std::array probBins = {0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.99, 1.};
  fHisto->BookHisto(new TH2F("tailsNum_p_muonProb", ";p [GeV/c];P(#mu)", 6, 15, 45, probBins.size() - 1, probBins.data()));
  fHisto->BookHisto(new TH2F("tailsDen_p_muonProb", ";p [GeV/c];P(#mu)", 6, 15, 45, probBins.size() - 1, probBins.data()));
}

void Kmu2Analysis::StartOfRun()
{
  fNKmu2 = 0;
}

void Kmu2Analysis::Process(PnnNA62Event *event)
{
  PnnSelection kmu2(event, *fKmu2Histo);

  kmu2.MonitorCut("Total");

  PNNSELECT(kmu2, TriggerMask, 2)
  PNNSELECT(kmu2, Vertex)
  PNNSELECT(kmu2, CHANTI)
  PNNSELECT(kmu2, BeamBackground)
  PNNSELECT(kmu2, VetoCounterVeto)
  PNNSELECT(kmu2, Timing)
  PNNSELECT(kmu2, BoxCut)
  PNNSELECT(kmu2, CDA)
  PNNSELECT(kmu2, KPiAssociationBayes)
  PNNSELECT(kmu2, RICHPresence)
  PNNSELECT(kmu2, BroadMultivertex, true)
  PNNSELECT(kmu2, CobraVeto)
  PNNSELECT(kmu2, ANTI0Veto)

  if (kmu2.PionPIDCalo() && kmu2.MUV3Veto()) // orthogonal to PNN
    return;
  kmu2.MonitorCut("!PionPIDCalo");
  if (event->GetDownstreamParticle()->GetIsPositron())
    return;
  kmu2.MonitorCut("PositronVeto");
  if (event->GetDownstreamParticle()->GetCaloMuonProb() < 0.99)
    return;
  kmu2.MonitorCut("MuonPIDCaloProb");

  PNNSELECT(kmu2, Kmu2gVeto)

  if (!(15 < kmu2.GetTrackMomentum() && kmu2.GetTrackMomentum() < 45))
    return;
  kmu2.MonitorCut("MomentumCut");

  RandomVetoAnalysis(event);
  TailsAnalysis(event);
}

void Kmu2Analysis::TailsAnalysis(PnnNA62Event *event)
{
  PnnSelection tails(event, *fTailsHisto);

  PNNSELECT(tails, PhotonVeto)
  PNNSELECT(tails, MultiplicityVeto)

  PNNSELECT(tails, PionPIDRICH)

  auto region = tails.GetRegion();
  if (RegionString.count(region))
    fHisto->FillHisto<TH2>("p_region", tails.GetTrackMomentum(), RegionString.at(region), 1);

  TString histoToFill = "";
  if (region == Pnn::kKmu2)
    histoToFill = "tailsDen_p_muonProb";
  else if (region == Pnn::kPNN1 || region == Pnn::kPNN2)
    histoToFill = "tailsNum_p_muonProb";
  if (histoToFill != "")
    fHisto->FillHisto(histoToFill, tails.GetTrackMomentum(), event->GetDownstreamParticle()->GetCaloMuonProb());
}

void Kmu2Analysis::RandomVetoAnalysis(PnnNA62Event *event)
{
  PnnSelection rv(event, *fRVHisto);

  PNNSELECT(rv, MUV3SpatialAssociation)

  // remove HASC2 acceptance
  double hascZposition       = 253324;
  auto   downstreamParticle  = event->GetDownstreamParticle();
  auto   positionAfterMagnet = TVector3(
      downstreamParticle->GetPositionXAfterMagnet(),
      downstreamParticle->GetPositionYAfterMagnet(),
      downstreamParticle->GetPositionZAfterMagnet());
  auto slopeAfterMagnet = TVector2(downstreamParticle->GetSlopeXAfterMagnet(), downstreamParticle->GetSlopeYAfterMagnet());

  auto positionAtHASC = positionAfterMagnet.XYvector() + slopeAfterMagnet * (hascZposition - positionAfterMagnet.Z());
  if (positionAtHASC.X() > 0 && abs(positionAtHASC.Y()) < 400)
    return;
  rv.MonitorCut("!HASC2Acceptance");

  if (!(std::abs(event->GetMmuMiss()) < 0.005 && rv.GetRegion() == Pnn::KinematicRegion::kKmu2)) return;
  rv.MonitorCut("End");

  fNKmu2++;

  double intensity = event->GetLambdaFW();
  fHisto->FillHisto("intensity", intensity);

  fHisto->FillHisto<TEfficiency>("RandomVeto", rv.PhotonVeto() && rv.MultiplicityVeto(), rv.GetTrackMomentum());
  for (const auto &mode : {
           std::make_pair(PnnSelection::kSingle, "singleVeto"),
           std::make_pair(PnnSelection::kSequence, "sequenceVeto"),
           std::make_pair(PnnSelection::kExclusive, "exclusiveVeto"),
       })
    for (auto &v : rv.PnnVeto(Pnn::PnnVetoes, mode.first))
      if (v.second)
        fHisto->FillHisto<TH2>(mode.second, intensity, Pnn::VetoToString.at(v.first), 1);
}

void Kmu2Analysis::EndOfRun(const EventInfo &eventInfo)
{
  fHisto->FillHisto("NKmu2_vs_Run", eventInfo.GetRunID(), fNKmu2);
}

void Kmu2Analysis::EndOfJob()
{
  // random veto efficiencies: simple efficiency (no fancy graphs with adjusted X coordinates)
  TH1   *RVdenominator   = fHisto->Get<TH1F>("intensity")->Rebin(NIntensityBins, "RVdenominator", IntensityBinEdges.data());
  double totalKmu2Events = RVdenominator->GetEntries();
  TH2F  *singleVetoes    = fHisto->Get<TH2F>("singleVeto");
  for (auto &v : Pnn::PnnVetoes) {
    int  bin        = singleVetoes->GetYaxis()->FindFixBin(Pnn::VetoToString.at(v));
    auto efficiency = new TEfficiency(*singleVetoes->ProjectionX("_px", bin, bin), *RVdenominator);
    StyleRVEfficiency(efficiency, v);
    TString vetoString = Pnn::VetoToString.at(v);
    efficiency->SetName("RV_" + vetoString);
    efficiency->SetTitle(vetoString + " random veto efficiency " +
                         Form("(#epsilon_{RV} = %.1f%%)", 100 * efficiency->GetPassedHistogram()->GetEntries() / totalKmu2Events) +
                         ";Intensity (MHz);#epsilon_{RV}");
    fHisto->BookHisto(efficiency, "RVEfficiencies");
  }

  // integrated RV
  {
    TH1D *numerator   = singleVetoes->ProjectionY();
    TH1D *denominator = static_cast<TH1D *>(numerator->Clone("integratedRVdenominator"));
    denominator->Reset();
    for (int i = 1; i <= denominator->GetNbinsX(); i++)
      denominator->SetBinContent(i, totalKmu2Events);
    numerator->LabelsDeflate();
    denominator->LabelsDeflate();
    auto efficiency = new TEfficiency(*numerator, *denominator);
    efficiency->SetName("RVEfficiencies");
    efficiency->SetTitle("Random veto efficiencies;;");
    fHisto->BookHisto(efficiency, "RVEfficiencies");
  }

  // tails fraction analysis
  int kmu2Bin = std::find(RegionBins.begin(), RegionBins.end(), Pnn::kKmu2) - RegionBins.begin() + 1;
  int SR1Bin  = std::find(RegionBins.begin(), RegionBins.end(), Pnn::kPNN1) - RegionBins.begin() + 1;
  int SR2Bin  = std::find(RegionBins.begin(), RegionBins.end(), Pnn::kPNN2) - RegionBins.begin() + 1;

  TH2  *tailsHisto  = fHisto->Get<TH2>("p_region");
  TH1D *tailsDen    = tailsHisto->ProjectionX("tailsDen", kmu2Bin, kmu2Bin);
  TH1D *tailsNumSR1 = tailsHisto->ProjectionX("tailsNum1", SR1Bin, SR1Bin);
  TH1D *tailsNumSR2 = tailsHisto->ProjectionX("tailsNum2", SR2Bin, SR2Bin);
  TH1D *tailsNumTot = tailsHisto->ProjectionX("tailsNum", SR1Bin, SR2Bin);
  fHisto->BookHisto(tailsDen);
  fHisto->BookHisto(tailsNumSR1);
  fHisto->BookHisto(tailsNumSR2);
  fHisto->BookHisto(tailsNumTot);

  TEfficiency *tailsSR1 = new TEfficiency(*tailsNumSR1, *tailsDen);
  tailsSR1->SetNameTitle("tailsSR1", "f_{#mu2} (Region 1);p (GeV/c)");
  tailsSR1->SetLineColor(kRed);
  tailsSR1->SetMarkerColor(kRed);
  tailsSR1->SetMarkerStyle(kFullCircle);
  tailsSR1->SetFillColor(kRed);
  tailsSR1->SetFillStyle(3001);
  fHisto->BookHisto(tailsSR1);

  TEfficiency *tailsSR2 = new TEfficiency(*tailsNumSR2, *tailsDen);
  tailsSR2->SetNameTitle("tailsSR2", "f_{#mu2} (Region 2);p (GeV/c)");
  tailsSR2->SetLineColor(kBlue);
  tailsSR2->SetMarkerColor(kBlue);
  tailsSR2->SetMarkerStyle(kFullCircle);
  tailsSR2->SetFillColor(kBlue);
  tailsSR2->SetFillStyle(3001);
  fHisto->BookHisto(tailsSR2);

  TEfficiency *tailsTot = new TEfficiency(*tailsNumTot, *tailsDen);
  tailsTot->SetNameTitle("tails", "f_{#mu2};p (GeV/c)");
  tailsTot->SetLineColor(kBlack);
  tailsTot->SetMarkerColor(kBlack);
  tailsTot->SetMarkerStyle(kFullCircle);
  fHisto->BookHisto(tailsTot);

  TEfficiency *tailsMuonProb = new TEfficiency(*fHisto->Get<TH2>("tailsNum_p_muonProb"), *fHisto->Get<TH2>("tailsDen_p_muonProb"));
  tailsMuonProb->SetNameTitle("tails_p_muonProb", "f_{#mu2};p [GeV/c];P(#mu)");
  tailsMuonProb->SetLineColor(kBlack);
  tailsMuonProb->SetMarkerColor(kBlack);
  tailsMuonProb->SetMarkerStyle(kFullCircle);
  fHisto->BookHisto(tailsMuonProb);
}

void StyleRVEfficiency(TEfficiency *efficiency, Pnn::Veto v)
{
  int marker = 1, color = 1;
  switch (v) {
  case Pnn::kSAV:
    color  = kGreen;
    marker = kFullSquare;
    break;
  case Pnn::kLAV:
    color  = kMagenta;
    marker = kFullTriangleUp;
    break;
  case Pnn::kLKr:
    color  = kBlue;
    marker = kFullTriangleDown;
    break;
  case Pnn::kPhotonVeto:
    color  = kRed;
    marker = kFullDiamond;
    break;
  case Pnn::kAllVetoes:
    color  = kBlack;
    marker = kFullCircle;
    break;
  default:
    break;
  }

  efficiency->SetMarkerStyle(marker);
  efficiency->SetMarkerColor(color);
  efficiency->SetLineColor(color);
}