#include "K2piNormAnalysis.hh"
#include <iostream>
#include <TH2F.h>
#include <TEfficiency.h>
#include "KinematicRegion.hh"
#include "PnnSelection.hh"

void K2piNormAnalysis::StartOfJob()
{
  fSelectionHisto = fHisto->MakeChild();

  fHisto->BookHisto(new TEfficiency("Pmistag", ";CDA (mm);#DeltaT_{+} (ns);N_{GTK}", 40, 0, 4, 80, 0, 0.8, 26, -0.5, 25.5));

  fHisto->BookHisto(new TH2F("p_timestamp", ";Timestamp;p (GeV/c)", 200, 0, 6.5, 30, 15, 45));
  fHisto->BookHisto(new TH2F("intensity_p", ";p (GeV/c);Intensity (MHz)", 30, 15, 45, 120, 0, 5400));
  fHisto->BookHisto(new TH1F("downscaled_p", ";p (GeV/c)", 6, 15, 45));

  fHisto->BookHisto(new TH2F("Npipi_run_burst", "N_{#pi#pi};Burst;Run", 2501, -0.5, 2500.5, 1, 0, 1));
  fHisto->BookHisto(new TH2F("NpipiD_run_burst", "N_{#pi#pi} #times D;Burst;Run", 2501, -0.5, 2500.5, 1, 0, 1));
  fHisto->BookHisto(new TH2F("NpipiDBurst_intensity", ";Avg intensity (MHz);N_{#pi#pi} #times D/400 per burst", 100, 0, 1000, 800, 0, 800));
}

void K2piNormAnalysis::StartOfRun(const EventInfo &eventInfo)
{
  int runID = eventInfo.GetRunID();

  if (runID < 10000)
    fNormTrigDownscaling = 400;
  else if (runID <= 10778)
    fNormTrigDownscaling = 100;
  else if (runID <= 10887)
    fNormTrigDownscaling = 300;
  else
    fNormTrigDownscaling = 400;

  if (runID < 10000)
    fNormTrigBit = -1;
  else
    fNormTrigBit = 0;

  // resize run / burst histos
  for (TString histoname : {"Npipi_run_burst", "NpipiD_run_burst"}) {
    auto   histo       = fHisto->Get<TH2>(histoname);
    auto   itPeriodEnd = std::lower_bound(Pnn::PeriodStarts.begin(), Pnn::PeriodStarts.end(), runID);
    double runMin      = itPeriodEnd == Pnn::PeriodStarts.begin() ? runID : *std::prev(itPeriodEnd);
    double runMax      = itPeriodEnd == Pnn::PeriodStarts.end() ? runID : *itPeriodEnd;
    if (histo->GetYaxis()->GetXmin() == runMin && histo->GetYaxis()->GetXmax() == runMax)
      continue;
    fHisto->ResizeHisto(histoname, histo->GetXaxis()->GetXmin(), histo->GetXaxis()->GetXmax(), runMin, runMax);
  }
}

void K2piNormAnalysis::StartOfBurst()
{
  fIntensitySumPerBurst = 0;
  fNpipiPerBurst        = 0;
}

void K2piNormAnalysis::Process(const EventInfo &eventInfo, PnnNA62Event *event)
{
  fEventSelected = false;

  PnnSelection pnn(event, *fSelectionHisto);
  pnn.MonitorCut("Total");

  PNNSELECT(pnn, TriggerMask, fNormTrigBit)

  if (!(15 < pnn.GetTrackMomentum() && pnn.GetTrackMomentum() < 45)) return;
  pnn.MonitorCut("MomentumCut");

  PNNSELECT(pnn, Vertex)
  PNNSELECT(pnn, CHANTI)
  PNNSELECT(pnn, BeamBackground)
  PNNSELECT(pnn, VetoCounterVeto)
  PNNSELECT(pnn, Timing)
  PNNSELECT(pnn, CDA)
  PNNSELECT(pnn, MUV3Veto)
  PNNSELECT(pnn, PionPIDCalo)
  PNNSELECT(pnn, PionPIDRICH)
  PNNSELECT(pnn, BroadMultivertex)
  PNNSELECT(pnn, BoxCut)
  PNNSELECT(pnn, CobraVeto)
  PNNSELECT(pnn, ANTI0Veto)
  PNNSELECT(pnn, Kmu2gVeto)

  auto region = pnn.GetRegion();
  if (!(region == Pnn::kK2pi || region == Pnn::kControl1 || region == Pnn::kControl2))
    return;
  pnn.MonitorCut("NormalizationRegion");

  bool passesKpiAssociation = pnn.KPiAssociationBayes();
  fHisto->FillHisto<TEfficiency>("Pmistag",
                                 passesKpiAssociation,
                                 event->GetCDA(),
                                 sqrt(2) * (event->GetUpstreamParticle()->GetTGTK() -
                                            (event->GetUpstreamParticle()->GetTKTAG() + event->GetDownstreamParticle()->GetTRICH()) / 2),
                                 event->GetUpstreamParticle()->GetNGTKTracks());
  if (!passesKpiAssociation)
    return;
  pnn.MonitorCut("KPiAssociation");

  fEventSelected = true;
  fNpipiPerBurst++;

  double gtkIntensity = event->GetLambdaFW();
  double timestamp    = event->GetTimestamp() * TIMESTAMP_TO_SECONDS;

  fIntensitySumPerBurst += gtkIntensity;
  fHisto->FillHisto("downscaled_p", pnn.GetTrackMomentum(), fNormTrigDownscaling / 400.);
  fHisto->FillHisto("p_timestamp", timestamp, pnn.GetTrackMomentum());
  fHisto->FillHisto("intensity_p", pnn.GetTrackMomentum(), gtkIntensity);
  fHisto->FillHisto("Npipi_run_burst", eventInfo.GetBurstID(), eventInfo.GetRunID());
  fHisto->FillHisto<TH2>("NpipiD_run_burst", eventInfo.GetBurstID(), eventInfo.GetRunID(), fNormTrigDownscaling / 400.); // weight by downscaling factor for NK_effective computation
}

void K2piNormAnalysis::EndOfBurst()
{
  fEventSelected = false; // avoid persistence of state

  fHisto->FillHisto("NpipiDBurst_intensity", (double)fIntensitySumPerBurst / fNpipiPerBurst, fNpipiPerBurst * fNormTrigDownscaling / 400.);
}

void K2piNormAnalysis::EndOfJob()
{
  // MC acceptance
  TH1 *MCZvtx = fHisto->Get<TH1>("MCZvtx");
  if (MCZvtx != nullptr && MCZvtx->GetEntries() != 0) {
    // numerator
    auto num = fHisto->Get<TH1F>("downscaled_p");

    // denominator
    auto den = static_cast<TH1F *>(num->Clone("acceptance_denominator"));
    den->Reset();
    for (int i = 0; i <= den->GetNbinsX() + 1; i++)
      den->SetBinContent(i, MCZvtx->Integral(MCZvtx->FindBin(105e3), MCZvtx->FindBin(170e3 - 1)));

    // ratio
    auto acceptance = new TEfficiency(*num, *den);
    acceptance->SetName("MCAcceptance");
    fHisto->BookHisto(acceptance);
  }

  // remove empty histos vs run, which are a problem in hadd
  for (TString histoName : {"Npipi_run_burst", "NpipiD_run_burst"})
    if (fHisto->Get<TH1>(histoName)->GetEntries() == 0)
      fHisto->UnbookHisto(histoName);
}
