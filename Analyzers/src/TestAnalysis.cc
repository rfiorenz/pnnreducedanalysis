#include "TestAnalysis.hh"
#include <iostream>
#include "PnnSelection.hh"

void TestAnalysis::StartOfJob()
{
  fHisto->BookHisto(new TH1F("h", "", 10, 0, 80000));
  fHisto->BookHisto(new TH1F("analysisName", "", 1, 0, 1));
  fKmu2Histo = fHisto->MakeChild("Kmu2");
}

void TestAnalysis::Process(const EventInfo &eventInfo, PnnNA62Event *event)
{
  PnnSelection selection(event, eventInfo.GetTree() == Pnn::TKMU2 ? *fKmu2Histo : *fHisto);
  selection.MonitorCut("Events");

  fHisto->FillHisto("h", eventInfo.GetEventIndex());
  fHisto->FillHisto("analysisName", GetName(), 1);
}

void TestAnalysis::EndOfJob()
{
  std::cout << GetName() << ": " << std::endl;
  if (fSubAnalysis == NULL)
    std::cout << "   no subanalysis" << std::endl;
  else
    std::cout << "   subanalysis entries of 'h': " << fSubAnalysis->GetHisto("h", "tmp")->GetEntries() << std::endl;
}

void TestAnalysis::ProcessHistos(HistoHandler &histos)
{
  if (fSubAnalysis == NULL) // subanalysis; parent analysis is not asking to process histos
    std::cout << "entries here: " << histos.Get<TH1F>("CTRL_trig")->GetEntries() << "\taccumulated: " << fHisto->Get<TH1F>("CTRL_trig")->GetEntries() << std::endl;
}