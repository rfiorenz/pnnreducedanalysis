#include "UpstreamAnalysis.hh"
#include "KinematicRegion.hh"
#include <iostream>
#include "PnnSelection.hh"

void UpstreamAnalysis::StartOfJob()
{
  fSelectionHisto = fHisto->MakeChild();
  fSelectionHisto->BookHisto(new TH1F("NGTKTracks", "", 20, 0, 20));

  for (int i = 0; i < PnnSelection::NUpstreamValidationSamples; i++)
    fSampleHisto[i] = fHisto->MakeChild(i == 0 ? "SampleC" : Form("Validation%d", i));

  fUpstreamTree   = new TTree("UpstreamSample", "");
  fValidationTree = new TTree("Validation", "");
  for (auto &tree : {fUpstreamTree, fValidationTree}) {
    tree->Branch("RunID", &fRunID);
    tree->Branch("BurstID", &fBurstID);
    tree->Branch("EventID", &fEventID);
    tree->Branch("CDA", &fCDA);
    tree->Branch("TGTK", &fTGTK);
    tree->Branch("TRICH", &fTRICH);
    tree->Branch("TKTAG", &fTKTAG);
    tree->Branch("IsUpstreamSample", &fIsUpstreamSample);
    tree->Branch("Sample", &fSample);
    tree->Branch("NGTKTracks", &fNGTKTracks);
    tree->Branch("Momentum", &fMomentum);
    fHisto->BookHisto(tree);
  }
}

void UpstreamAnalysis::Process(const EventInfo &eventInfo, PnnNA62Event *event)
{
  PnnSelection pnn(event, *fSelectionHisto);

  auto region = pnn.GetRegion();

  // signal blinding
  pnn.DisableHistoFilling = true;
  if (!eventInfo.IsMC() && eventInfo.GetRunID() > 10000 && pnn.UpstreamValidationSample() == 0 && pnn.CDA() && pnn.Timing())
    return;
  pnn.DisableHistoFilling = false;

  pnn.MonitorCut("Total");
  PNNSELECT(pnn, TriggerMask, 1)
  PNNSELECT(pnn, Vertex)
  PNNSELECT(pnn, MUV3Veto)
  PNNSELECT(pnn, PionPIDCalo)
  PNNSELECT(pnn, PionPIDRICH)
  PNNSELECT(pnn, PhotonVeto)
  PNNSELECT(pnn, MultiplicityVeto)
  PNNSELECT(pnn, BroadMultivertex)
  PNNSELECT(pnn, CobraVeto)
  PNNSELECT(pnn, ANTI0Veto)
  PNNSELECT(pnn, Kmu2gVeto)

  double momentum = pnn.GetTrackMomentum();
  if (!(15 < momentum && momentum < 45)) return;
  pnn.MonitorCut("MomentumCut");

  fSelectionHisto->FillHisto("NGTKTracks", event->GetUpstreamParticle()->GetNGTKTracks());

  if (!std::set{Pnn::kPNN1, Pnn::kPNN2, Pnn::kUpstream}.count(region))
    return;
  pnn.MonitorCut("RegionCut");

  fRunID            = eventInfo.GetRunID();
  fBurstID          = eventInfo.GetBurstID();
  fEventID          = eventInfo.GetEventNumber();
  fCDA              = event->GetCDA();
  fTGTK             = event->GetUpstreamParticle()->GetTGTK();
  fTKTAG            = event->GetUpstreamParticle()->GetTKTAG();
  fTRICH            = event->GetDownstreamParticle()->GetTRICH();
  fNGTKTracks       = event->GetUpstreamParticle()->GetNGTKTracks();
  fSample           = pnn.UpstreamValidationSample();
  fMomentum         = pnn.GetTrackMomentum();
  fIsUpstreamSample = IsUpstreamSample(event, fSample); // aka Sample C. not applying this gives the sample used for CDA shape estimation

  if (fSample < 0)
    return;

  pnn.DisableHistoFilling = fSample == 0; // histogram blinding

  bool passesCDA = pnn.CDA();

  if (!passesCDA && fIsUpstreamSample)
    PnnSelection(event, *fSampleHisto[fSample]).MonitorCut("SampleC");

  if (!passesCDA || !fIsUpstreamSample) // sample for estimation of upstream background
    fUpstreamTree->Fill();
  else if (passesCDA && pnn.KPiAssociationBayes() && fIsUpstreamSample && // full PNN selection
           fSample != 0)                                                  // redundant blinding for signal region!
    fValidationTree->Fill();
}

bool UpstreamAnalysis::IsUpstreamSample(PnnNA62Event *event, int sample)
{
  // don't fill for signal sample
  return PnnSelection(event, sample == 0 ? nullptr : fSampleHisto[sample].get()).Timing();
}

void UpstreamAnalysis::EndOfJob()
{
  fHisto->Write();
  fSelectionHisto->Write();
  if (fUpstreamTree->GetEntries())
    for (auto &histo : fSampleHisto)
      histo->Write();
}