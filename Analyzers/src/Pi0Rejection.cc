#include "Pi0Rejection.hh"
#include <numeric>
#include <TEfficiency.h>
#include "PnnSelection.hh"

Pi0Rejection::Pi0Rejection(PnnAnalysis *pnnAnalysis, K2piNormAnalysis *normAnalysis)
    : VAnalysis("Pi0Rejection", {Pnn::TNORMK2PI, Pnn::TPNN}, {pnnAnalysis, normAnalysis}),
      fPnnAnalysis(pnnAnalysis), fK2piNormAnalysis(normAnalysis)
{
}

void Pi0Rejection::StartOfJob()
{
  for (auto &t : GetTrees()) {
    if (t == Pnn::TNORMK2PI || t == Pnn::TPNN) {
      TString treeString = Pnn::TreeToString.at(t);
      for (auto &x : {
               std::make_pair("photonVeto", "Single vetoes: events passing the veto in K_{2#pi} region (" + treeString + ")"),
               std::make_pair("pi0rejection", "#pi^{0} rejection: events passing sequential vetoes in K_{2#pi} region (" + treeString + ")"),
               std::make_pair("exclusiveVeto", "Events passing all other vetoes in K_{2#pi} region (" + treeString + ")"),
           }) {
        auto h = new TH2F(treeString + "_" + x.first, x.second, 6, 15, 45, Pnn::PnnVetoes.size() + 1, 0, Pnn::PnnVetoes.size() + 1);
        h->GetYaxis()->SetBinLabel(1, "Start");
        for (int i = 0; i < Pnn::PnnVetoes.size(); i++)
          h->GetYaxis()->SetBinLabel(i + 2, Pnn::VetoToString.at(Pnn::PnnVetoes[i]));
        fHisto->BookHisto(h);
      }
    }
  }
}

void Pi0Rejection::Process(const EventInfo &eventInfo, PnnNA62Event *event)
{
  if (eventInfo.GetTree() == Pnn::TNORMK2PI && fK2piNormAnalysis->IsEventSelected())
    Pi0RejectionAnalysis(Pnn::TNORMK2PI, event, fK2piNormAnalysis->GetNormTrigDownscaling());

  if (eventInfo.GetTree() == Pnn::TPNN && fPnnAnalysis->IsEventForPi0RejectionAnalysis())
    Pi0RejectionAnalysis(Pnn::TPNN, event);
}

void Pi0Rejection::Pi0RejectionAnalysis(Pnn::ReducedTree tree, PnnNA62Event *event, int weight)
{
  TString treePrefix = Pnn::TreeToString.at(tree) + "_";

  PnnSelection pnn(event, *fHisto);

  double momentum = pnn.GetTrackMomentum();

  fHisto->FillHisto<TH2>(treePrefix + "photonVeto", momentum, 0, weight);
  fHisto->FillHisto<TH2>(treePrefix + "pi0rejection", momentum, 0, weight);
  fHisto->FillHisto<TH2>(treePrefix + "exclusiveVeto", momentum, 0, weight);

  for (const auto &mode : {
           std::make_pair(PnnSelection::kSingle, "photonVeto"),
           std::make_pair(PnnSelection::kSequence, "pi0rejection"),
           std::make_pair(PnnSelection::kExclusive, "exclusiveVeto"),
       })
    for (auto &v : pnn.PnnVeto(Pnn::PnnVetoes, mode.first))
      if (v.second)
        fHisto->FillHisto<TH2>(treePrefix + mode.second, momentum, Pnn::VetoToString.at(v.first), weight);
}

void Pi0Rejection::EndOfJob()
{
  auto denominatorHisto = fHisto->Get<TH2>(Pnn::TreeToString.at(Pnn::TNORMK2PI) + "_pi0rejection")->ProjectionX("", 1, 1); // NpipiD vs p

  auto numerators = fHisto->Get<TH2>(Pnn::TreeToString.at(Pnn::TPNN) + "_pi0rejection"); // last y bin = events selected by PNN in K2pi + control1 + control2
  for (auto &v : Pnn::PnnVetoes) {
    auto         bin = numerators->GetYaxis()->FindFixBin(Pnn::VetoToString.at(v));
    TEfficiency *efficiency;
    ROOTErrorIgnoreLevel(kWarning, [&] { // silence "Info: weighted events"
      efficiency = new TEfficiency(*numerators->ProjectionX("_px", bin, bin), *denominatorHisto);
    });
    efficiency->SetNameTitle("pi0rejection_" + Pnn::VetoToString.at(v), "#pi^{0} rejection after " + Pnn::VetoToString.at(v) + " veto (uncorrected for RV and trig)");
    fHisto->BookHisto(efficiency);
  }
}
