#include "K2piAnalysis.hh"

#include "KinematicRegion.hh"
#include "PnnSelection.hh"

#include <TEfficiency.h>

static const std::vector<int> RegionBins = {
    Pnn::KinematicRegion::kPNN1,
    Pnn::KinematicRegion::kPNN2,
    Pnn::KinematicRegion::kControl1,
    Pnn::KinematicRegion::kControl2,
    Pnn::KinematicRegion::kControlMu,
    Pnn::KinematicRegion::kControlMu2,
    Pnn::KinematicRegion::kControlMu3,
    Pnn::KinematicRegion::kControl3pi,
    Pnn::KinematicRegion::kControl3d,
    Pnn::KinematicRegion::kK2pi,
    Pnn::KinematicRegion::kKmu2,
    Pnn::KinematicRegion::kK3pi,
    Pnn::KinematicRegion::kUpstream,
    Pnn::KinematicRegion::kOther,
    1000,
};

static const std::map<int, TString> RegionString = {
    {Pnn::KinematicRegion::kPNN1, "#pi#nu#nu1"},
    {Pnn::KinematicRegion::kPNN2, "#pi#nu#nu2"},
    {Pnn::KinematicRegion::kControl1, "Control1"},
    {Pnn::KinematicRegion::kControl2, "Control2"},
    {Pnn::KinematicRegion::kControlMu, "Control"},
    {Pnn::KinematicRegion::kControlMu2, "ControlMu2"},
    {Pnn::KinematicRegion::kControlMu3, "ControlMu3"},
    {Pnn::KinematicRegion::kControl3pi, "Control3pi"},
    {Pnn::KinematicRegion::kControl3d, "Control3d"},
    {Pnn::KinematicRegion::kK2pi, "K_{2#pi}"},
    {Pnn::KinematicRegion::kKmu2, "K_{#mu2}"},
    {Pnn::KinematicRegion::kK3pi, "K_{3#pi}"},
    {Pnn::KinematicRegion::kUpstream, "Upstream"},
    {Pnn::KinematicRegion::kOther, "Other"},
    {1000, "K_{2#pi}Wide"},
};

void K2piAnalysis::StartOfJob()
{
  fSelectionHisto = fHisto->MakeChild();

  auto h = new TH2F("p_region", "Region after tails selection; p (GeV/c)", 6, 15, 45, RegionBins.size(), 0, RegionBins.size());
  for (size_t iBin = 0; iBin < RegionBins.size(); iBin++)
    h->GetYaxis()->SetBinLabel(iBin + 1, RegionString.at(RegionBins[iBin]));
  fHisto->BookHisto(h);
}

void K2piAnalysis::Process(PnnNA62Event *event)
{
  PnnSelection k2pi(event, *fSelectionHisto);

  k2pi.MonitorCut("Total");
  PNNSELECT(k2pi, TriggerMask, 0)
  PNNSELECT(k2pi, Vertex)
  PNNSELECT(k2pi, CHANTI)
  PNNSELECT(k2pi, BeamBackground)
  PNNSELECT(k2pi, VetoCounterVeto)
  PNNSELECT(k2pi, Timing)
  PNNSELECT(k2pi, CDA)
  PNNSELECT(k2pi, KPiAssociationBayes)
  PNNSELECT(k2pi, MUV3Veto)
  PNNSELECT(k2pi, PionPIDCalo)
  PNNSELECT(k2pi, PionPIDRICH)
  PNNSELECT(k2pi, BoxCut)
  PNNSELECT(k2pi, CobraVeto)
  PNNSELECT(k2pi, ANTI0Veto)
  if (k2pi.PhotonVeto()) return; // orthogonal to PNN selection (redundant, already applied in reduction) (Reminder: PhotonVeto() = event passes photonveto = no signal in PV system)
  k2pi.MonitorCut("PhotonAntiVeto");
  for (auto &v : {Pnn::Veto::kLAV, Pnn::Veto::kIRC, Pnn::Veto::kSAC, Pnn::Veto::kSAV})
    if (k2pi.PnnVeto(v))
      return;
  k2pi.MonitorCut("LAVSAVVeto");

  PNNSELECT(k2pi, BroadMultivertex)
  PNNSELECT(k2pi, MultiplicityVeto)

  PNNSELECT(k2pi, Kmu2gVeto)

  auto region = k2pi.GetRegion();
  if (RegionString.count(region))
    fHisto->FillHisto<TH2>("p_region", k2pi.GetTrackMomentum(), RegionString.at(region), 1);

  // k2pi wide region
  if (event->GetMMiss() > 0.008 && event->GetMMiss() < 0.031)
    fHisto->FillHisto<TH2>("p_region", k2pi.GetTrackMomentum(), RegionString.at(1000), 1);
}

void K2piAnalysis::EndOfJob()
{
  TH2 *tailsHisto = fHisto->Get<TH2>("p_region");

  // tails fraction analysis
  int k2piBin = tailsHisto->GetYaxis()->FindFixBin(RegionString.at(Pnn::kK2pi));
  int SR1Bin  = tailsHisto->GetYaxis()->FindFixBin(RegionString.at(Pnn::kPNN1));
  int SR2Bin  = tailsHisto->GetYaxis()->FindFixBin(RegionString.at(Pnn::kPNN2));

  TH1D *tailsDen    = tailsHisto->ProjectionX("tailsDen", k2piBin, k2piBin);
  TH1D *tailsNumSR1 = tailsHisto->ProjectionX("tailsNum1", SR1Bin, SR1Bin);
  TH1D *tailsNumSR2 = tailsHisto->ProjectionX("tailsNum2", SR2Bin, SR2Bin);
  TH1D *tailsNumTot = tailsHisto->ProjectionX("tailsNum", SR1Bin, SR2Bin);
  fHisto->BookHisto(tailsDen);
  fHisto->BookHisto(tailsNumSR1);
  fHisto->BookHisto(tailsNumSR2);
  fHisto->BookHisto(tailsNumTot);

  TEfficiency *tailsSR1 = new TEfficiency(*tailsNumSR1, *tailsDen);
  tailsSR1->SetNameTitle("tailsSR1", "f_{2#pi} (Region 1);p (GeV/c)");
  tailsSR1->SetLineColor(kRed);
  tailsSR1->SetMarkerColor(kRed);
  tailsSR1->SetMarkerStyle(kFullCircle);
  tailsSR1->SetFillColor(kRed);
  tailsSR1->SetFillStyle(3001);
  fHisto->BookHisto(tailsSR1);

  TEfficiency *tailsSR2 = new TEfficiency(*tailsNumSR2, *tailsDen);
  tailsSR2->SetNameTitle("tailsSR2", "f_{2#pi} (Region 2);p (GeV/c)");
  tailsSR2->SetLineColor(kBlue);
  tailsSR2->SetMarkerColor(kBlue);
  tailsSR2->SetMarkerStyle(kFullCircle);
  tailsSR2->SetFillColor(kBlue);
  tailsSR2->SetFillStyle(3001);
  fHisto->BookHisto(tailsSR2);

  TEfficiency *tailsTot = new TEfficiency(*tailsNumTot, *tailsDen);
  tailsTot->SetNameTitle("tails", "f_{2#pi};p (GeV/c)");
  tailsTot->SetLineColor(kBlack);
  tailsTot->SetMarkerColor(kBlack);
  tailsTot->SetMarkerStyle(kFullCircle);
  fHisto->BookHisto(tailsTot);
}
